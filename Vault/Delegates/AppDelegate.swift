//
//  AppDelegate.swift
//  iOSTemplate
//
//  Created by John Hampton on 2/24/19.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // purchases
        PayManager.shared.initialiaze()
        PayManager.shared.loadPaywalls()
        PayManager.shared.getPurchaserInfo { isSuccess in}
        
        // marketing
        AdManager.initialize()
        
        // analitika
        Analitika.initialize()
        Analitika.updateUser([:])
        
        // notification
        NotificationManager.setDelegate(delegate: self)
        
        return true
    }

    
    // MARK: Scene Session Life Cycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {}
}

