//
//  SceneDelegate.swift
//  Cloudy
//
//  Created by Bart Jacobs on 15/05/2020.
//  Copyright © 2020 Cocoacasts. All rights reserved.
//

import UIKit
import Branch

var isWillResignActivePresentation = true{
    didSet{
        print("isWillResignActivePresentation = ", isWillResignActivePresentation)
        if isWillResignActivePresentation == false{
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                isWillResignActivePresentation = true
            }
        }
    }
}

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    // MARK: - Properties
    
    var window: UIWindow?

    // MARK: -
    
    static let appCoordinator = AppCoordinator()
    
    // MARK: - Scene Life Cycle
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else {return}
        
        // workaround for SceneDelegate continueUserActivity not getting called on cold start
        if let userActivity = connectionOptions.userActivities.first {
            BranchScene.shared().scene(scene, continue: userActivity)
        } else if !connectionOptions.urlContexts.isEmpty {
            BranchScene.shared().scene(scene, openURLContexts: connectionOptions.urlContexts)
        }
        
        // window
        self.window = UIWindow(windowScene: windowScene)
        self.window?.rootViewController = SceneDelegate.appCoordinator.rootViewController
        self.window?.makeKeyAndVisible()
        SceneDelegate.appCoordinator.start()
    }

    func sceneDidDisconnect(_ scene: UIScene) {}

    // MARK: -

    func sceneWillResignActive(_ scene: UIScene) {
        if isWillResignActivePresentation == true{
            SceneDelegate.appCoordinator.presentEnterViewController()
        }
    }

    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        BranchScene.shared().scene(scene, continue: userActivity)
    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        BranchScene.shared().scene(scene, openURLContexts: URLContexts)
    }
}
