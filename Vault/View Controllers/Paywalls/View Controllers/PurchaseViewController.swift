//
//  PurchaseViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 25.02.2022.
//

import UIKit
import SnapKit

protocol PurchaseProtocol{
    var didChangePrice: ((_ price: String) -> ())? {get set}
    
    func closeAction()
    func tryAction()
    func showTermsOfUse()
    func showPrivacyPolicy()
    func restoreAction()
}


class PurchaseViewController: UIViewController{

    // MARK: - Properties
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        let photoWidth: CGFloat = 0.637333333333333 * UIDevice.width
        scrollView.contentSize = CGSize(width: UIDevice.width, height: photoWidth + 930)
        return scrollView
    }()
    
    private let backImageView1: UIImageView = {
        let imageView = UIImageView(image: "purchaseback".img())
        return imageView
    }()
    
    private let backImageView2: UIImageView = {
        let imageView = UIImageView(image: "purchaseback".img())
        return imageView
    }()
    
    private let closeBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        button.setImage("close".img(), for: .normal)
        return button
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = Font.bold.size(24)
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 2
        label.textAlignment = .center
        label.textColor = UIColor(rgb: 0x1A202C)
        label.text = "Discover more features\nwith a premium"
        return label
    }()
    
    private let safeImageView: UIImageView = {
        let imageView = UIImageView(image: "safe".img())
        return imageView
    }()
    
    // MARK: -
    
    private let purchIcon1: UIImageView = {
        let imageView = UIImageView(image: "purchicon1".img())
        return imageView
    }()
    
    private let purchTitleLabel1: UILabel = {
        let label = UILabel()
        label.font = Font.medium.size(18)
        label.textColor = UIColor(rgb: 0x1A202C)
        label.text = "Fake Passwords"
        return label
    }()
    
    private let purchTextLabel1: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x718096)
        label.numberOfLines = 0
        label.font = Font.regular.size(14)
        label.text = """
You can create multiple passwords. If a user
enters the wrong password, they are
redirected to another account.
"""
        return label
    }()
    
    // MARK: -
    
    private let purchIcon2: UIImageView = {
        let imageView = UIImageView(image: "purchicon2".img())
        return imageView
    }()
    
    private let purchTitleLabel2: UILabel = {
        let label = UILabel()
        label.font = Font.medium.size(18)
        label.textColor = UIColor(rgb: 0x1A202C)
        label.text = "Unlimited Files"
        return label
    }()
    
    private let purchTextLabel2: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x718096)
        label.numberOfLines = 0
        label.font = Font.regular.size(14)
        label.text = """
Create as many files and
folders on your device as you want
"""
        return label
    }()
    
    // MARK: -
    
    private let purchIcon3: UIImageView = {
        let imageView = UIImageView(image: "purchicon3".img())
        return imageView
    }()
    
    private let purchTitleLabel3: UILabel = {
        let label = UILabel()
        label.font = Font.medium.size(18)
        label.textColor = UIColor(rgb: 0x1A202C)
        label.text = "No ads"
        return label
    }()
    
    private let purchTextLabel3: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x718096)
        label.numberOfLines = 0
        label.font = Font.regular.size(14)
        label.text = "Turn off all ads in the app"
        return label
    }()
    
    // MARK: -
    
    private let purchIcon4: UIImageView = {
        let imageView = UIImageView(image: "purchicon4".img())
        return imageView
    }()
    
    private let purchTitleLabel4: UILabel = {
        let label = UILabel()
        label.font = Font.medium.size(18)
        label.textColor = UIColor(rgb: 0x1A202C)
        label.text = "Screenshot Preventing"
        return label
    }()
    
    private let purchTextLabel4: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x718096)
        label.numberOfLines = 0
        label.font = Font.regular.size(14)
        label.text = """
Stop taking screenshots on your device,
keep all data protected!
"""
        return label
    }()
    
    // MARK: -
    
    private let purchIcon5: UIImageView = {
        let imageView = UIImageView(image: "purchicon5".img())
        return imageView
    }()
    
    private let purchTitleLabel5: UILabel = {
        let label = UILabel()
        label.font = Font.medium.size(18)
        label.textColor = UIColor(rgb: 0x1A202C)
        label.text = "Password Generator"
        return label
    }()
    
    private let purchTextLabel5: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x718096)
        label.numberOfLines = 0
        label.font = Font.regular.size(14)
        label.text = """
Create strong passwords
for any type of data
"""
        return label
    }()
    
    // MARK: -
    
    private let purchIcon6: UIImageView = {
        let imageView = UIImageView(image: "purchicon6".img())
        return imageView
    }()
    
    private let purchTitleLabel6: UILabel = {
        let label = UILabel()
        label.font = Font.medium.size(18)
        label.textColor = UIColor(rgb: 0x1A202C)
        label.text = "Passwords Section"
        return label
    }()
    
    private let purchTextLabel6: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x718096)
        label.numberOfLines = 0
        label.font = Font.regular.size(14)
        label.text = """
Create as many files and
folders on your device as you want
"""
        return label
    }()
    
    // MARK: -
    
    private let purchIcon7: UIImageView = {
        let imageView = UIImageView(image: "purchicon7".img())
        return imageView
    }()
    
    private let purchTitleLabel7: UILabel = {
        let label = UILabel()
        label.font = Font.medium.size(18)
        label.textColor = UIColor(rgb: 0x1A202C)
        label.text = "New Features and Updates"
        return label
    }()
    
    private let purchTextLabel7: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x718096)
        label.numberOfLines = 0
        label.font = Font.regular.size(14)
        label.text = """
Get access to new features
after updating the app.
"""
        return label
    }()
    
    // MARK: -
    
    private let priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x718096)
        label.textAlignment = .center
        label.numberOfLines = 2
        label.font = Font.regular.size(16)
        label.text = ""
        return label
    }()
    
    private let tryBtn: UIButton = {
        let button = UIButton()
        button.layer.cornerCurve = .continuous
        button.layer.cornerRadius = 16
        button.setTitle("Try for free", for: .normal)
        button.backgroundColor = UIColor(rgb: 0x334BFF)
        button.titleLabel?.font = Font.bold.size(16)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(tryAction), for: .touchUpInside)
        return button
    }()
    
    private let bottomStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        stackView.spacing = 3
        return stackView
    }()
        
    // MARK: -
    
    private var viewModel: PurchaseProtocol
        
    // MARK: - Initialization
    
    init(viewModel: PurchaseProtocol){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
     
        self.viewModel.didChangePrice = { [weak self] (price) in
            self?.priceLabel.text = price
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        setupViews()
        setConstraints()
        setBottomStackView()
    }
    
    // MARK: - Actions
    
    @objc private func tryAction(){
        viewModel.tryAction()
    }
    
    @objc private func closeAction(){
        viewModel.closeAction()
    }
    
    @objc private func restoreAction(){
        viewModel.restoreAction()
    }
    
    @objc private func policyAction(){
        viewModel.showPrivacyPolicy()
    }
    
    @objc private func termsAction(){
        viewModel.showTermsOfUse()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        view.addSubview(scrollView)
        scrollView.addSubview(backImageView1)
        scrollView.addSubview(backImageView2)
        scrollView.addSubview(closeBtn)
        scrollView.addSubview(titleLabel)
        scrollView.addSubview(safeImageView)
        
        scrollView.addSubview(purchIcon1)
        scrollView.addSubview(purchTitleLabel1)
        scrollView.addSubview(purchTextLabel1)
        
        scrollView.addSubview(purchIcon2)
        scrollView.addSubview(purchTitleLabel2)
        scrollView.addSubview(purchTextLabel2)

        scrollView.addSubview(purchIcon3)
        scrollView.addSubview(purchTitleLabel3)
        scrollView.addSubview(purchTextLabel3)
        
        scrollView.addSubview(purchIcon4)
        scrollView.addSubview(purchTitleLabel4)
        scrollView.addSubview(purchTextLabel4)
        
        scrollView.addSubview(purchIcon5)
        scrollView.addSubview(purchTitleLabel5)
        scrollView.addSubview(purchTextLabel5)
        
        scrollView.addSubview(purchIcon6)
        scrollView.addSubview(purchTitleLabel6)
        scrollView.addSubview(purchTextLabel6)
        
        scrollView.addSubview(purchIcon7)
        scrollView.addSubview(purchTitleLabel7)
        scrollView.addSubview(purchTextLabel7)
        
        scrollView.addSubview(priceLabel)
        scrollView.addSubview(tryBtn)
        scrollView.addSubview(bottomStackView)
    }
    
    private func setConstraints(){
        scrollView.snp.makeConstraints { make in
            make.leading.trailing.bottom.top.equalToSuperview()
        }
        
        backImageView1.snp.makeConstraints { make in
            make.leading.top.equalToSuperview()
            make.width.equalTo(UIDevice.width)
            make.height.equalTo(backImageView1.snp.width).multipliedBy(1.749333333333333)
        }
        
        backImageView2.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.top.equalTo(backImageView1.snp.bottom)
            make.width.equalTo(UIDevice.width)
            make.height.equalTo(backImageView1.snp.width).multipliedBy(1.749333333333333)
        }
        
        closeBtn.snp.makeConstraints { make in
            make.height.width.equalTo(48)
            make.leading.equalToSuperview().inset(16)
            make.top.equalTo(scrollView).inset(24)
        }

        titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(52)
            make.top.equalTo(closeBtn).inset(-3)
            make.width.equalTo(UIDevice.width-104)
        }
        
        safeImageView.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.width.equalTo(UIDevice.width)
            make.top.equalTo(titleLabel.snp.bottom).inset(-18)
            make.height.equalTo(safeImageView.snp.width).multipliedBy(0.637333333333333)
        }

        // --- 1
        
        purchIcon1.snp.makeConstraints { make in
            make.width.height.equalTo(32)
            make.leading.equalToSuperview().inset(24)
            make.top.equalTo(safeImageView.snp.bottom).inset(-24)
        }
        
        purchTitleLabel1.snp.makeConstraints { make in
            make.leading.equalTo(purchIcon1.snp.trailing).inset(-8)
            make.height.equalTo(32)
            make.centerY.equalTo(purchIcon1)
        }
        
        purchTextLabel1.snp.makeConstraints { make in
            make.leading.equalTo(purchTitleLabel1)
            make.top.equalTo(purchTitleLabel1.snp.bottom).inset(-8)
            make.width.equalTo(UIDevice.width-99)
        }
        
        // --- 2
        
        purchIcon2.snp.makeConstraints { make in
            make.width.height.equalTo(32)
            make.leading.equalToSuperview().inset(24)
            make.top.equalTo(purchTextLabel1.snp.bottom).inset(-16)
        }
        
        purchTitleLabel2.snp.makeConstraints { make in
            make.leading.equalTo(purchIcon2.snp.trailing).inset(-8)
            make.height.equalTo(32)
            make.centerY.equalTo(purchIcon2)
        }
        
        purchTextLabel2.snp.makeConstraints { make in
            make.leading.equalTo(purchTitleLabel2)
            make.top.equalTo(purchTitleLabel2.snp.bottom).inset(-8)
            make.width.equalTo(UIDevice.width-99)
        }
        
        // --- 3
        
        purchIcon3.snp.makeConstraints { make in
            make.width.height.equalTo(32)
            make.leading.equalToSuperview().inset(24)
            make.top.equalTo(purchTextLabel2.snp.bottom).inset(-16)
        }
        
        purchTitleLabel3.snp.makeConstraints { make in
            make.leading.equalTo(purchIcon3.snp.trailing).inset(-8)
            make.height.equalTo(32)
            make.centerY.equalTo(purchIcon3)
        }
        
        purchTextLabel3.snp.makeConstraints { make in
            make.leading.equalTo(purchTitleLabel3)
            make.top.equalTo(purchTitleLabel3.snp.bottom).inset(-8)
            make.width.equalTo(UIDevice.width-99)
        }
        
        // --- 4
        
        purchIcon4.snp.makeConstraints { make in
            make.width.height.equalTo(32)
            make.leading.equalToSuperview().inset(24)
            make.top.equalTo(purchTextLabel3.snp.bottom).inset(-16)
        }
        
        purchTitleLabel4.snp.makeConstraints { make in
            make.leading.equalTo(purchIcon4.snp.trailing).inset(-8)
            make.height.equalTo(32)
            make.centerY.equalTo(purchIcon4)
        }
        
        purchTextLabel4.snp.makeConstraints { make in
            make.leading.equalTo(purchTitleLabel4)
            make.top.equalTo(purchTitleLabel4.snp.bottom).inset(-8)
            make.width.equalTo(UIDevice.width-99)
        }
        
        // --- 5
        
        purchIcon5.snp.makeConstraints { make in
            make.width.height.equalTo(32)
            make.leading.equalToSuperview().inset(24)
            make.top.equalTo(purchTextLabel4.snp.bottom).inset(-16)
        }
        
        purchTitleLabel5.snp.makeConstraints { make in
            make.leading.equalTo(purchIcon5.snp.trailing).inset(-8)
            make.height.equalTo(32)
            make.centerY.equalTo(purchIcon5)
        }
        
        purchTextLabel5.snp.makeConstraints { make in
            make.leading.equalTo(purchTitleLabel5)
            make.top.equalTo(purchTitleLabel5.snp.bottom).inset(-8)
            make.width.equalTo(UIDevice.width-99)
        }
        
        // --- 6
        
        purchIcon6.snp.makeConstraints { make in
            make.width.height.equalTo(32)
            make.leading.equalToSuperview().inset(24)
            make.top.equalTo(purchTextLabel5.snp.bottom).inset(-16)
        }
        
        purchTitleLabel6.snp.makeConstraints { make in
            make.leading.equalTo(purchIcon6.snp.trailing).inset(-8)
            make.height.equalTo(32)
            make.centerY.equalTo(purchIcon6)
        }
        
        purchTextLabel6.snp.makeConstraints { make in
            make.leading.equalTo(purchTitleLabel6) // -
            make.top.equalTo(purchTitleLabel6.snp.bottom).inset(-8)
            make.width.equalTo(UIDevice.width-99)
        }
        
        // --- 7
        
        purchIcon7.snp.makeConstraints { make in
            make.width.height.equalTo(32)
            make.leading.equalToSuperview().inset(24)
            make.top.equalTo(purchTextLabel6.snp.bottom).inset(-16)
        }
        
        purchTitleLabel7.snp.makeConstraints { make in
            make.leading.equalTo(purchIcon7.snp.trailing).inset(-8)
            make.height.equalTo(32)
            make.centerY.equalTo(purchIcon7)
        }
        
        purchTextLabel7.snp.makeConstraints { make in
            make.leading.equalTo(purchTitleLabel7)
            make.top.equalTo(purchTitleLabel7.snp.bottom).inset(-8)
            make.width.equalTo(UIDevice.width-99)
        }
        
        // ---
        
        priceLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(24)
            make.width.equalTo(UIDevice.width-48)
            make.height.equalTo(44)
            make.top.equalTo(purchTextLabel7.snp.bottom).inset(-24)
        }
        
        tryBtn.snp.makeConstraints { make in
            make.height.equalTo(56)
            make.leading.equalToSuperview().inset(16)
            make.width.equalTo(UIDevice.width-32)
            make.top.equalTo(priceLabel.snp.bottom).inset(-16)
        }
        
        bottomStackView.snp.makeConstraints { make in
            make.height.equalTo(18)
            make.leading.trailing.equalTo(backImageView1).inset(16)
            make.top.equalTo(tryBtn.snp.bottom).inset(-15)
        }
    }
    
    
    private func setBottomStackView(){
        let privacyBtn = UIButton()
        privacyBtn.setTitle("Privacy Policy", for: .normal)
        privacyBtn.addTarget(self, action: #selector(policyAction), for: .touchUpInside)
        privacyBtn.setTitleColor(UIColor(rgb: 0x718096), for: .normal)
        privacyBtn.titleLabel?.font = Font.regular.size(14)
        privacyBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        bottomStackView.addArrangedSubview(privacyBtn)
        
        let imageView1 = UIImageView(image: "dot".img())
        imageView1.contentMode = .scaleAspectFit
        bottomStackView.addArrangedSubview(imageView1)
        
        let termsBtn = UIButton()
        termsBtn.setTitle("Terms of use", for: .normal)
        termsBtn.addTarget(self, action: #selector(termsAction), for: .touchUpInside)
        termsBtn.setTitleColor(UIColor(rgb: 0x718096), for: .normal)
        termsBtn.titleLabel?.font = Font.regular.size(14)
        termsBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        bottomStackView.addArrangedSubview(termsBtn)
        
        let imageView2 = UIImageView(image: "dot".img())
        imageView2.contentMode = .scaleAspectFit
        bottomStackView.addArrangedSubview(imageView2)
        
        let restoreBtn = UIButton()
        restoreBtn.setTitle("Restore Purchases", for: .normal)
        restoreBtn.addTarget(self, action: #selector(restoreAction), for: .touchUpInside)
        restoreBtn.setTitleColor(UIColor(rgb: 0x718096), for: .normal)
        restoreBtn.titleLabel?.font = Font.regular.size(14)
        restoreBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        bottomStackView.addArrangedSubview(restoreBtn)
    }
    
}
