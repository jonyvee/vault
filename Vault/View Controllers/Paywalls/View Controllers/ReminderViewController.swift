//
//  ReminderViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit
import SnapKit
import Combine

class ReminderViewController: UIViewController{
    
    // MARK: - Properties
    
    private var closeBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        button.setImage("close".img(), for: .normal)
        return button
    }()
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor(rgb: 0x1A202C)
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.text = "How the free Trial\nversion works"
        label.font = Font.bold.size(24)
        return label
    }()
    
    private var subtitleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor(rgb: 0x334BFF)
        label.adjustsFontSizeToFitWidth = true
        label.text = "Data security solutions"
        label.font = Font.bold.size(24)
        return label
    }()
    
    private var imageView: UIImageView = {
        let imageView = UIImageView(image: "reminder".img())
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let bottomStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        stackView.spacing = 3
        return stackView
    }()
    
    private var payBtn: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(rgb: 0x334BFF)
        button.addTarget(self, action: #selector(payAction), for: .touchUpInside)
        button.layer.cornerRadius = 16
        button.setTitle("Try it for free", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = Font.bold.size(16)
        return button
    }()
    
    private var priceLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = Font.semibold.size(16)
        label.textColor = UIColor(rgb: 0x718096)
        label.text = ""
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 0
        return label
    }()
    
    private var viewModel: ReminderViewModel
    
    // MARK: -
    
    var subscriptions: [AnyCancellable] = []
    
    // MARK: - Initialization
    
    init(viewModel: ReminderViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        
        viewModel.$priceText
            .sink { text in
                self.priceLabel.text = text
            }.store(in: &subscriptions)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        setupViews()
        setConstraints()
    }
    
    // MARK: - Actions
    
    @objc private func closeAction(){
        viewModel.closeAction()
    }
    
    @objc private func restoreAction(){
        viewModel.restoreAction()
    }
    
    @objc private func policyAction(){
        viewModel.showPrivacyPolicy()
    }
    
    @objc private func termsAction(){
        viewModel.showTermsOfUse()
    }
    
    @objc private func payAction(){
        viewModel.payAction()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        view.addSubview(closeBtn)
        view.addSubview(titleLabel)
        view.addSubview(subtitleLabel)
        view.addSubview(imageView)
        view.addSubview(bottomStackView)
        view.addSubview(payBtn)
        view.addSubview(priceLabel)
        setBottomStackView()
    }
    
    private func setConstraints(){
        closeBtn.snp.makeConstraints { make in
            make.height.width.equalTo(48)
            make.trailing.equalToSuperview().inset(16)
            make.top.equalTo(view.safeAreaLayoutGuide).inset(24)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.height.equalTo(60)
            make.leading.trailing.equalToSuperview().inset(24)
            make.top.equalTo(view.safeAreaLayoutGuide).inset(22)
        }
        
        subtitleLabel.snp.makeConstraints { make in
            make.height.equalTo(25)
            make.top.equalTo(titleLabel.snp.bottom).inset(-5)
            make.leading.trailing.equalTo(titleLabel)
        }
        
        imageView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(subtitleLabel.snp.bottom)
            make.bottom.equalTo(priceLabel.snp.top)
        }
        
        bottomStackView.snp.makeConstraints { make in
            make.height.equalTo(18)
            make.leading.trailing.equalToSuperview().inset(16)
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(UIDevice.hasNotch ? 0 : 8)
        }
        
        payBtn.snp.makeConstraints { make in
            make.height.equalTo(56)
            make.leading.trailing.equalToSuperview().inset(16)
            make.bottom.equalTo(bottomStackView.snp.top).inset(UIDevice.isSmall ? -8 : -14)
        }
        
        priceLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16)
            make.height.equalTo(50)
            make.bottom.equalTo(payBtn.snp.top).inset(UIDevice.isSmall ? -30 : -30)
        }
    }
        
    private func setBottomStackView(){
        let privacyBtn = UIButton()
        privacyBtn.setTitle("Privacy Policy", for: .normal)
        privacyBtn.addTarget(self, action: #selector(policyAction), for: .touchUpInside)
        privacyBtn.setTitleColor(UIColor(rgb: 0x718096), for: .normal)
        privacyBtn.titleLabel?.font = Font.regular.size(14)
        privacyBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        bottomStackView.addArrangedSubview(privacyBtn)
        
        let imageView1 = UIImageView(image: "dot".img())
        imageView1.contentMode = .scaleAspectFit
        bottomStackView.addArrangedSubview(imageView1)
        
        let termsBtn = UIButton()
        termsBtn.setTitle("Terms of use", for: .normal)
        termsBtn.addTarget(self, action: #selector(termsAction), for: .touchUpInside)
        termsBtn.setTitleColor(UIColor(rgb: 0x718096), for: .normal)
        termsBtn.titleLabel?.font = Font.regular.size(14)
        termsBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        bottomStackView.addArrangedSubview(termsBtn)
        
        let imageView2 = UIImageView(image: "dot".img())
        imageView2.contentMode = .scaleAspectFit
        bottomStackView.addArrangedSubview(imageView2)
        
        let restoreBtn = UIButton()
        restoreBtn.setTitle("Restore Purchases", for: .normal)
        restoreBtn.addTarget(self, action: #selector(restoreAction), for: .touchUpInside)
        restoreBtn.setTitleColor(UIColor(rgb: 0x718096), for: .normal)
        restoreBtn.titleLabel?.font = Font.regular.size(14)
        restoreBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        bottomStackView.addArrangedSubview(restoreBtn)
    }
}
