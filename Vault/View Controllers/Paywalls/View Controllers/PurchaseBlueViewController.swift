//
//  PurchaseBlueViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 21.03.2022.
//

import UIKit
import SnapKit

protocol PurchaseBlueProtocol{
    
    var numberOfSections: Int {get}
    var numberOfItems: Int {get}
    var selectedPrice: String {get set}
    var price1Text: String {get}
    var price2Text: String {get}
    var price3Text: String {get}
    var didChangePrice: (() -> ())? {get set}
    
    func viewModel(at index: Int) -> PurchaseBlueCellPresentable
    func closeAction()
    func termsAction()
    func restoreAction()
    func policyAction()
    func buyAction()
}

class PurchaseBlueViewController: UIViewController{
    
    // MARK: - Properties
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        let photoWidth = UIDevice.width*0.5
        scrollView.contentSize = CGSize(width: UIDevice.width, height: photoWidth + 600)
        return scrollView
    }()
    
    private let closeBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        button.setImage("close".img(), for: .normal)
        return button
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = Font.bold.size(24)
        label.textAlignment = .center
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.text = "Discover more features\nwith a premium"
        return label
    }()
    
    private let backImageView: UIImageView = {
        let imageView = UIImageView(image: "bluePaywallBackground".img())
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private let kingImageView: UIImageView = {
        let imageView = UIImageView(image: "bluePaywallKing".img())
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets.init(top: 0, left: 8, bottom: 0, right: 8)
        layout.itemSize = CGSize(width: UIDevice.width-32, height: 138)
        layout.minimumLineSpacing = 8
        layout.minimumInteritemSpacing = 8
        
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(PurchaseBlueCell.self, forCellWithReuseIdentifier: PurchaseBlueCell.key)
        
        return collectionView
    }()
    
    private var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.spacing = 4
        return stackView
    }()
    
    private var stackViewItemsRef: [UIView] = []

    private let payStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 6
        return stackView
    }()
    
    private lazy var payBtn1: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(payAction1), for: .touchUpInside)
        button.layer.cornerRadius = 24
        button.layer.borderColor = UIColor(rgb: 0x001EFF).cgColor
        button.backgroundColor = .white
        return button
    }()
    
    private lazy var payBtn2: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(payAction2), for: .touchUpInside)
        button.layer.cornerRadius = 24
        button.layer.borderColor = UIColor(rgb: 0x001EFF).cgColor
        button.backgroundColor = .white
        return button
    }()
    
    private lazy var payBtn3: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(payAction3), for: .touchUpInside)
        button.layer.cornerRadius = 24
        button.layer.borderColor = UIColor(rgb: 0x001EFF).cgColor
        button.backgroundColor = .white
        return button
    }()
    
    private let payTitleLabel1: UILabel = {
        let label = UILabel()
        label.font = Font.bold.size(36)
        label.text = "1"
        label.textAlignment = .center
        return label
    }()
    
    private let payTitleLabel2: UILabel = {
        let label = UILabel()
        label.font = Font.bold.size(36)
        label.text = "1"
        label.textAlignment = .center
        return label
    }()
    
    private let payTitleLabel3: UILabel = {
        let label = UILabel()
        label.font = Font.bold.size(36)
        label.text = "12"
        label.textAlignment = .center
        return label
    }()
    
    private let payMonthLabel1: UILabel = {
        let label = UILabel()
        label.text = "month"
        label.font = Font.bold.size(18)
        label.textAlignment = .center
        return label
    }()
    
    private let payMonthLabel2: UILabel = {
        let label = UILabel()
        label.text = "week"
        label.font = Font.bold.size(18)
        label.textAlignment = .center
        return label
    }()
    
    private let payMonthLabel3: UILabel = {
        let label = UILabel()
        label.text = "month"
        label.font = Font.bold.size(18)
        label.textAlignment = .center
        return label
    }()
    
    private let payPriceLabel1: UILabel = {
        let label = UILabel()
        label.font = Font.semibold.size(16)
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    private let payPriceLabel2: UILabel = {
        let label = UILabel()
        label.font = Font.semibold.size(16)
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    private let payPriceLabel3: UILabel = {
        let label = UILabel()
        label.font = Font.semibold.size(16)
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    private lazy var payBtn: UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.setTitleColor(UIColor(rgb: 0x334BFF), for: .normal)
        button.titleLabel?.font = Font.bold.size(20)
        button.setTitle("Get Premium Now", for: .normal)
        button.addTarget(self, action: #selector(payAction), for: .touchUpInside)
        button.layer.cornerRadius = 16
        button.layer.cornerCurve = .continuous
        return button
    }()
    
    private let bottomStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        stackView.spacing = 3
        return stackView
    }()
    
    // MARK: -
    
    private var isViewDidAppear = false
    
    private var viewModel: PurchaseBlueProtocol
        
    // MARK: - Initialization
    
    init(viewModel: PurchaseBlueProtocol){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
                
        self.viewModel.didChangePrice = { [weak self] in
            self?.payPriceLabel1.text = viewModel.price1Text
            self?.payPriceLabel2.text = viewModel.price2Text
            self?.payPriceLabel3.text = viewModel.price3Text
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ui
        setGradient()
        setupViews()
        setupPayStackViewItems()
        setupStackViewItems()
        updateStackViewItems(at: 0)
        setBottomStackView()
        setConstraints()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if isViewDidAppear == false{
            updatePriceTap(identifier: viewModel.selectedPrice)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isViewDidAppear = true
    }
    
    // MARK: - Actions
    
    @objc private func closeAction(){
        viewModel.closeAction()
    }
    
    @objc private func payAction1(){
        Vibration.soft.vibrate()
        updatePriceTap(identifier: Identifiers.month.rawValue)
    }
    
    @objc private func payAction2(){
        Vibration.soft.vibrate()
        updatePriceTap(identifier: Identifiers.weak.rawValue)
    }
    
    @objc private func payAction3(){
        Vibration.soft.vibrate()
        updatePriceTap(identifier: Identifiers.year.rawValue)
    }
    
    @objc private func payAction(){
        Vibration.soft.vibrate()
        viewModel.buyAction()
    }
    
    @objc private func restoreAction(){
        viewModel.restoreAction()
    }
    
    @objc private func policyAction(){
        viewModel.policyAction()
    }
    
    @objc private func termsAction(){
        viewModel.termsAction()
    }
    
    // MARK: - UI
    
    private func setGradient(){
        view.applyGradient(colours: [UIColor(rgb: 0x334BFF), UIColor(rgb: 0x020B55)], locations: [0,1], startPoint: CGPoint(x: 0.5, y: 0), endPoint: CGPoint(x: 0.5, y: 1))
    }
    
    private func setupStackViewItems(){
        for _ in 0..<viewModel.numberOfItems{
            let view = UIView()
            view.layer.cornerRadius = 2
            stackView.addArrangedSubview(view)
            stackViewItemsRef.append(view)
        }
    }
    
    private func setupPayStackViewItems(){
        payStackView.addArrangedSubview(payBtn1)
        payStackView.addArrangedSubview(payBtn2)
        payStackView.addArrangedSubview(payBtn3)
        payBtn1.addSubview(payTitleLabel1)
        payBtn1.addSubview(payMonthLabel1)
        payBtn1.addSubview(payPriceLabel1)
        payBtn2.addSubview(payTitleLabel2)
        payBtn2.addSubview(payMonthLabel2)
        payBtn2.addSubview(payPriceLabel2)
        payBtn3.addSubview(payTitleLabel3)
        payBtn3.addSubview(payMonthLabel3)
        payBtn3.addSubview(payPriceLabel3)
    }
    
    private func setupViews(){
        view.addSubview(scrollView)
        scrollView.addSubview(backImageView)
        scrollView.addSubview(kingImageView)
        scrollView.addSubview(closeBtn)
        scrollView.addSubview(titleLabel)
        scrollView.addSubview(collectionView)
        scrollView.addSubview(stackView)
        scrollView.addSubview(payStackView)
        scrollView.addSubview(payBtn)
        scrollView.addSubview(bottomStackView)
    }
    
    private func setConstraints(){
        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        backImageView.snp.makeConstraints { make in
            make.leading.top.equalToSuperview()
            make.width.equalTo(UIDevice.width)
            make.height.equalTo(UIDevice.width*0.864)
        }
        
        kingImageView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(backImageView)
            make.top.equalTo(titleLabel.snp.bottom).inset(-8)
            make.height.equalTo(UIDevice.width*0.5)
        }
        
        closeBtn.snp.makeConstraints { make in
            make.height.width.equalTo(48)
            make.leading.equalToSuperview().inset(10)
            make.top.equalTo(scrollView)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(16)
            make.width.equalTo(UIDevice.width-32)
            make.height.equalTo(60)
            make.top.equalTo(closeBtn.snp.bottom)
        }
        
        collectionView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(backImageView)
            make.top.equalTo(kingImageView.snp.bottom).inset(-24)
            make.height.equalTo(138)
        }
        
        stackView.snp.makeConstraints { make in
            make.height.equalTo(4)
            make.width.equalTo(212)
            make.centerX.equalTo(titleLabel)
            make.top.equalTo(collectionView.snp.bottom).inset(-16)
        }
        
        payStackView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(backImageView).inset(16)
            make.height.equalTo(110)
            make.top.equalTo(stackView.snp.bottom).inset(-24)
        }
        
        payTitleLabel1.snp.makeConstraints { make in
            make.height.equalTo(44)
            make.leading.trailing.equalToSuperview().inset(15)
            make.top.equalToSuperview().inset(11)
        }
        
        payMonthLabel1.snp.makeConstraints { make in
            make.height.equalTo(25)
            make.leading.trailing.equalToSuperview().inset(15)
            make.top.equalTo(payTitleLabel1.snp.bottom)
        }
        
        payPriceLabel1.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(15)
            make.bottom.equalToSuperview().inset(11)
            make.top.equalTo(payMonthLabel1.snp.bottom)
        }
        
        payTitleLabel2.snp.makeConstraints { make in
            make.height.equalTo(44)
            make.leading.trailing.equalToSuperview().inset(15)
            make.top.equalToSuperview().inset(11)
        }
        
        payMonthLabel2.snp.makeConstraints { make in
            make.height.equalTo(25)
            make.leading.trailing.equalToSuperview().inset(15)
            make.top.equalTo(payTitleLabel2.snp.bottom)
        }
        
        payPriceLabel2.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(15)
            make.bottom.equalToSuperview().inset(11)
            make.top.equalTo(payMonthLabel2.snp.bottom)
        }
        
        payTitleLabel3.snp.makeConstraints { make in
            make.height.equalTo(44)
            make.leading.trailing.equalToSuperview().inset(15)
            make.top.equalToSuperview().inset(11)
        }
        
        payMonthLabel3.snp.makeConstraints { make in
            make.height.equalTo(25)
            make.leading.trailing.equalToSuperview().inset(15)
            make.top.equalTo(payTitleLabel3.snp.bottom)
        }
        
        payPriceLabel3.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(15)
            make.bottom.equalToSuperview().inset(11)
            make.top.equalTo(payMonthLabel3.snp.bottom)
        }
        
        payBtn.snp.makeConstraints { make in
            make.height.equalTo(56)
            make.leading.trailing.equalTo(backImageView).inset(16)
            make.top.equalTo(payStackView.snp.bottom).inset(-16)
        }
        
        bottomStackView.snp.makeConstraints { make in
            make.height.equalTo(18)
            make.leading.trailing.equalTo(backImageView).inset(16)
            make.top.equalTo(payBtn.snp.bottom).inset(-24)
        }
    }
    
    private func setBottomStackView(){
        let privacyBtn = UIButton()
        privacyBtn.setTitle("Privacy Policy", for: .normal)
        privacyBtn.addTarget(self, action: #selector(policyAction), for: .touchUpInside)
        privacyBtn.setTitleColor(.white, for: .normal)
        privacyBtn.titleLabel?.font = Font.regular.size(14)
        privacyBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        bottomStackView.addArrangedSubview(privacyBtn)
        
        let imageView1 = UIImageView(image: "dot".img())
        imageView1.contentMode = .scaleAspectFit
        bottomStackView.addArrangedSubview(imageView1)
        
        let termsBtn = UIButton()
        termsBtn.setTitle("Terms of use", for: .normal)
        termsBtn.addTarget(self, action: #selector(termsAction), for: .touchUpInside)
        termsBtn.setTitleColor(.white, for: .normal)
        termsBtn.titleLabel?.font = Font.regular.size(14)
        termsBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        bottomStackView.addArrangedSubview(termsBtn)
        
        let imageView2 = UIImageView(image: "dot".img())
        imageView2.contentMode = .scaleAspectFit
        bottomStackView.addArrangedSubview(imageView2)
        
        let restoreBtn = UIButton()
        restoreBtn.setTitle("Restore Purchases", for: .normal)
        restoreBtn.addTarget(self, action: #selector(restoreAction), for: .touchUpInside)
        restoreBtn.setTitleColor(.white, for: .normal)
        restoreBtn.titleLabel?.font = Font.regular.size(14)
        restoreBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        bottomStackView.addArrangedSubview(restoreBtn)
    }
    
    // MARK: - Helper
    
    private func updatePriceTap(identifier: String){
        viewModel.selectedPrice = identifier
        
        // remove gradient
        payBtn1.removeGradient()
        payBtn2.removeGradient()
        payBtn3.removeGradient()
        payBtn1.layer.borderWidth = 0
        payBtn2.layer.borderWidth = 0
        payBtn3.layer.borderWidth = 0
        payTitleLabel1.textColor = UIColor(rgb: 0x334BFF)
        payMonthLabel1.textColor = UIColor(rgb: 0x334BFF)
        payPriceLabel1.textColor = UIColor(rgb: 0x1A202C)
        payTitleLabel2.textColor = UIColor(rgb: 0x334BFF)
        payMonthLabel2.textColor = UIColor(rgb: 0x334BFF)
        payPriceLabel2.textColor = UIColor(rgb: 0x1A202C)
        payTitleLabel3.textColor = UIColor(rgb: 0x334BFF)
        payMonthLabel3.textColor = UIColor(rgb: 0x334BFF)
        payPriceLabel3.textColor = UIColor(rgb: 0x1A202C)
        
        if identifier == Identifiers.month.rawValue{
            payTitleLabel1.textColor = .white
            payMonthLabel1.textColor = .white
            payPriceLabel1.textColor = .white
            payBtn1.layer.borderWidth = 2
            payBtn1.applyGradient(colours: [UIColor(rgb: 0x001EFF), UIColor(rgb: 0x061169)], locations: [0,1], startPoint: CGPoint(x: 0.5, y: 0), endPoint: CGPoint(x: 0.5, y: 1), cornerRadius: 24)
        }else if identifier == Identifiers.weak.rawValue{
            payTitleLabel2.textColor = .white
            payMonthLabel2.textColor = .white
            payPriceLabel2.textColor = .white
            payBtn2.layer.borderWidth = 2
            payBtn2.applyGradient(colours: [UIColor(rgb: 0x001EFF), UIColor(rgb: 0x061169)], locations: [0,1], startPoint: CGPoint(x: 0.5, y: 0), endPoint: CGPoint(x: 0.5, y: 1), cornerRadius: 24)
        }else {
            payTitleLabel3.textColor = .white
            payMonthLabel3.textColor = .white
            payPriceLabel3.textColor = .white
            payBtn3.layer.borderWidth = 2
            payBtn3.applyGradient(colours: [UIColor(rgb: 0x001EFF), UIColor(rgb: 0x061169)], locations: [0,1], startPoint: CGPoint(x: 0.5, y: 0), endPoint: CGPoint(x: 0.5, y: 1), cornerRadius: 24)
        }
    }
    
    private func update(at index: Int){
        updateStackViewItems(at: index)
    }

    private func updateStackViewItems(at index: Int){
        stackViewItemsRef.enumerated().forEach { element in
            let isChoosed = index == element.offset
            element.element.backgroundColor = isChoosed ? UIColor(rgb: 0x334BFF) : .white
        }
    }
}

// MARK: - UICollectionViewDelegate

extension PurchaseBlueViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let board = Int(scrollView.contentOffset.x)/Int(UIDevice.width-32-8)
        self.update(at: board)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let board = Int(scrollView.contentOffset.x)/Int(UIDevice.width-32-8)
        self.update(at: board)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        viewModel.numberOfSections
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PurchaseBlueCell.key, for: indexPath) as! PurchaseBlueCell
        let presentable = viewModel.viewModel(at: indexPath.row)
        cell.configure(with: presentable)
        return cell
    }
}
