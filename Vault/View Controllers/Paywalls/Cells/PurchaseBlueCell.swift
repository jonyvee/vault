//
//  PurchaseBlueCell.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 21.03.2022.
//

import UIKit
import SnapKit

protocol PurchaseBlueCellPresentable{
    var image: UIImage? {get}
    var title: String {get}
    var subtitle: String {get}
}

class PurchaseBlueCell: UICollectionViewCell{
    
    // MARK: - Static
    
    static let key = "PurchaseBlueCell"
    
    // MARK: - Properties
    
    private let backView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerCurve = .continuous
        view.layer.cornerRadius = 24
        return view
    }()
    
    private let titleView: UIView = UIView()
    private let iconImageView: UIImageView = UIImageView()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = Font.bold.size(18)
        label.textColor = UIColor(rgb: 0x1A202C)
        return label
    }()
    
    private let textLabel: UILabel = {
        let label = UILabel()
        label.font = Font.regular.size(16)
        label.textColor = UIColor(rgb: 0x718096)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configuration
    
    func configure(with presentable: PurchaseBlueCellPresentable){
        iconImageView.image = presentable.image
        titleLabel.text = presentable.title
        textLabel.text = presentable.subtitle
    }
    
    // MARK: - UI
    
    private func setupViews(){
        contentView.addSubview(backView)
        backView.addSubview(titleView)
        titleView.addSubview(iconImageView)
        titleView.addSubview(titleLabel)
        backView.addSubview(textLabel)
    }
    
    private func setConstraints(){
        backView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        titleView.snp.makeConstraints { make in
            make.height.equalTo(32)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(16)
        }
        
        iconImageView.snp.makeConstraints { make in
            make.top.leading.bottom.equalToSuperview()
            make.width.equalTo(32)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(iconImageView.snp.trailing).inset(-8)
            make.top.bottom.trailing.equalToSuperview()
        }
        
        textLabel.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview().inset(16)
            make.top.equalTo(titleView.snp.bottom).inset(-8)
        }
    }
    
}
