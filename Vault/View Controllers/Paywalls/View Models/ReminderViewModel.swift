//
//  ReminderViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import Foundation
import Combine
import Adapty
import SwiftLoader

class ReminderViewModel{
    
    // MARK: - Properties
    
    var didClose: (() -> ())?
    var didShowPrivacy: (() -> ())?
    var didShowTerms: (() -> ())?
    
    // MARK: -
    
    private var products: [ProductModel] = []
    
    // MARK: -
    
    @Published var priceText: String = ""
    
    // MARK: - Initialization
    
    init() {
        loadPurchaseDataIfNeeded()
    }
    
    // MARK: - Methods
    
    func showTermsOfUse(){
        didShowPrivacy?()
        Analitika.report("reminderTermsAction")
    }
    
    func showPrivacyPolicy(){
        didShowTerms?()
        Analitika.report("reminderPolicyAction")
    }
    
    func restoreAction(){
        Analitika.report("reminderRestoreAction")
        SwiftLoader.show(animated: true)
        Adapty.restorePurchases { purchaserInfo, receipt, appleValidationResult, error in
            DispatchQueue.main.async {
                SwiftLoader.hide()
            }
            
            if let error = error{
                DispatchQueue.main.async {JonyMessage.show(text: error.localizedDescription)}
                Analitika.report("reminderRestoreCancelation")
                return
            }
            
            User.shared.isPurchased = true
            Analitika.report("reminderRestoreSuccess")
            DispatchQueue.main.async {
                self.didClose?()
            }
        }
    }
    
    
    func payAction(){
        // запрос на уведомление
        NotificationManager.registerForPushNotifications { isEnabled in
            User.shared.isPushEnabled = isEnabled
            if isEnabled{
                Analitika.report("notificationEnableAction")
            }else{
                Analitika.report("notificationDissableAction")
            }
        }
        
        // загрузка оплаты
        Analitika.report("reminderPayAction")
        guard let product = self.products.first else {return}
        SwiftLoader.show(animated: true)
        Adapty.makePurchase(product: product) { purchaserInfo, receipt, appleValidationResult, product, error in
            DispatchQueue.main.async {
                SwiftLoader.hide()
            }
            
            if error != nil{
                DispatchQueue.main.async {JonyMessage.show(text: "Something went wrong, try again")}
                Analitika.report("reminderPurchaseCancelation")
                return
            }
            
            User.shared.isPurchased = true
            Analitika.report("reminderPurchaseSuccess")
            if User.shared.isPushEnabled == true{
                NotificationManager.turnOnTrialReminderNotification()
            }
            
            DispatchQueue.main.async {
                self.didClose?()
            }
        }
    }
    
    func closeAction(){
        didClose?()
        Analitika.report("reminderCloseAction")
    }
    
    
    // MARK: - Helper
    
    private func setPriceText(){
        guard let product = self.products.first else {return}
        guard let price = product.localizedPrice else {return}
        let trialText = product.introductoryOfferEligibility ? "3 days for free\nthen " : ""
        priceText = "\(trialText)\(price) per week"
    }
    
    private func loadPurchaseDataIfNeeded(){
        PayManager.shared.getProducts(paywall: .reminder) { products in
            guard let products = products else {
                JonyMessage.show(text: "Something went wrong, try again")
                return
            }
            self.products = products
            
            DispatchQueue.main.async {
                self.setPriceText()
            }
        }
    }
}
