//
//  PurchaseViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 25.02.2022.
//

import UIKit
import Adapty
import SwiftLoader

class PurchaseViewModel: PurchaseProtocol{
    
    // MARK: - Properties
    
    var didClose: (() -> ())?
    var didShowPrivacy: (() -> ())?
    var didShowTerms: (() -> ())?
    
    // MARK: -
    
    var didChangePrice: ((_ price: String) -> ())?
    
    // MARK: -
    
    private var products: [ProductModel] = []
        
    // MARK: - Initialization
    
    init(){
        loadPurchaseDataIfNeeded()
    }
    
    // MARK: - Methods
    
    func closeAction() {
        didClose?()
        Analitika.report("purchaseWhiteCloseAction")
    }
    
    func showTermsOfUse() {
        didShowTerms?()
        Analitika.report("purchaseWhiteTermsAction")
    }
    
    func showPrivacyPolicy() {
        didShowPrivacy?()
        Analitika.report("purchaseWhitePolicy")
    }
    
    func restoreAction() {
        Analitika.report("purchaseWhiteRestoreAction")
        SwiftLoader.show(animated: true)
        isWillResignActivePresentation = false
        
        Adapty.restorePurchases { purchaserInfo, receipt, appleValidationResult, error in
            DispatchQueue.main.async {
                SwiftLoader.hide()
            }
            
            if let error = error{
                DispatchQueue.main.async {JonyMessage.show(text: error.localizedDescription)}
                Analitika.report("purchaseWhiteRestoreCancelation")
                return
            }
            
            User.shared.isPurchased = true
            Analitika.report("purchaseWhiteRestoreSuccess")
            DispatchQueue.main.async {
                self.didClose?()
            }
        }
    }
    
    func tryAction() {
        Analitika.report("purchaseWhitePayAction")
        guard let product = self.products.first else {return}
        SwiftLoader.show(animated: true)
        isWillResignActivePresentation = false
        
        Adapty.makePurchase(product: product) { purchaserInfo, receipt, appleValidationResult, product, error in
            DispatchQueue.main.async {
                SwiftLoader.hide()
            }
            
            if error != nil{
                DispatchQueue.main.async {JonyMessage.show(text: "Something went wrong, try again")}
                Analitika.report("purchaseWhitePurchaseCancelation")
                return
            }
            
            User.shared.isPurchased = true
            Analitika.report("purchaseWhitePurchaseSuccess")
            DispatchQueue.main.async {
                self.didClose?()
            }
        }
    }

    // MARK: - Helper
    
    private func setPriceText(){
        guard let product = self.products.first else {return}
        guard let price = product.localizedPrice else {return}
        let trialText = product.introductoryOfferEligibility ? "3 days for free\nthen " : ""
        didChangePrice?("\(trialText)\(price) per week")
    }
    
    private func loadPurchaseDataIfNeeded(){
        PayManager.shared.getProducts(paywall: .white) { products in
            guard let products = products else {
                JonyMessage.show(text: "Something went wrong, try again")
                return
            }
            self.products = products
            
            DispatchQueue.main.async {
                self.setPriceText()
            }
        }
    }
}
