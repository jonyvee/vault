//
//  PurchaseBlueViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 21.03.2022.
//

import UIKit
import Adapty
import SwiftLoader

class PurchaseBlueViewModel: PurchaseBlueProtocol{
    
    // MARK: - Properties
    
    var didClose: (() -> ())?
    var didShowPrivacy: (() -> ())?
    var didShowTerms: (() -> ())?
    
    // MARK: -
    
    private let data = PurchaseBlueData().items
    
    // MARK: -
    
    private var products: [ProductModel] = []
    
    // MARK: - Initialization
    
    init(){
        loadPurchaseDataIfNeeded()
    }
    
    // MARK: - Methods
    
    var didChangePrice: (() -> ())?
    
    var selectedPrice: String = Identifiers.weak.rawValue
    
    var numberOfSections: Int{
        return 1
    }
    
    var numberOfItems: Int{
        return data.count
    }
    
    var price1Text: String = ""
    
    var price2Text: String = ""
    
    var price3Text: String = ""
    
    
    func viewModel(at index: Int) -> PurchaseBlueCellPresentable {
        return data[index]
    }
    
    func closeAction() {
        didClose?()
        Analitika.report("purchaseBlueCloseAction")
    }
    
    func termsAction() {
        didShowTerms?()
        Analitika.report("purchaseBlueTermsAction")
    }
    
    func policyAction() {
        didShowPrivacy?()
        Analitika.report("purchaseBluePolicyAction")
    }
    
    func restoreAction() {
        Analitika.report("purchaseBlueRestoreAction")
        SwiftLoader.show(animated: true)
        isWillResignActivePresentation = false
        
        Adapty.restorePurchases { purchaserInfo, receipt, appleValidationResult, error in
            DispatchQueue.main.async {
                SwiftLoader.hide()
            }
            
            if let error = error{
                DispatchQueue.main.async {JonyMessage.show(text: error.localizedDescription)}
                Analitika.report("purchaseBlueRestoreCanceltion")
                return
            }
            
            User.shared.isPurchased = true
            Analitika.report("purchaseBlueRestoreSuccess")
            DispatchQueue.main.async {
                self.didClose?()
            }
        }
    }
    
    func buyAction() {
        Analitika.report("purchaseBluePayAction")
        guard let product = self.products.first(where: {$0.vendorProductId == selectedPrice}) else {return}
        SwiftLoader.show(animated: true)
        isWillResignActivePresentation = false
        
        Adapty.makePurchase(product: product) { purchaserInfo, receipt, appleValidationResult, product, error in
            DispatchQueue.main.async {
                SwiftLoader.hide()
            }
            
            if error != nil{
                DispatchQueue.main.async {JonyMessage.show(text: "Something went wrong, try again")}
                Analitika.report("purchaseBluePurchaseCancelation")
                return
            }
            
            User.shared.isPurchased = true
            Analitika.report("purchaseBluePurchaseSuccess")
            DispatchQueue.main.async {
                self.didClose?()
            }
        }
    }
    
    // MARK: - Helper
    
    private func setPriceText(){
        guard let product1 = self.products.first(where: {$0.vendorProductId == Identifiers.month.rawValue}) else {return}
        guard let product2 = self.products.first(where: {$0.vendorProductId == Identifiers.weak.rawValue}) else {return}
        guard let product3 = self.products.first(where: {$0.vendorProductId == Identifiers.year.rawValue}) else {return}
        self.price1Text = product1.localizedPrice ?? ""
        self.price2Text = product2.localizedPrice ?? ""
        self.price3Text = product3.localizedPrice ?? ""
        didChangePrice?()
    }
    
    private func loadPurchaseDataIfNeeded(){
        PayManager.shared.getProducts(paywall: .blue) { products in
            guard let products = products else {
                JonyMessage.show(text: "Something went wrong, try again")
                return
            }
            self.products = products
                        
            DispatchQueue.main.async {
                self.setPriceText()
            }
        }
    }
}
