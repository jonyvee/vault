//
//  SettingsTableViewCellPresentable.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 22.02.2022.
//

import UIKit

protocol SettingsCellPresentable{
    
    var isSwitcher: Bool { get }
    var isOn: Bool? { get }
    var icon: UIImage? { get }
    var title: String { get }
    var subtitle: String { get }
}
