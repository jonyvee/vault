//
//  ImportTypeViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 26.02.2022.
//

import UIKit
import SnapKit

protocol ImportTypeProtocol{
    func closeAction()
    func chooseType(_ index: Int)
}

class ImportTypeViewController: UIViewController{
    
    private let bottomSafeAreaView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    private let backView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        view.layer.cornerCurve = .continuous
        view.layer.cornerRadius = 24
        return view
    }()
    
    private let topLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(rgb: 0xF6F6F6)
        view.layer.cornerRadius = 2.5
        return view
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = Font.bold.size(24)
        label.textAlignment = .center
        label.textColor = UIColor(rgb: 0x23262F)
        label.text = "Adding to Vault"
        return label
    }()
    
    private let textLabel: UILabel = {
        let label = UILabel()
        label.font = Font.regular.size(14)
        label.textAlignment = .center
        label.textColor = UIColor(rgb: 0x718096)
        label.text = "Rate the app"
        label.numberOfLines = 3
        label.adjustsFontSizeToFitWidth = true
        label.text = "By default, Vault App will ask if you want to keep or\ndelete files copied from Photos. Deleting originals\neach time requires permission"
        return label
    }()
    
    private let type1Btn: UIButton = {
        let button = UIButton()
        button.tag = 0
        button.addTarget(self, action: #selector(typeAction(_:)), for: .touchUpInside)
        return button
    }()
    
    private let type2Btn: UIButton = {
        let button = UIButton()
        button.tag = 1
        button.addTarget(self, action: #selector(typeAction(_:)), for: .touchUpInside)
        return button
    }()
    
    private let type3Btn: UIButton = {
        let button = UIButton()
        button.tag = 2
        button.addTarget(self, action: #selector(typeAction(_:)), for: .touchUpInside)
        return button
    }()
    
    private let type1Label: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x1A202C)
        label.text = "Ask to Delete or Keep Originals"
        label.font = Font.medium.size(18)
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    private let type2Label: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x1A202C)
        label.text = "Always Delete"
        label.font = Font.medium.size(18)
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    private let type3Label: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x1A202C)
        label.text = "Always Keep Originals"
        label.font = Font.medium.size(18)
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    private let type1ImageView = UIImageView()
    private let type2ImageView = UIImageView()
    private let type3ImageView = UIImageView()
    
    // MARK: -
    
    private let viewModel: ImportTypeProtocol
    
    // MARK: - Initialization
    
    init(viewModel: ImportTypeProtocol){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        setupViews()
        setGestures()
        setConstraints()
        setSelection()
    }
    
    // MARK: - Actions
    
    @objc private func closeAction(){
        Vibration.soft.vibrate()
        viewModel.closeAction()
    }
    
    @objc private func typeAction(_ sender: UIButton){
        Vibration.soft.vibrate()
        viewModel.chooseType(sender.tag)
        setSelection()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        self.view.addSubview(backView)
        self.view.addSubview(bottomSafeAreaView)
        backView.addSubview(topLineView)
        backView.addSubview(titleLabel)
        backView.addSubview(textLabel)
        
        backView.addSubview(type1Btn)
        backView.addSubview(type2Btn)
        backView.addSubview(type3Btn)

        type1Btn.addSubview(type1Label)
        type2Btn.addSubview(type2Label)
        type3Btn.addSubview(type3Label)
        type1Btn.addSubview(type1ImageView)
        type2Btn.addSubview(type2ImageView)
        type3Btn.addSubview(type3ImageView)
    }
    
    private func setConstraints(){
        bottomSafeAreaView.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalToSuperview()
            make.top.equalTo(backView.snp.bottom)
        }
        
        backView.snp.makeConstraints { make in
            make.height.equalTo(305)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }
        
        topLineView.snp.makeConstraints { make in
            make.height.equalTo(5)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(8)
            make.width.equalTo(47)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.height.equalTo(30)
            make.top.equalToSuperview().inset(28)
        }
        
        type1Btn.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(24)
            make.top.equalTo(titleLabel.snp.bottom).inset(-32)
            make.height.equalTo(30)
        }
        
        type1ImageView.snp.makeConstraints { make in
            make.height.width.equalTo(30)
            make.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        type1Label.snp.makeConstraints { make in
            make.leading.top.bottom.equalToSuperview()
            make.trailing.equalTo(type1ImageView.snp.leading).inset(-8)
        }
        
        // -
        
        type2Btn.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(24)
            make.top.equalTo(type1Btn.snp.bottom).inset(-16)
            make.height.equalTo(30)
        }
        
        type2ImageView.snp.makeConstraints { make in
            make.height.width.equalTo(30)
            make.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        type2Label.snp.makeConstraints { make in
            make.leading.top.bottom.equalToSuperview()
            make.trailing.equalTo(type2ImageView.snp.leading).inset(-8)
        }
        
        // -
        
        type3Btn.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(24)
            make.top.equalTo(type2Btn.snp.bottom).inset(-16)
            make.height.equalTo(30)
        }
        
        type3ImageView.snp.makeConstraints { make in
            make.height.width.equalTo(30)
            make.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        type3Label.snp.makeConstraints { make in
            make.leading.top.bottom.equalToSuperview()
            make.trailing.equalTo(type3ImageView.snp.leading).inset(-8)
        }
        
        // -
        
        textLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(24)
            make.top.equalTo(type3Btn.snp.bottom).inset(-16)
            make.bottom.equalToSuperview().inset(11)
        }
    }
    
    // MARK: - Gestures
    
    private func setGestures(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(closeAction))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapGesture)
    }
    
    // MARK: -
    
    private func setSelection(){
        let type = User.shared.importType!
        
        self.type1ImageView.image = "unchecked".img()
        self.type2ImageView.image = "unchecked".img()
        self.type3ImageView.image = "unchecked".img()
        
        switch type{
        case .askToDeleteOrLeave:
            self.type1ImageView.image = "checked".img()
        case .alwaysDelete:
            self.type2ImageView.image = "checked".img()
        case .alwaysLeave:
            self.type3ImageView.image = "checked".img()
        }
    }
}
