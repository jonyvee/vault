//
//  RateViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 26.02.2022.
//

import UIKit
import SnapKit

protocol RateProtocol{
    func rateAction()
    func closeAction()
}

class RateViewController: UIViewController{
    
    // MARK: - Properties
    
    private let bottomSafeAreaView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    private let backView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        view.layer.cornerCurve = .continuous
        view.layer.cornerRadius = 24
        return view
    }()
    
    private let topLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(rgb: 0xF6F6F6)
        view.layer.cornerRadius = 2.5
        return view
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = Font.bold.size(24)
        label.textAlignment = .center
        label.textColor = UIColor(rgb: 0x23262F)
        label.text = "Rate the app"
        return label
    }()
    
    private let imageView: UIImageView = {
        let imageView = UIImageView(image: "rateapp".img())
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let textLabel: UILabel = {
        let label = UILabel()
        label.font = Font.regular.size(14)
        label.textAlignment = .center
        label.textColor = UIColor(rgb: 0x718096)
        label.text = "Rate the app"
        label.numberOfLines = 3
        label.adjustsFontSizeToFitWidth = true
        label.text = "Rate us and our app that-we've gotten\nbetter! We want to make your life better! Help\nus in our endeavors"
        return label
    }()
    
    private let rateBtn: UIButton = {
        let button = UIButton()
        button.layer.cornerCurve = .continuous
        button.layer.cornerRadius = 16
        button.setTitle("Rate us", for: .normal)
        button.backgroundColor = UIColor(rgb: 0x334BFF)
        button.titleLabel?.font = Font.bold.size(16)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(rateAction), for: .touchUpInside)
        return button
    }()
    
    // MARK: -
    
    private let viewModel: RateProtocol
    
    // MARK: - Initialization
    
    init(viewModel: RateProtocol){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        setupViews()
        setGestures()
        setConstraints()
    }
    
    // MARK: - Actions
    
    @objc private func closeAction(){
        viewModel.closeAction()
    }
    
    @objc private func rateAction(){
        viewModel.rateAction()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        self.view.addSubview(backView)
        self.view.addSubview(bottomSafeAreaView)
        backView.addSubview(topLineView)
        backView.addSubview(titleLabel)
        backView.addSubview(rateBtn)
        backView.addSubview(imageView)
        backView.addSubview(textLabel)
    }
    
    private func setConstraints(){
        bottomSafeAreaView.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalToSuperview()
            make.top.equalTo(backView.snp.bottom)
        }
        
        backView.snp.makeConstraints { make in
            make.height.equalTo(490)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }
        
        topLineView.snp.makeConstraints { make in
            make.height.equalTo(5)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(8)
            make.width.equalTo(47)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.height.equalTo(30)
            make.top.equalToSuperview().inset(28)
        }
        
        imageView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(titleLabel.snp.bottom).inset(-32)
            make.height.equalTo(220)
        }
        
        textLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16)
            make.top.equalTo(imageView.snp.bottom).inset(-24)
            make.bottom.equalTo(rateBtn.snp.top).inset(-32)
        }
                
        rateBtn.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16)
            make.height.equalTo(56)
            make.bottom.equalToSuperview().inset(8)
        }
    }
    
    // MARK: - Gestures
    
    private func setGestures(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(closeAction))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapGesture)
    }
}
