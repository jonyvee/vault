//
//  SettingsViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit
import SnapKit

protocol SettingsProtocol{
    var numberOfSections: Int {get}
    var numberOfRows: Int {get}
    
    func viewModel(at index: Int) -> SettingsCellPresentable
    func selectItem(at index: Int)
    func bannerTapped()
    
    func didTapFaceSwitcher(isOn: Bool)
    func didTapScreenSwitcher(isOn: Bool)
}

class SettingsViewController: UIViewController{
    
    // MARK: - Properties
    
    private let titleLabel: UILabel = {
        let title = UILabel()
        title.text = "Settings"
        title.font = Font.bold.size(24)
        title.textColor = UIColor(rgb: 0x1A202C)
        return title
    }()
    
    private lazy var bannerImageView: UIImageView = {
        let imageView = UIImageView(image: "banner".img())
        imageView.isHidden = User.shared.isPurchased!
        imageView.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(tapBannerAction))
        imageView.addGestureRecognizer(gesture)
        return imageView
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets.init(top: 19, left: 16, bottom: 20, right: 16)
        layout.itemSize = CGSize(width: UIDevice.width-32, height: 45)
        layout.minimumLineSpacing = 24
        
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(SettingsCell.self, forCellWithReuseIdentifier: SettingsCell.key)
        
        return collectionView
    }()
    
    // MARK: -
    
    private var viewModel: SettingsViewModel
    
    // MARK: - Initialization
    
    init(viewModel: SettingsViewModel){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setViews()
        setConstraints()
        
        viewModel.reloadCollectionView = {[weak self] in
            self?.collectionView.reloadData()
        }
    }
    
    // MARK: - Actions
    
    @objc private func tapBannerAction(){
        Vibration.soft.vibrate()
        viewModel.bannerTapped()
    }
    
    // MARK: - UI
    
    private func setViews(){
        view.addSubview(titleLabel)
        view.addSubview(bannerImageView)
        view.addSubview(collectionView)
    }
    
    private func setConstraints(){
        titleLabel.snp.makeConstraints { make in
            make.height.equalTo(30)
            make.leading.equalToSuperview().inset(16)
            make.top.equalTo(view.safeAreaLayoutGuide).inset(29)
        }
        
        bannerImageView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16)
            make.top.equalTo(titleLabel.snp.bottom).inset(-24)
            
            if User.shared.isPurchased == true{
                make.height.equalTo(0)
            }else{
                make.height.equalTo(bannerImageView.snp.width).multipliedBy(0.451895043731778)
            }
        }
        
        collectionView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(bannerImageView.snp.bottom).inset(-5)
        }
    }
}


extension SettingsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SettingsCell.key, for: indexPath as IndexPath) as! SettingsCell
                
        cell.didTapSwitcher = { [weak self] (isOn) in
            if indexPath.row == 0{ // face id
                self?.viewModel.didTapFaceSwitcher(isOn: isOn)
            } else if indexPath.row == 1{ // screen resctricts
                self?.viewModel.didTapScreenSwitcher(isOn: isOn)
            }
        }
        
        let presentable = viewModel.viewModel(at: indexPath.row)
        cell.configure(presentable: presentable)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.selectItem(at: indexPath.row)
    }
}
