//
//  FakePassViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 26.02.2022.
//

import UIKit
import SnapKit

protocol FakePassProtocol{
    func closeAction()
    func switcherAction(isOn: Bool)
    func passcodeAction()
}

class FakePassViewController: UIViewController{
    
    // MARK: - Properties
    
    private let bottomSafeAreaView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    private let backView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        view.layer.cornerCurve = .continuous
        view.layer.cornerRadius = 24
        return view
    }()
    
    private let topLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(rgb: 0xF6F6F6)
        view.layer.cornerRadius = 2.5
        return view
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = Font.bold.size(24)
        label.textAlignment = .center
        label.textColor = UIColor(rgb: 0x23262F)
        label.text = "Fake passcode"
        return label
    }()
    
    private let switcherLabel: UILabel = {
        let label = UILabel()
        label.text = "Fake passcode"
        label.textColor = UIColor(rgb: 0x1A202C)
        label.font = Font.medium.size(18)
        return label
    }()
    
    private let switcher: UISwitch = {
        let switcher = UISwitch()
        switcher.tintColor = UIColor(rgb: 0xE3E6EA)
        switcher.onTintColor = UIColor(rgb: 0x334BFF)
        switcher.addTarget(self, action: #selector(switcherAction(_:)), for: .valueChanged)
        switcher.isOn = User.shared.isFakePasscoded!
        return switcher
    }()
    
    private let passcodeBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(passcodeAction), for: .touchUpInside)
        return button
    }()
    
    private let passcodeLabel: UILabel = {
        let label = UILabel()
        label.text = "Change passcode"
        label.textColor = UIColor(rgb: 0x1A202C)
        label.font = Font.medium.size(18)
        return label
    }()
    
    private let passcodeSubLabel: UILabel = {
        let label = UILabel()
        label.text = "To enter the application"
        label.textColor = UIColor(rgb: 0x718096)
        label.font = Font.regular.size(14)
        return label
    }()
    
    private let passcodeImageView: UIImageView = {
        let imageView = UIImageView(image: "forward".img())
        return imageView
    }()
    
    // MARK: -
    
    private let viewModel: FakePassProtocol
    
    // MARK: - Initialization
    
    init(viewModel: FakePassProtocol){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        setupViews()
        setGestures()
        setConstraints()
    }
    
    // MARK: - Actions
    
    @objc private func closeAction(){
        Vibration.soft.vibrate()
        viewModel.closeAction()
    }
    
    @objc private func switcherAction(_ sender: UISwitch){
        Vibration.soft.vibrate()
        viewModel.switcherAction(isOn: sender.isOn)
    }
    
    @objc private func passcodeAction(){
        Vibration.soft.vibrate()
        viewModel.passcodeAction()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        self.view.addSubview(backView)
        self.view.addSubview(bottomSafeAreaView)
        backView.addSubview(topLineView)
        backView.addSubview(titleLabel)
        backView.addSubview(switcherLabel)
        backView.addSubview(switcher)
        backView.addSubview(passcodeBtn)
        passcodeBtn.addSubview(passcodeLabel)
        passcodeBtn.addSubview(passcodeSubLabel)
        passcodeBtn.addSubview(passcodeImageView)
    }
    
    private func setConstraints(){
        bottomSafeAreaView.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalToSuperview()
            make.top.equalTo(backView.snp.bottom)
        }
        
        backView.snp.makeConstraints { make in
            make.height.equalTo(193)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }
        
        topLineView.snp.makeConstraints { make in
            make.height.equalTo(5)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(8)
            make.width.equalTo(47)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.height.equalTo(30)
            make.top.equalToSuperview().inset(28)
        }
        
        switcherLabel.snp.makeConstraints { make in
            make.height.equalTo(31)
            make.leading.equalToSuperview().inset(24)
            make.top.equalTo(titleLabel.snp.bottom).inset(-32)
            make.trailing.equalTo(switcher.snp.leading)
        }
        
        switcher.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(24)
            make.centerY.equalTo(switcherLabel)
        }
        
        passcodeBtn.snp.makeConstraints { make in
            make.height.equalTo(45)
            make.leading.trailing.equalToSuperview().inset(24)
            make.top.equalTo(switcherLabel.snp.bottom).inset(-16)
        }
        
        passcodeImageView.snp.makeConstraints { make in
            make.width.height.equalTo(30)
            make.centerY.trailing.equalToSuperview()
        }
        
        passcodeLabel.snp.makeConstraints { make in
            make.height.equalTo(25)
            make.leading.top.equalToSuperview()
            make.trailing.equalTo(passcodeImageView.snp.leading)
        }
        
        passcodeSubLabel.snp.makeConstraints { make in
            make.height.equalTo(20)
            make.leading.bottom.equalToSuperview()
            make.trailing.equalTo(passcodeImageView.snp.leading)
        }
    }
    
    // MARK: - Gestures
    
    private func setGestures(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(closeAction))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapGesture)
    }
    
}
