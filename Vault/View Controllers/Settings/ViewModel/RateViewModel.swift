//
//  RateViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 26.02.2022.
//

import UIKit
import StoreKit


class RateViewModel: RateProtocol{
    
    // MARK: - Properties
        
    var didClose: (() -> ())?
    
    
    // MARK: - Methods
    
    func rateAction() {
        if #available(iOS 14.0, *) {
            if let windowScene = UIApplication.shared.windowScene{
                SKStoreReviewController.requestReview(in: windowScene)
            }
        } else {
            SKStoreReviewController.requestReview()
        }
        
        Analitika.report("rateViewRateAction")
    }
    
    func closeAction() {
        didClose?()
        Analitika.report("rateViewCloseAction")
    }
    
}
