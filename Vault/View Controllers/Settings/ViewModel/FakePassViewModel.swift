//
//  FakePassViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 26.02.2022.
//

import Foundation


class FakePassViewModel: FakePassProtocol{
    
    // MARK: - Properties
    
    var didClose: (() -> ())?
    var didTapCreatePasscode: (() -> ())?
    
    // MARK: - Methods
    
    func closeAction() {
        didClose?()
        Analitika.report("fakePasscodeCloseAction")
    }
    
    func switcherAction(isOn: Bool) {
        User.shared.isFakePasscoded = isOn
        Analitika.report("fakePasscodeTurn\(isOn ? "On" : "Off")Action")
    }
    
    func passcodeAction() {
        didClose?()
        didTapCreatePasscode?()
        
        Analitika.report("fakePasscodeAction")
    }
    
}
