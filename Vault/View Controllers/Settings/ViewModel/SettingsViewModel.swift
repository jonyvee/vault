//
//  SettingsViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 19.02.2022.
//

import Foundation
import StoreKit

class SettingsViewModel: SettingsProtocol, LocalAuthenticationManager{
       
    // MARK: - Properties
    
    var didOpenPurchase: (() -> ())?
    var didOpen: ((SettingsData.SettingType) -> ())?
    
    // MARK: - 
    
    var reloadCollectionView: (() -> ())?
    
    // MARK: -
    
    private var data = SettingsData()
    
    
    // MARK: - Public Methods
    
    var numberOfSections: Int{
        1
    }
    
    var numberOfRows: Int{
        User.shared.isFakeLogin ? data.fakeItems.count : data.items.count
    }
    
    func viewModel(at index: Int) -> SettingsCellPresentable{
        User.shared.isFakeLogin ? data.fakeItems[index] : data.items[index]
    }
    
    func bannerTapped() {
        didOpenPurchase?()
    }
    
    func selectItem(at index: Int) {
        let item = User.shared.isFakeLogin ? data.fakeItems[index] : data.items[index]
        let type = item.type
            
        switch type {
        case .support, .privacy, .terms, .rate, .vault, .fakePasscode, .changePasscode:
            didOpen?(type)
        default:
            break;
        }
        
        Analitika.report("settings\(type.rawValue)Action")
    }
    
    func didTapFaceSwitcher(isOn: Bool) {
        isWillResignActivePresentation = false
        
        if isOn{
            if self.hasFaceId(){
                loginWithFaceId {[weak self] (success) in
                    User.shared.hasFaceId = success
                    self?.reloadSettingsData()
                    DispatchQueue.main.async {
                        self?.reloadCollectionView?()
                    }
                }
            }else{
                User.shared.hasFaceId = false
                self.reloadSettingsData()
                reloadCollectionView?()
            }
        }else{
            User.shared.hasFaceId = false
            self.reloadSettingsData()
            reloadCollectionView?()
        }
        
        Analitika.report("settingsFaceIDAction")
    }
    
    func didTapScreenSwitcher(isOn: Bool) {
        if User.shared.isPurchased == true{
            User.shared.hasScreenRestriction = isOn
            Analitika.report("settingsScreenResctrictAction")
        }else{
            didOpenPurchase?()
            reloadCollectionView?()
        }
    }
    
    // MARK: - Helper
    
    private func reloadSettingsData(){
        data = SettingsData()
    }
}
