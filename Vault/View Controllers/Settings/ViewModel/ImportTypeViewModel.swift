//
//  ImportTypeViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 26.02.2022.
//

import UIKit


class ImportTypeViewModel: ImportTypeProtocol{
    
    var didClose: (() -> ())?
    
    // MARK: - Types
    
    enum ImportType: Int{
        case askToDeleteOrLeave
        case alwaysDelete
        case alwaysLeave
    }
    
    // MARK: - Methods
    
    func closeAction() {
        didClose?()
        Analitika.report("AmportTypeCloseAction")
    }
    
    func chooseType(_ index: Int) {
        User.shared.importType = ImportType(rawValue: index)
        
        switch User.shared.importType!{
        case .askToDeleteOrLeave:
            Analitika.report("ImportTypeAskToDeleteOrKeepAction")
        case .alwaysDelete:
            Analitika.report("ImportTypeAlwaysDeleteAction")
        case .alwaysLeave:
            Analitika.report("ImportTypeAlwaysKeepAction")
        }
        
    }
}
