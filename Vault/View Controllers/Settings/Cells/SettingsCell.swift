////
////  SettingsTableViewCell.swift
////  Vault
////
////  Created by Nikola Cvetkovic on 21.02.2022.
////

import UIKit
import SnapKit

class SettingsCell: UICollectionViewCell{

    // MARK: - Static
    
    static let key = "SettingsCell"

    // MARK: - Properties

    private let photoImageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = Font.medium.size(18)
        label.textColor = UIColor(rgb: 0x1A202C)
        return label
    }()
    
    private let subTitleLabel: UILabel = {
        let label = UILabel()
        label.font = Font.regular.size(14)
        label.textColor = UIColor(rgb: 0x718096)
        return label
    }()
    
    private let arrowImageView: UIImageView = {
        let imageView = UIImageView(image: "settingsArrow".img())
        return imageView
    }()
    
    private lazy var switcher: UISwitch = {
        let switcher = UISwitch()
        switcher.onTintColor = UIColor(rgb: 0x334BFF)
        switcher.tintColor = UIColor(rgb: 0xE3E6EA)
        switcher.addTarget(self, action: #selector(switcherAction(_:)), for: .valueChanged)
        return switcher
    }()
    
    // MARK: -
    
    var didTapSwitcher: ((Bool) -> ())?
    
    // MARK: - Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configuration
    
    func configure(presentable: SettingsCellPresentable){
        self.photoImageView.image = presentable.icon
        self.titleLabel.text = presentable.title
        self.subTitleLabel.text = presentable.subtitle
        self.switcher.isHidden = !presentable.isSwitcher
        self.arrowImageView.isHidden = presentable.isSwitcher
        self.switcher.isOn = presentable.isOn ?? false
    }
    
    // MARK: - Actions
    
    @objc private func switcherAction(_ sender: UISwitch){
        didTapSwitcher?(sender.isOn)
    }
    
    // MARK: - UI

    private func setupViews(){
        contentView.addSubview(photoImageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(subTitleLabel)
        contentView.addSubview(arrowImageView)
        contentView.addSubview(switcher)
    }
    
    private func setConstraints(){
        photoImageView.snp.makeConstraints { make in
            make.height.width.equalTo(32)
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.height.equalTo(25)
            make.leading.equalTo(photoImageView.snp.trailing).inset(-12)
            make.top.equalToSuperview()
        }
        
        subTitleLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom)
            make.leading.equalTo(photoImageView.snp.trailing).inset(-12)
            make.bottom.equalToSuperview()
        }
        
        arrowImageView.snp.makeConstraints { make in
            make.height.width.equalTo(30)
            make.trailing.equalToSuperview().inset(8)
        }
        
        switcher.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview()
        }
    }
}
