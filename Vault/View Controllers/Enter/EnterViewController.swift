//
//  EnterViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 20.02.2022.
//

import UIKit
import SnapKit
import Combine

class EnterViewController: UIViewController{
    
    // MARK: - Properties
    
    private let codeStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.spacing = 16
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private let code1View: UIView = {
        return UIView().setCodeView()
    }()
    
    private let code2View: UIView = {
        return UIView().setCodeView()
    }()
    
    private let code3View: UIView = {
        return UIView().setCodeView()
    }()
    
    private let code4View: UIView = {
        return UIView().setCodeView()
    }()
    
    private let code1Label: UILabel = {
        return UILabel().setCodeLabel()
    }()
    
    private let code2Label: UILabel = {
        return UILabel().setCodeLabel()
    }()
    
    private let code3Label: UILabel = {
        return UILabel().setCodeLabel()
    }()
    
    private let code4Label: UILabel = {
        return UILabel().setCodeLabel()
    }()
    
    private let codeCenteringView = UIView()
    
    private let lockImageView: UIImageView = {
        let imageView = UIImageView(image: "lock".img())
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Create passcode"
        label.textColor = UIColor(rgb: 0x1A202C)
        label.textAlignment = .center
        label.font = Font.bold.size(24)
        return label
    }()
    
    private let subtitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Please, create strong passcode\nand remember him"
        label.textColor = UIColor(rgb: 0x1A202C)
        label.textAlignment = .center
        label.numberOfLines = 2
        label.font = Font.regular.size(16)
        return label
    }()
    
    private let buttonsVStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private lazy var faceIdImageView: UIImageView = {
        let imageView = UIImageView(image: viewModel.isFaceIdHidden ? "".img() : "faceid".img())
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(faceIdAction))
        imageView.addGestureRecognizer(tapGesture)
        return imageView
    }()
    
    private lazy var deleteIdImageView: UIImageView = {
        let imageView = UIImageView(image: "delete".img())
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(removeAction))
        imageView.addGestureRecognizer(tapGesture)
        return imageView
    }()
    
    private lazy var closeBtn: UIButton = {
        let button = UIButton()
        button.setImage("close".img(), for: .normal)
        button.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        button.isHidden = !viewModel.isCloseBtnShowed
        return button
    }()
    
    private var viewModel: EnterViewModel
    
    private var subscriptions: Set<AnyCancellable> = []
    
    // MARK: - Initialization
    
    init(viewModel: EnterViewModel){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        setConstraints()
        setButtonsHStackView()
        
        viewModel.$enteredPassword
            .sink { [weak self] (codes) in
                self?.changeCodeView(codes: codes)
            }.store(in: &subscriptions)
        
        viewModel.$isFirstEntering
            .sink { [weak self] (isFirstEntering) in
                self?.changeEntering(isFirstEntering: isFirstEntering, enterType: self?.viewModel.enterType ?? .registration)
            }.store(in: &subscriptions)
    }
    
    // MARK: - Actions
    
    @objc private func buttonAction(_ sender: UIButton){
        Vibration.soft.vibrate()
        viewModel.buttonTapped(sender.tag)
    }
    
    @objc private func faceIdAction(){
        if viewModel.isFaceIdHidden == false{
            Vibration.soft.vibrate()
            viewModel.faceIdTapped()
        }
    }
    
    @objc private func removeAction(){
        Vibration.soft.vibrate()
        viewModel.removeTapped()
    }
    
    @objc private func closeAction(){
        viewModel.closeAction()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        view.addSubview(buttonsVStackView)
        view.addSubview(lockImageView)
        view.addSubview(titleLabel)
        view.addSubview(subtitleLabel)
        view.addSubview(codeCenteringView)
        view.addSubview(closeBtn)
        codeCenteringView.addSubview(codeStackView)
        
        codeStackView.addArrangedSubview(code1View)
        codeStackView.addArrangedSubview(code2View)
        codeStackView.addArrangedSubview(code3View)
        codeStackView.addArrangedSubview(code4View)
        
        code1View.addSubview(code1Label)
        code2View.addSubview(code2Label)
        code3View.addSubview(code3Label)
        code4View.addSubview(code4Label)
    }
    
    private func setConstraints(){
        
        closeBtn.snp.makeConstraints { make in
            make.width.height.equalTo(48)
            make.leading.equalToSuperview().inset(16)
            make.top.equalTo(view.safeAreaLayoutGuide).inset(24)
        }
        
        buttonsVStackView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide)
            make.height.equalTo(320)
        }
        
        lockImageView.snp.makeConstraints { make in
            make.height.width.equalTo(64)
            make.centerX.equalToSuperview()
            make.top.equalTo(view.safeAreaLayoutGuide).inset(49)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.height.equalTo(30)
            make.leading.trailing.equalToSuperview().inset(24)
            make.top.equalTo(lockImageView.snp.bottom).inset(-32)
        }
        
        subtitleLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(24)
            make.top.equalTo(titleLabel.snp.bottom).inset(-16)
        }
        
        codeCenteringView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(subtitleLabel.snp.bottom)
            make.bottom.equalTo(buttonsVStackView.snp.top)
        }
        
        codeStackView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.width.equalTo(272)
            make.height.equalTo(56)
        }
        
        code1Label.snp.makeConstraints { make in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
        
        code2Label.snp.makeConstraints { make in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
        
        code3Label.snp.makeConstraints { make in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
        
        code4Label.snp.makeConstraints { make in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
    }
    
    private func setButtonsHStackView(){
        // 1
        let hStackView1 = UIStackView()
        hStackView1.setHStackView(to: buttonsVStackView)
        setButtons(to: hStackView1, numbers: [1,2,3])
        // 2
        let hStackView2 = UIStackView()
        hStackView2.setHStackView(to: buttonsVStackView)
        setButtons(to: hStackView2, numbers: [4,5,6])
        // 3
        let hStackView3 = UIStackView()
        hStackView3.setHStackView(to: buttonsVStackView)
        setButtons(to: hStackView3, numbers: [7,8,9])
        // 4
        let hStackView4 = UIStackView()
        hStackView4.setHStackView(to: buttonsVStackView)
        hStackView4.addArrangedSubview(faceIdImageView)
        setButton(to: hStackView4, number: 0)
        hStackView4.addArrangedSubview(deleteIdImageView)
    }
    
    // MARK: -
    
    private func changeCodeView(codes: [Int]){
        code1View.layer.borderColor = UIColor(rgb: 0xE2E8F0).cgColor
        code2View.layer.borderColor = UIColor(rgb: 0xE2E8F0).cgColor
        code3View.layer.borderColor = UIColor(rgb: 0xE2E8F0).cgColor
        code4View.layer.borderColor = UIColor(rgb: 0xE2E8F0).cgColor
        
        switch codes.count{
        case 0:
            code1View.layer.borderColor = UIColor(rgb: 0x334BFF).cgColor
        case 1:
            code2View.layer.borderColor = UIColor(rgb: 0x334BFF).cgColor
        case 2:
            code3View.layer.borderColor = UIColor(rgb: 0x334BFF).cgColor
        default:
            code4View.layer.borderColor = UIColor(rgb: 0x334BFF).cgColor
        }
        
        code1Label.text = codes[safe: 0] != nil ? "●" : ""
        code2Label.text = codes[safe: 1] != nil ? "●" : ""
        code3Label.text = codes[safe: 2] != nil ? "●" : ""
        code4Label.text = codes[safe: 3] != nil ? "●" : ""
    }
    
    // MARK: -
    
    private func changeEntering(isFirstEntering: Bool, enterType: EnterViewModel.EnterType){
        if enterType == .registration || enterType == .registrationWithClose{
            if isFirstEntering{
                self.titleLabel.text = "Create passcode"
                self.subtitleLabel.text = "Please, create strong passcode\nand remember him"
            }else{
                self.titleLabel.text = "Repeat passcode"
                self.subtitleLabel.text = "Please, repeat strong passcode\nand remember him"
            }
        }else if enterType == .login{
            self.titleLabel.text = "Enter passcode"
            self.subtitleLabel.text = "Please, enter passcode to login"
        }else if enterType == .fakePasscode{
            if isFirstEntering{
                self.titleLabel.text = "Create fake passcode"
                self.subtitleLabel.text = "Please, create fake passcode\nand remember him"
            }else{
                self.titleLabel.text = "Repeat fake passcode"
                self.subtitleLabel.text = "Please, repeat fake passcode\nand remember him"
            }
        }
    }
    
    // MARK: - Helper
    
    private func setButtons(to stackView: UIStackView, numbers: [Int]){
        numbers.forEach { number in
            setButton(to: stackView, number: number)
        }
    }
    
    private func setButton(to stackView: UIStackView, number: Int){
        let button = UIButton()
        button.tag = number
        button.setTitle(number.description, for: .normal)
        button.setTitleColor(UIColor(rgb: 0x1A202C), for: .normal)
        button.titleLabel?.font = Font.semibold.size(28)
        button.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        stackView.addArrangedSubview(button)
    }
}

// MARK: - Helper

fileprivate extension UIStackView{
    func setHStackView(to stackView: UIStackView){
        stackView.addArrangedSubview(self)
        self.alignment = .fill
        self.axis = .horizontal
        self.distribution = .fillEqually
    }
}

fileprivate extension UILabel{
    func setCodeLabel() -> UILabel{
        self.textColor = UIColor(rgb: 0x000000)
        self.textAlignment = .center
        self.font = Font.semibold.size(15)
        return self
    }
}

fileprivate extension UIView{
    func setCodeView() -> UIView{
        self.layer.cornerRadius = 12
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(rgb: 0xE2E8F0).cgColor
        return self
    }
}
