//
//  EnterViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 20.02.2022.
//

import Foundation
import Combine
import RealmSwift

class EnterViewModel: LocalAuthenticationManager{
    
    // MARK: - Types
    
    enum EnterType{
        case login
        case registration
        case registrationWithClose
        case fakePasscode
    }
    
    // MARK: - Properties
    
    var didClose: (() -> ())?
    
    // MARK: - 
    
    let enterType: EnterType
    
    // MARK: -
    
    @Published  private(set) var enteredPassword: [Int] = []
    @Published  private(set) var isFirstEntering: Bool = true
    
    // MARK: -
    
    private var firstEnteredPassword: String = ""
    private var secondEnteredPassword: String = ""
    
    // MARK: - Initialization
    
    init(enterType: EnterType) {
        self.enterType = enterType
    }
    
    // MARK: - Methods
    
    var isFaceIdHidden: Bool{
        if enterType == .login{
            return User.shared.hasFaceId == false
        }
        return true
    }
    
    var isCloseBtnShowed: Bool{
        return enterType == .registrationWithClose || enterType == .fakePasscode
    }
    
    func buttonTapped(_ number: Int){
        if enteredPassword.count < 4{
            enteredPassword.append(number)
        }
        
        if enteredPassword.count == 4{
            if enterType == .login{
                enteredLoginPassword()
            }else{
                enteredRegistrationPassword()
            }
        }
    }
    
    func faceIdTapped(){
        loginWithFaceId { success in
            if success{
                DispatchQueue.main.async {
                    self.didClose?()
                }
            }
        }
    }
    
    func removeTapped(){
        if enteredPassword.isEmpty == false{
            enteredPassword.removeLast()
        }
    }
    
    func closeAction(){
        didClose?()
    }

    // MARK: - Private Methods
    
    private func enteredLoginPassword(){
        firstEnteredPassword.removeAll()
        enteredPassword.forEach({firstEnteredPassword.append($0.description)})
        if firstEnteredPassword == User.shared.password{
            
            if User.shared.isFakeLogin == true{
                User.shared.isFakeLogin = false
                
                var config = Realm.Configuration()
                config.fileURL = URL.documentDirectory?.appendingPathComponent("default.realm")
                realm = try! Realm(configuration: config, queue: .main)
                
                UIApplication.shared.keyWindow?.rootViewController = SceneDelegate.appCoordinator.rootViewController
                UIApplication.shared.keyWindow?.makeKeyAndVisible()
                SceneDelegate.appCoordinator.hasEnterCoordinator = false
                SceneDelegate.appCoordinator.start()
            }
            
            self.didClose?()
            
        }else if firstEnteredPassword == User.shared.fakePasscode! && User.shared.isFakePasscoded == true{

            if User.shared.isFakeLogin == false{
                User.shared.isFakeLogin = true
                var config = Realm.Configuration()
                config.fileURL = URL.documentDirectory?.appendingPathComponent("fakeData.realm")
                realm = try! Realm(configuration: config, queue: .main)
                
                UIApplication.shared.keyWindow?.rootViewController = SceneDelegate.appCoordinator.rootViewController
                UIApplication.shared.keyWindow?.makeKeyAndVisible()
                SceneDelegate.appCoordinator.hasEnterCoordinator = false
                SceneDelegate.appCoordinator.start()
            }
            
            self.didClose?()
        }else{
            Vibration.error.vibrate()
            JonyMessage.show(text: "Wrong passcode")
            enteredPassword = []
        }
    }
    
    private func enteredRegistrationPassword(){
        if isFirstEntering{
            enteredPassword.forEach({firstEnteredPassword.append($0.description)})
            enteredPassword = []
            isFirstEntering.toggle()
        }else{
            enteredPassword.forEach({secondEnteredPassword.append($0.description)})
            if firstEnteredPassword == secondEnteredPassword{
                
                if enterType == .fakePasscode{
                    User.shared.fakePasscode = firstEnteredPassword
                    User.shared.isFakePasscoded = true
                }else{
                    User.shared.password = firstEnteredPassword
                }
                
                self.didClose?()
            }else{
                JonyMessage.show(text: "Wrong passcode")
                firstEnteredPassword.removeAll()
                secondEnteredPassword.removeAll()
                isFirstEntering = true
                enteredPassword = []
            }
        }
    }
    
}
