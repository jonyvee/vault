//
//  GeneratePassViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 22.02.2022.
//

import UIKit
import SnapKit

class GeneratePassViewController: UIViewController{
    
    // MARK: - Properties
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x1A202C)
        label.textAlignment = .center
        label.font = Font.bold.size(24)
        label.text = "Generate"
        return label
    }()
    
    private let backBtn: UIButton = {
        let button = UIButton()
        button.setImage("pass_back".img(), for: .normal)
        button.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        return button
    }()
    
    private let saveBtn: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(rgb: 0x334BFF)
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Save", for: .normal)
        button.titleLabel?.font = Font.regular.size(14)
        button.layer.cornerRadius = 12
        button.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
        return button
    }()
    
    private let generateBtn: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(rgb: 0x334BFF)
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Generate new", for: .normal)
        button.titleLabel?.font = Font.bold.size(16)
        button.layer.cornerRadius = 16
        button.addTarget(self, action: #selector(generateAction), for: .touchUpInside)
        return button
    }()
    
    private let includingView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 16
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.1
        view.layer.shadowRadius = 7
        view.layer.shadowOffset = .zero
        return view
    }()
    
    private let numbersLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = Font.regular.size(16)
        label.text = "Numbers (0-9)"
        return label
    }()
    
    private let uppercaseLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = Font.regular.size(16)
        label.text = "Uppercase letters (A-Z)"
        return label
    }()
    
    private let lowercaseLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = Font.regular.size(16)
        label.text = "Lowercase letters (a-z)"
        return label
    }()
    
    private let symbolsLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = Font.regular.size(16)
        label.text = "Symbols (@!_$, etc.)"
        return label
    }()
    
    private let numberSwitcher: UISwitch = {
        let switcher = UISwitch()
        switcher.tag = 0
        switcher.tintColor = UIColor(rgb: 0xE3E6EA)
        switcher.onTintColor = UIColor(rgb: 0x334BFF)
        switcher.addTarget(self, action: #selector(switcherAction(_:)), for: .valueChanged)
        switcher.isOn = true
        return switcher
    }()
    
    private let uppercaseSwitcher: UISwitch = {
        let switcher = UISwitch()
        switcher.tag = 1
        switcher.tintColor = UIColor(rgb: 0xE3E6EA)
        switcher.onTintColor = UIColor(rgb: 0x334BFF)
        switcher.addTarget(self, action: #selector(switcherAction(_:)), for: .valueChanged)
        switcher.isOn = false
        return switcher
    }()
    
    private let lowercaseSwitcher: UISwitch = {
        let switcher = UISwitch()
        switcher.tag = 2
        switcher.tintColor = UIColor(rgb: 0xE3E6EA)
        switcher.onTintColor = UIColor(rgb: 0x334BFF)
        switcher.addTarget(self, action: #selector(switcherAction(_:)), for: .valueChanged)
        switcher.isOn = false
        return switcher
    }()
    
    private let symbolSwitcher: UISwitch = {
        let switcher = UISwitch()
        switcher.tag = 3
        switcher.tintColor = UIColor(rgb: 0xE3E6EA)
        switcher.onTintColor = UIColor(rgb: 0x334BFF)
        switcher.addTarget(self, action: #selector(switcherAction(_:)), for: .valueChanged)
        switcher.isOn = true
        return switcher
    }()
    
    private let includingLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x1A202C)
        label.font = Font.medium.size(18)
        label.text = "Including"
        return label
    }()
    
    private let lenghtView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 16
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.1
        view.layer.shadowRadius = 7
        view.layer.shadowOffset = .zero
        return view
    }()
    
    private let lenghtLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x1A202C)
        label.font = Font.medium.size(18)
        label.text = "Lenght: 16"
        return label
    }()
    
    private let centeringView: UIView = {
        let view = UIView()
        return view
    }()
    
    private let generatedPassLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x1A202C)
        label.font = Font.medium.size(18)
        label.text = "Generated password:"
        label.textAlignment = .center
        return label
    }()
    
    private let passwordLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x334BFF)
        label.font = Font.bold.size(24)
        label.text = "!Gf562d-fgh6571@"
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        return label
    }()
    
    private let slider: UISlider = {
        let slider = UISlider()
        slider.minimumValue = 4
        slider.maximumValue = 32
        slider.value = 16
        slider.maximumTrackTintColor = UIColor(rgb: 0xEDF2F7)
        slider.minimumTrackTintColor = UIColor(rgb: 0x455AF7)
        slider.setThumbImage("thumb".img(), for: .normal)
        slider.addTarget(self, action: #selector(sliderAction(_:)), for: .valueChanged)
        return slider
    }()
    
    private let lenghtStartLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = Font.bold.size(16)
        label.text = "4"
        label.textAlignment = .center
        return label
    }()
    
    private let lenghtEndLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = Font.bold.size(16)
        label.text = "32"
        label.textAlignment = .center
        return label
    }()
    
    // MARK: -
    
    private var viewModel: GeneratePassProtocol
    
    // MARK: - Initialization
    
    init(viewModel: GeneratePassProtocol){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        setupViews()
        setConstraints()
        
        viewModel.didChangeText = {[weak self] (text) in
            self?.passwordLabel.text = text
        }
        
        viewModel.generateAction()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        includingView.layer.shadowPath = UIBezierPath(roundedRect: includingView.bounds, cornerRadius: 16).cgPath
        lenghtView.layer.shadowPath = UIBezierPath(roundedRect: lenghtView.bounds, cornerRadius: 16).cgPath
    }
    
    // MARK: - Actions
    
    @objc private func backAction(){
        viewModel.backAction()
    }
    
    @objc private func saveAction(){
        viewModel.saveAction()
    }
    
    @objc private func generateAction(){
        viewModel.generateAction()
    }
    
    @objc private func switcherAction(_ sender: UISwitch){
        
        if sender.tag == 1{
            if uppercaseSwitcher.isOn{
                lowercaseSwitcher.isOn = false
            }
        }else if sender.tag == 2{
            if lowercaseSwitcher.isOn{
                uppercaseSwitcher.isOn = false
            }
        }
        
        viewModel.includingAction(numbers: numberSwitcher.isOn, uppercase: uppercaseSwitcher.isOn, lowercase: lowercaseSwitcher.isOn, symbols: symbolSwitcher.isOn)
    }
    
    @objc private func sliderAction(_ sender: UISlider){
        let lenght = Int(sender.value)
        lenghtLabel.text = "Lenght: \(lenght)"
        viewModel.changeLenghtAction(lenght: lenght)
    }
    
    // MARK: - UI
    
    private func setupViews(){
        view.addSubview(backBtn)
        view.addSubview(titleLabel)
        view.addSubview(saveBtn)
        view.addSubview(generateBtn)
        view.addSubview(includingView)
        view.addSubview(includingLabel)
        view.addSubview(lenghtLabel)
        view.addSubview(lenghtView)
        view.addSubview(centeringView)
        
        centeringView.addSubview(generatedPassLabel)
        centeringView.addSubview(passwordLabel)
        
        lenghtView.addSubview(lenghtStartLabel)
        lenghtView.addSubview(lenghtEndLabel)
        lenghtView.addSubview(slider)
        
        includingView.addSubview(numbersLabel)
        includingView.addSubview(numberSwitcher)
        includingView.addSubview(uppercaseLabel)
        includingView.addSubview(uppercaseSwitcher)
        includingView.addSubview(lowercaseLabel)
        includingView.addSubview(lowercaseSwitcher)
        includingView.addSubview(symbolsLabel)
        includingView.addSubview(symbolSwitcher)
    }
    
    private func setConstraints(){
        backBtn.snp.makeConstraints { make in
            make.height.width.equalTo(30)
            make.top.equalTo(view.safeAreaLayoutGuide).inset(24)
            make.leading.equalToSuperview().inset(16)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerY.equalTo(backBtn)
            make.centerX.equalToSuperview()
            make.height.equalTo(30)
        }
        
        saveBtn.snp.makeConstraints { make in
            make.height.equalTo(24)
            make.trailing.equalToSuperview().inset(16)
            make.width.equalTo(54)
            make.centerY.equalTo(backBtn)
        }
        
        generateBtn.snp.makeConstraints { make in
            make.height.equalTo(56)
            make.leading.trailing.equalToSuperview().inset(16)
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(18)
        }
        
        includingView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16)
            make.height.equalTo(200)
            make.bottom.equalTo(generateBtn.snp.top).inset(-32)
        }
        
        numbersLabel.snp.makeConstraints { make in
            make.height.equalTo(22)
            make.leading.equalToSuperview().inset(16)
            make.top.equalToSuperview().inset(17)
        }
        
        numberSwitcher.snp.makeConstraints { make in
            make.centerY.equalTo(numbersLabel)
            make.trailing.equalToSuperview().inset(16)
        }
        
        uppercaseLabel.snp.makeConstraints { make in
            make.height.equalTo(22)
            make.leading.equalToSuperview().inset(16)
            make.top.equalTo(numbersLabel.snp.bottom).inset(-24)
        }
        
        uppercaseSwitcher.snp.makeConstraints { make in
            make.centerY.equalTo(uppercaseLabel)
            make.trailing.equalToSuperview().inset(16)
        }
        
        lowercaseLabel.snp.makeConstraints { make in
            make.height.equalTo(22)
            make.leading.equalToSuperview().inset(16)
            make.top.equalTo(uppercaseSwitcher.snp.bottom).inset(-24)
        }
        
        lowercaseSwitcher.snp.makeConstraints { make in
            make.centerY.equalTo(lowercaseLabel)
            make.trailing.equalToSuperview().inset(16)
        }
        
        symbolsLabel.snp.makeConstraints { make in
            make.height.equalTo(22)
            make.leading.equalToSuperview().inset(16)
            make.top.equalTo(lowercaseSwitcher.snp.bottom).inset(-24)
        }
        
        symbolSwitcher.snp.makeConstraints { make in
            make.centerY.equalTo(symbolsLabel)
            make.trailing.equalToSuperview().inset(16)
        }
        
        includingLabel.snp.makeConstraints { make in
            make.height.equalTo(25)
            make.leading.equalToSuperview().inset(16)
            make.bottom.equalTo(includingView.snp.top).inset(-8)
        }
        
        lenghtView.snp.makeConstraints { make in
            make.height.equalTo(82)
            make.leading.trailing.equalToSuperview().inset(16)
            make.bottom.equalTo(includingLabel.snp.top).inset(-24)
        }
        
        lenghtLabel.snp.makeConstraints { make in
            make.height.equalTo(25)
            make.leading.equalToSuperview().inset(16)
            make.bottom.equalTo(lenghtView.snp.top).inset(-8)
        }
        
        centeringView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(backBtn.snp.bottom)
            make.bottom.equalTo(lenghtLabel.snp.top)
        }
        
        generatedPassLabel.snp.makeConstraints { make in
            make.height.equalTo(25)
            make.leading.trailing.equalToSuperview().inset(16)
            make.centerY.equalToSuperview().inset(-19)
        }
        
        passwordLabel.snp.makeConstraints { make in
            make.height.equalTo(30)
            make.leading.trailing.equalToSuperview().inset(16)
            make.top.equalTo(generatedPassLabel.snp.bottom).inset(-8)
        }
        
        slider.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16)
            make.bottom.equalToSuperview().inset(16)
        }
        
        lenghtStartLabel.snp.makeConstraints { make in
            make.height.equalTo(22)
            make.leading.equalToSuperview().inset(16)
            make.bottom.equalTo(slider.snp.top).inset(10)
        }
        
        lenghtEndLabel.snp.makeConstraints { make in
            make.height.equalTo(22)
            make.trailing.equalToSuperview().inset(16)
            make.bottom.equalTo(slider.snp.top).inset(10)
        }
    }
    
}
