//
//  PassDetailViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 23.02.2022.
//

import UIKit
import SnapKit

protocol PassDetailProtocol{
    var title: String {get}
    var login: String {get}
    var url: String {get}
    var password: String {get}
    var isFavorite: Bool {get}
    var didUpdatePasswordField: ((String) -> ())?{set get}
    
    func backAction()
    func favouriteAction()
    func saveAction()
    func generateAction()
    func updateData(title: String, login: String, url: String, password: String)
    func deleteAction()
}

class PassDetailViewController: UIViewController{
    
    // MARK: - Properties
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.contentSize = CGSize(width: UIDevice.width, height: 660)
        return scrollView
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x1A202C)
        label.textAlignment = .center
        label.font = Font.bold.size(24)
        label.text = "Generate"
        return label
    }()
    
    private let backBtn: UIButton = {
        let button = UIButton()
        button.setImage("pass_back".img(), for: .normal)
        button.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        return button
    }()
    
    private let saveBtn: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(rgb: 0x718096)
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Save", for: .normal)
        button.titleLabel?.font = Font.regular.size(14)
        button.layer.cornerRadius = 12
        button.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
        return button
    }()
    
    private let heartBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(heartAction), for: .touchUpInside)
        return button
    }()
    
    private let titleTextLabel: UILabel = {
        let label = UILabel()
        label.text = "Title"
        label.textColor = UIColor(rgb: 0x718096)
        label.font = Font.regular.size(14)
        return label
    }()
    
    private lazy var titleTextField: UITextField = {
        let textField = UITextField()
        textField.addTarget(self, action: #selector(textFieldAction), for: .editingChanged)
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 16
        textField.layer.borderColor = UIColor(rgb: 0xA0AEC0).cgColor
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: 16.78, height: 56)
        textField.leftView = leftView
        textField.leftViewMode = .always
        textField.placeholder = "Google"
        textField.autocorrectionType = .no
        textField.delegate = self
        return textField
    }()
    
    private let loginTextLabel: UILabel = {
        let label = UILabel()
        label.text = "Login"
        label.textColor = UIColor(rgb: 0x718096)
        label.font = Font.regular.size(14)
        return label
    }()
    
    private lazy var loginTextField: UITextField = {
        let textField = UITextField()
        textField.addTarget(self, action: #selector(textFieldAction), for: .editingChanged)
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 16
        textField.layer.borderColor = UIColor(rgb: 0xA0AEC0).cgColor
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: 16.78, height: 56)
        textField.leftView = leftView
        textField.leftViewMode = .always
        textField.placeholder = "example@gmail.com"
        textField.autocorrectionType = .no
        textField.delegate = self
        return textField
    }()
    
    private let urlTextLabel: UILabel = {
        let label = UILabel()
        label.text = "URL"
        label.textColor = UIColor(rgb: 0x718096)
        label.font = Font.regular.size(14)
        return label
    }()
    
    private lazy var urlTextField: UITextField = {
        let textField = UITextField()
        textField.addTarget(self, action: #selector(textFieldAction), for: .editingChanged)
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 16
        textField.layer.borderColor = UIColor(rgb: 0xA0AEC0).cgColor
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: 16.78, height: 56)
        textField.leftView = leftView
        textField.leftViewMode = .always
        textField.placeholder = "https://www.google.ru/"
        textField.autocorrectionType = .no
        textField.delegate = self
        return textField
    }()
    
    private let passwordTextLabel: UILabel = {
        let label = UILabel()
        label.text = "Password"
        label.textColor = UIColor(rgb: 0x718096)
        label.font = Font.regular.size(14)
        return label
    }()
    
    private lazy var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.addTarget(self, action: #selector(textFieldAction), for: .editingChanged)
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 16
        textField.layer.borderColor = UIColor(rgb: 0xA0AEC0).cgColor
        textField.delegate = self
        
        let rightView = UIView(frame: CGRect(x: 0, y: 0, width: 82, height: 56))
        let generateBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 65, height: 56))
        generateBtn.addTarget(self, action: #selector(generateAction), for: .touchUpInside)
        generateBtn.setTitle("Generate", for: .normal)
        generateBtn.contentHorizontalAlignment = .left
        generateBtn.setTitleColor(UIColor(rgb: 0x334BFF), for: .normal)
        generateBtn.titleLabel?.font = Font.regular.size(16)
        rightView.addSubview(generateBtn)
        
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: 16.78, height: 56)
        
        textField.leftView = leftView
        textField.rightView = rightView
        textField.leftViewMode = .always
        textField.rightViewMode = .always
        textField.placeholder = "https://www.google.ru/"
        textField.autocorrectionType = .no
        return textField
    }()
    
    private lazy var deleteBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(deleteAction), for: .touchUpInside)
        button.setTitle("Delete password", for: .normal)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.titleLabel?.font = Font.regular.size(15)
        return button
    }()
    
    // MARK: -
    
    private var viewModel: PassDetailProtocol
    
    // MARK: - Initialization
    
    init(viewModel: PassDetailProtocol){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        setConstraints()
        setData()
        
        viewModel.didUpdatePasswordField = {[weak self] (password) in
            self?.passwordTextField.text = password
        }
    }
    
    // MARK: - Actions
    
    @objc private func backAction(){
        viewModel.backAction()
    }
    
    @objc private func saveAction(){
        viewModel.saveAction()
    }
    
    @objc private func deleteAction(){
        viewModel.deleteAction()
    }
    
    @objc private func heartAction(){
        viewModel.favouriteAction()
        self.heartBtn.setImage(viewModel.isFavorite ? "heartfill".img() : "heart".img(), for: .normal)
        saveBtn.backgroundColor = UIColor(rgb: 0x334BFF)
    }
    
    @objc private func generateAction(){
        viewModel.generateAction()
        saveBtn.backgroundColor = UIColor(rgb: 0x334BFF)
    }
    
    @objc private func textFieldAction(){
        viewModel.updateData(title: titleTextField.text ?? "", login: loginTextField.text ?? "", url: urlTextField.text ?? "", password: passwordTextField.text ?? "")
        saveBtn.backgroundColor = UIColor(rgb: 0x334BFF)
    }
    
    // MARK: - UI
    
    private func setupViews(){
        view.addSubview(backBtn)
        view.addSubview(titleLabel)
        view.addSubview(saveBtn)
        view.addSubview(heartBtn)
        view.addSubview(scrollView)
        scrollView.addSubview(titleTextLabel)
        scrollView.addSubview(titleTextField)
        scrollView.addSubview(loginTextLabel)
        scrollView.addSubview(loginTextField)
        scrollView.addSubview(urlTextLabel)
        scrollView.addSubview(urlTextField)
        scrollView.addSubview(passwordTextLabel)
        scrollView.addSubview(passwordTextField)
        scrollView.addSubview(deleteBtn)
    }
    
    private func setConstraints(){
        backBtn.snp.makeConstraints { make in
            make.height.width.equalTo(30)
            make.top.equalTo(view.safeAreaLayoutGuide).inset(24)
            make.leading.equalToSuperview().inset(16)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerY.equalTo(backBtn)
            make.centerX.equalToSuperview()
            make.trailing.equalTo(heartBtn.snp.leading).inset(-5)
            make.height.equalTo(30)
        }
        
        saveBtn.snp.makeConstraints { make in
            make.height.equalTo(24)
            make.trailing.equalToSuperview().inset(16)
            make.width.equalTo(54)
            make.centerY.equalTo(backBtn)
        }
        
        heartBtn.snp.makeConstraints { make in
            make.height.width.equalTo(24)
            make.centerY.equalTo(backBtn)
            make.trailing.equalTo(saveBtn.snp.leading).inset(-16)
        }
        
        scrollView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(backBtn.snp.bottom).inset(-5)
        }
        
        titleTextLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(16)
            make.width.equalTo(UIDevice.width-32)
            make.height.equalTo(20)
            make.top.equalToSuperview().inset(9)
        }
        
        titleTextField.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleTextLabel)
            make.height.equalTo(56)
            make.top.equalTo(titleTextLabel.snp.bottom).inset(-8)
        }
        
        loginTextLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleTextLabel)
            make.width.equalTo(UIDevice.width-32)
            make.height.equalTo(20)
            make.top.equalTo(titleTextField.snp.bottom).inset(-16)
        }
        
        loginTextField.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleTextLabel)
            make.height.equalTo(56)
            make.top.equalTo(loginTextLabel.snp.bottom).inset(-8)
        }
        
        urlTextLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleTextLabel)
            make.width.equalTo(UIDevice.width-32)
            make.height.equalTo(20)
            make.top.equalTo(loginTextField.snp.bottom).inset(-16)
        }
        
        urlTextField.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleTextLabel)
            make.height.equalTo(56)
            make.top.equalTo(urlTextLabel.snp.bottom).inset(-8)
        }
        
        passwordTextLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleTextLabel)
            make.width.equalTo(UIDevice.width-32)
            make.height.equalTo(20)
            make.top.equalTo(urlTextField.snp.bottom).inset(-16)
        }
        
        passwordTextField.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleTextLabel)
            make.height.equalTo(56)
            make.top.equalTo(passwordTextLabel.snp.bottom).inset(-8)
        }
        
        deleteBtn.snp.makeConstraints { make in
            make.height.equalTo(35)
            make.width.equalTo(200)
            make.centerX.equalTo(titleTextField)
            make.top.equalTo(passwordTextField.snp.bottom).inset(-8)
        }
    }
    
    // MARK: -
    
    private func setData(){
        self.titleLabel.text = viewModel.title
        self.titleTextField.text = viewModel.title
        self.loginTextField.text = viewModel.login
        self.urlTextField.text = viewModel.url
        self.passwordTextField.text = viewModel.password
        self.heartBtn.setImage(viewModel.isFavorite ? "heartfill".img() : "heart".img(), for: .normal)
    }
}

// MARK: - UITextFieldDelegate

extension PassDetailViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
