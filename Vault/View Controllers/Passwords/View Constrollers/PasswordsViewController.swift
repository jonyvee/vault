//
//  PasswordsViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit
import SnapKit

protocol PasswordsProtocol{
    var isFavourite: Bool {get set}
    var searchText: String {get set}
    var reloadCollectionView: ((Int) -> ())? {set get}
    var numberOfSections: Int{get}
    var numberOfRows: Int{get}
    
    func viewModel(at index: Int) -> PasswordCellPresentable
    func updatePasswordHidden(isHidden: Bool, at index: Int)
    func showPasswordAdd()
    func didSelectItem(at index: Int)
}

class PasswordsViewController: UIViewController{
    
    // MARK: - Properties
    
    private let navigationView: AddNavigationView = {
        let navigation = AddNavigationView(title: "Passwords")
        return navigation
    }()
    
    private lazy var plugImageView: UIImageView = {
        let imageView = UIImageView(image: "passwordsnone".img())
        imageView.contentMode = .scaleAspectFit
        imageView.isHidden = viewModel.numberOfRows > 0
        return imageView
    }()
    
    private lazy var textField: UITextField = {
        let textField = UITextField()
        textField.addTarget(self, action: #selector(textFieldAction), for: .editingChanged)
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 16
        textField.layer.borderColor = UIColor(rgb: 0xA0AEC0).cgColor
        let imageView = UIImageView(image: "search".img())
        imageView.frame = CGRect(x: 16, y: 18, width: 20, height: 20)
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: 48, height: 56)
        leftView.addSubview(imageView)
        textField.leftView = leftView
        textField.leftViewMode = .always
        textField.placeholder = "Search Passwords"
        textField.autocorrectionType = .no
        textField.delegate = self
        return textField
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.keyboardDismissMode = .onDrag
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets.init(top: 19, left: 16, bottom: 20, right: 16)
        layout.itemSize = CGSize(width: UIDevice.width-32, height: 100)
        layout.minimumLineSpacing = 16
        
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(PasswordCell.self, forCellWithReuseIdentifier: PasswordCell.key)
        
        return collectionView
    }()
    
    private var viewModel: PasswordsProtocol
    
    // MARK: - Initialization
    
    init(viewModel: PasswordsProtocol){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        setConstraints()
        
        
        viewModel.reloadCollectionView = { [weak self] (numberOfRows) in
            self?.plugImageView.isHidden = numberOfRows > 0
            self?.collectionView.reloadData()
        }
        
        navigationView.didTapAddBtn = { [weak self] in
            print(1)
            self?.viewModel.showPasswordAdd()
        }
        
        navigationView.didTapAllBtn = { [weak self] in
            print(2)
            self?.viewModel.isFavourite = false
        }
        
        navigationView.didTapFavoriteBtn = { [weak self] in
            print(3)
            self?.viewModel.isFavourite = true
        }
        
        if User.shared.isPurchased == false{
            AdManager.setBanner(rootVC: self)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.collectionView.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        view.endEditing(true)
    }
    
    // MARK: - Actions
    
    @objc private func textFieldAction(){
        viewModel.searchText = textField.text ?? ""
    }
    
    // MARK: - UI
    
    private func setupViews(){
        view.addSubview(navigationView)
        view.addSubview(plugImageView)
        view.addSubview(textField)
        view.addSubview(collectionView)
    }
    
    private func setConstraints(){
        navigationView.snp.makeConstraints { make in
            make.height.equalTo(AddNavigationView.height)
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(view.safeAreaLayoutGuide)
        }
        
        plugImageView.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalToSuperview()
            make.top.equalTo(textField.snp.bottom)
        }
        
        textField.snp.makeConstraints { make in
            make.height.equalTo(56)
            make.leading.trailing.equalToSuperview().inset(16)
            make.top.equalTo(navigationView.snp.bottom).inset(-16)
        }
        
        collectionView.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalToSuperview()
            make.top.equalTo(textField.snp.bottom).inset(-5)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}


extension PasswordsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PasswordCell.key, for: indexPath as IndexPath) as! PasswordCell
        
        cell.didHidePassword = { [weak self] (isHidden) in
            self?.viewModel.updatePasswordHidden(isHidden: isHidden, at: indexPath.row)
        }
        
        let presentable = viewModel.viewModel(at: indexPath.row)
        cell.configure(with: presentable)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.didSelectItem(at: indexPath.row)
    }
}

// MARK: - UITextFieldDelegate

extension PasswordsViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
