//
//  PasswordAddViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 22.02.2022.
//

import UIKit
import SnapKit

protocol PasswordAddProtocol{
    func generateAction()
    func doneAction()
    func closeAction(animate: Bool)
    func updateData(title: String, login: String, url: String, password: String)
    
    var didUpdatePasswordField: ((String) -> ())? {get set}
}


class PasswordAddViewController: UIViewController{
    
    // MARK: - Properties
        
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.contentSize = CGSize(width: UIDevice.width, height: 820)
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var backView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        view.layer.cornerCurve = .continuous
        view.layer.cornerRadius = 24
        return view
    }()
    
    private var topLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(rgb: 0xF6F6F6)
        view.layer.cornerRadius = 2.5
        return view
    }()
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Font.bold.size(24)
        label.textAlignment = .center
        label.textColor = UIColor(rgb: 0x23262F)
        label.text = "Add password"
        return label
    }()
    
    private lazy var titleTextField: UITextField = {
        let textField = UITextField()
        textField.addTarget(self, action: #selector(textFieldAction), for: .editingChanged)
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 16
        textField.layer.borderColor = UIColor(rgb: 0xA0AEC0).cgColor
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: 16.78, height: 56)
        textField.leftView = leftView
        textField.leftViewMode = .always
        textField.placeholder = "Title"
        textField.autocorrectionType = .no
        textField.delegate = self
        return textField
    }()
    
    private lazy var loginTextField: UITextField = {
        let textField = UITextField()
        textField.addTarget(self, action: #selector(textFieldAction), for: .editingChanged)
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 16
        textField.layer.borderColor = UIColor(rgb: 0xA0AEC0).cgColor
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: 16.78, height: 56)
        textField.leftView = leftView
        textField.leftViewMode = .always
        textField.placeholder = "Login"
        textField.autocorrectionType = .no
        textField.delegate = self
        return textField
    }()
    
    private lazy var urlTextField: UITextField = {
        let textField = UITextField()
        textField.addTarget(self, action: #selector(textFieldAction), for: .editingChanged)
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 16
        textField.layer.borderColor = UIColor(rgb: 0xA0AEC0).cgColor
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: 16.78, height: 56)
        textField.leftView = leftView
        textField.leftViewMode = .always
        textField.placeholder = "URL"
        textField.autocorrectionType = .no
        textField.delegate = self
        return textField
    }()
    
    private lazy var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.addTarget(self, action: #selector(textFieldAction), for: .editingChanged)
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 16
        textField.layer.borderColor = UIColor(rgb: 0xA0AEC0).cgColor
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: 16.78, height: 56)
        textField.leftView = leftView
        textField.leftViewMode = .always
        textField.placeholder = "Password"
        textField.autocorrectionType = .no
        textField.delegate = self
        return textField
    }()
    
    private let geneateBtn: UIButton = {
        let button = UIButton()
        button.layer.cornerCurve = .continuous
        button.layer.cornerRadius = 16
        button.setTitle("Generate password", for: .normal)
        button.backgroundColor = .black
        button.titleLabel?.font = Font.bold.size(16)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(generateAction), for: .touchUpInside)
        return button
    }()
    
    private let doneBtn: UIButton = {
        let button = UIButton()
        button.layer.cornerCurve = .continuous
        button.layer.cornerRadius = 16
        button.setTitle("Done", for: .normal)
        button.backgroundColor = UIColor(rgb: 0x334BFF)
        button.titleLabel?.font = Font.bold.size(16)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(doneAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var closeBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        button.setImage("close".img(), for: .normal)
        return button
    }()
    
    private let bottomSafeAreaView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    // MARK: -
    
    private var isClosingByScroll = false
    private var isKeyboardShowed = false
    
    // MARK: -
    
    var viewModel: PasswordAddProtocol
    
    // MARK: - Initialization
    
    init(viewModel: PasswordAddProtocol){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        // ui
        view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        setViews()
        setConstraints()
        
        viewModel.didUpdatePasswordField = {[weak self] (password) in
            self?.passwordTextField.text = password
        }
    }
    
    // MARK: - Actions
    
    @objc private func closeAction(){
        self.view.endEditing(true)
        viewModel.closeAction(animate: true)
    }
    
    @objc private func generateAction(){
        viewModel.generateAction()
    }
    
    @objc private func doneAction(){
        viewModel.doneAction()
    }
    
    @objc private func textFieldAction(){
        viewModel.updateData(title: titleTextField.text ?? "", login: loginTextField.text ?? "", url: urlTextField.text ?? "", password: passwordTextField.text ?? "")
    }

    
    // MARK: - UI
    
    private func setViews(){
        self.view.addSubview(backView)
        self.view.addSubview(bottomSafeAreaView)
        
        backView.addSubview(scrollView)
        
        scrollView.addSubview(topLineView)
        scrollView.addSubview(titleLabel)
        scrollView.addSubview(closeBtn)
        scrollView.addSubview(titleTextField)
        scrollView.addSubview(loginTextField)
        scrollView.addSubview(urlTextField)
        scrollView.addSubview(passwordTextField)
        scrollView.addSubview(geneateBtn)
        scrollView.addSubview(doneBtn)
    }
    
    private func setConstraints(){
        backView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide).inset(16)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }
        
        topLineView.snp.makeConstraints { make in
            make.height.equalTo(5)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(8)
            make.width.equalTo(47)
        }
        
        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(16)
            make.width.equalTo(UIDevice.width-32)
            make.height.equalTo(30)
            make.top.equalToSuperview().inset(28)
        }
        
        closeBtn.snp.makeConstraints { make in
            make.width.height.equalTo(48)
            make.trailing.equalTo(titleLabel)
            make.centerY.equalTo(titleLabel)
        }
        
        titleTextField.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleLabel)
            make.height.equalTo(56)
            make.top.equalTo(titleLabel.snp.bottom).inset(-32)
        }
        
        loginTextField.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleLabel)
            make.height.equalTo(56)
            make.top.equalTo(titleTextField.snp.bottom).inset(-8)
        }
        
        urlTextField.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleLabel)
            make.height.equalTo(56)
            make.top.equalTo(loginTextField.snp.bottom).inset(-8)
        }
        
        passwordTextField.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleLabel)
            make.height.equalTo(56)
            make.top.equalTo(urlTextField.snp.bottom).inset(-8)
        }
        
        geneateBtn.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleLabel)
            make.height.equalTo(56)
            make.top.equalTo(passwordTextField.snp.bottom).inset(-24)
        }
        
        doneBtn.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleLabel)
            make.height.equalTo(56)
            make.top.equalTo(geneateBtn.snp.bottom).inset(-8)
        }
        
        bottomSafeAreaView.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalToSuperview()
            make.top.equalTo(backView.snp.bottom)
        }
    }
}

// MARK: - UITextFieldDelegate

extension PasswordAddViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
