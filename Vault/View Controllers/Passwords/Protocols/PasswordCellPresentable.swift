//
//  PasswordCellPresentable.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 20.02.2022.
//

import Foundation
import UIKit

protocol PasswordCellPresentable{
    
    // MARK: - Properties
    
    var title: String {get}
    var login: String {get}
    var url: String {get}
    var password: String {get}
    var isPassHidden: Bool {get}
    var isFavorite: Bool {get}
    
}
