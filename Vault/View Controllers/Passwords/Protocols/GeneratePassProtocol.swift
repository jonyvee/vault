//
//  GeneratePassProtocol.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 22.02.2022.
//

import UIKit


protocol GeneratePassProtocol{
    func backAction()
    func saveAction()
    func changeLenghtAction(lenght: Int)
    func includingAction(numbers: Bool, uppercase: Bool, lowercase: Bool, symbols: Bool)
    func generateAction()
    func generateRandomString(length: Int, type: GeneratePassViewModel.PasswordType) -> String
    var didChangeText: ((String) -> ())? {set get}
}
