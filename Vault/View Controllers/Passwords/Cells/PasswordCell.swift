//
//  PasswordsCell.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 20.02.2022.
//

import UIKit
import SnapKit

class PasswordCell: UICollectionViewCell{
    
    // MARK: - Properties
    
    static let key = "PasswordCell"
    
    // MARK: -
    
    private lazy var backView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        view.backgroundColor = .white
        view.layer.borderColor = UIColor(rgb: 0xE2E8F0).cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 16
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.1
        view.layer.shadowRadius = 6
        view.layer.shadowOffset = .zero
        return view
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 0.102, green: 0.125, blue: 0.173, alpha: 1)
        label.font = Font.bold.size(14)
        return label
    }()
    
    private let loginLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 0.443, green: 0.502, blue: 0.588, alpha: 1)
        label.font = Font.regular.size(14)
        return label
    }()
    
    private let passwordLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 0.443, green: 0.502, blue: 0.588, alpha: 1)
        label.font = Font.regular.size(14)
        return label
    }()
    
    private lazy var showBtn: UIButton = {
        let button = UIButton()
        button.setImage("show".img(), for: .normal)
        button.addTarget(self, action: #selector(showPasswordAction), for: .touchUpInside)
        return button
    }()
    
    private let heartImageView = UIImageView(image: "heartfill".img())
    
    // MARK: -
    
    private var isPasswordHidden = true
    private var password = ""
        
    // MARK: -
    
    var didHidePassword: ((Bool) -> ())?
        
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = .white
        setupViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backView.layer.shadowPath = UIBezierPath(rect:  backView.bounds).cgPath
    }
    
    // MARK: - Configuration
    
    func configure(with presentable: PasswordCellPresentable){
        isPasswordHidden = presentable.isPassHidden
        heartImageView.isHidden = !presentable.isFavorite
        
        self.titleLabel.text = presentable.title
        loginLabel.text = presentable.login
        passwordLabel.text = presentable.password
        self.password = presentable.password
        showBtn.setImage(isPasswordHidden ? "show".img() : "hide".img(), for: .normal)
        if isPasswordHidden{
            passwordLabel.secureTextEntry()
        }
        self.contentView.layoutIfNeeded()
    }
    
    // MARK: - Actions
    
    @objc private func showPasswordAction(){
        Vibration.soft.vibrate()
        isPasswordHidden.toggle()
        showBtn.setImage(isPasswordHidden ? "show".img() : "hide".img(), for: .normal)
        
        if isPasswordHidden{
            passwordLabel.secureTextEntry()
        }else{
            passwordLabel.text = self.password
        }
        
        didHidePassword?(isPasswordHidden)
    }
    
    // MARK: - UI
    
    private func setupViews(){
        contentView.addSubview(backView)
        backView.addSubview(titleLabel)
        backView.addSubview(loginLabel)
        backView.addSubview(passwordLabel)
        backView.addSubview(showBtn)
        backView.addSubview(heartImageView)
    }
    
    private func setConstraints(){
        backView.snp.makeConstraints { make in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview().inset(16)
            make.height.equalTo(20)
        }
        
        loginLabel.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel)
            make.top.equalTo(titleLabel.snp.bottom)
            make.height.equalTo(20)
            make.trailing.equalTo(showBtn.snp.leading)
        }
        
        passwordLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleLabel)
            make.top.equalTo(loginLabel.snp.bottom).inset(-4)
        }
        
        showBtn.snp.makeConstraints { make in
            make.height.width.equalTo(32)
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().inset(24)
        }

        heartImageView.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.top.trailing.equalToSuperview().inset(8)
        }
    }
    
}
