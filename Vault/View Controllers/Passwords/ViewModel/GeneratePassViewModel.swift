//
//  GeneratePassViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 22.02.2022.
//

import Foundation
import Combine


class GeneratePassViewModel: GeneratePassProtocol{
    
    // MARK: - Types
    
    enum PasswordType: String  {
        case upAndLowAndSymbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!#$%&'*+,-./:;<=>?@\'|~"
        case symbolsAndNumAndBrackets = "!#$%&'*+,-./:;<=>?@\'|~0123456789()[]{}"
        case upAndLowAndNum = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
        case all = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ()[]{}!#$%&'*+,-./:;<=>?@\'|~"
        case words =  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    }
    
    // MARK: - Properties
    
    private let lowercaseArray = Array("abcdefghijklmnopqrstuvwxyz")
    private let uppdercaseArray = Array("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    private let numbers = Array("0123456789")
    private let specialSymbols  = Array("!#$%&'*+,-./:;<=>?@\'|~")
    private let brackets = Array("()[]{}")
    private let all = Array("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ()[]{}!#$%&'*+,-./:;<=>?@\'|~")
    
    // MARK: -    
    private var isNumbers: Bool = true
    private var isUppercase: Bool = false
    private var isLowercase: Bool = false
    private var isSymbols: Bool = true
    private var lenght: Int = 16
    private var generatedPassword: String = ""
    
    var didChangeText: ((String) -> ())?
    
    // MARK: -
    
    var didClose: ((String) -> ())?
    
    // MARK: - Initialization
    
    init(){
    }
    
    // MARK: - Methods
    
    func backAction() {
        didClose?("")
        Analitika.report("passwordGenerationCloseAction")
    }
    
    func saveAction() {
        // save data then
        didClose?(generatedPassword)
        Analitika.report("passwordGenerationSaveAction")
    }
    
    func changeLenghtAction(lenght: Int) {
        self.lenght = lenght
        self.generateText(isNumbers: self.isNumbers, isUppercase: self.isUppercase, isLowercase: self.isLowercase, isSimbolds: self.isSymbols, lenght: self.lenght)
    }
    
    func includingAction(numbers: Bool, uppercase: Bool, lowercase: Bool, symbols: Bool) {
        self.isNumbers = numbers
        self.isUppercase = uppercase
        self.isLowercase = lowercase
        self.isSymbols = symbols
        self.generateText(isNumbers: self.isNumbers, isUppercase: self.isUppercase, isLowercase: self.isLowercase, isSimbolds: self.isSymbols, lenght: self.lenght)
    }
    
    func generateAction() {
        self.generateText(isNumbers: self.isNumbers, isUppercase: self.isUppercase, isLowercase: self.isLowercase, isSimbolds: self.isSymbols, lenght: self.lenght)
        Analitika.report("passwordGenerationGenerateAction")
    }
    
    func generateRandomString(length: Int, type: PasswordType) -> String {
        let array = Array(type.rawValue)
        var string = ""
        for _ in 0..<length {
            string.append(generateRandomCharacter(from: array))
        }
        
        return string
    }
    
    // MARK: - Helper
    
    private func generateRandomCharacter(from array: Array<Character>) -> Character {
        let index = Int(arc4random_uniform(UInt32(array.count)))
        let character = array[index]
        return character
    }
    
    private func generateText(isNumbers: Bool, isUppercase: Bool, isLowercase: Bool, isSimbolds: Bool, lenght: Int){
        var text = ""
        
        if isNumbers && isSymbols{
            text = generateRandomString(length: lenght, type: .all)
        }else if isNumbers == false && isSymbols{
            text = generateRandomString(length: lenght, type: .upAndLowAndSymbols)
        }else if isNumbers && isSymbols == false{
            text = generateRandomString(length: lenght, type: .upAndLowAndNum)
        }else if isNumbers == false && isSimbolds == false{
            text = generateRandomString(length: lenght, type: .words)
        }
        
        if isUppercase{
            text = text.uppercased()
        }
        
        if isLowercase{
            text = text.lowercased()
        }
        
        self.generatedPassword = text
        
        didChangeText?(text)
    }
    
}

