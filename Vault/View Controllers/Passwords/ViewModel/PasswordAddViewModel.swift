//
//  PasswordAddViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 22.02.2022.
//

import UIKit
import Realm
import Combine

class PasswordAddViewModel: PasswordAddProtocol{
    
    // MARK: - Properties

    private let passwordData: PasswordModel = PasswordModel()
    
    // MARK: -
    
    var didClose: ((Bool) -> ())?
    var didShowGeneratePassword: (() -> ())?
    var didUpdatePasswordField: ((String) -> ())?
    private var subscriptions: Set<AnyCancellable> = []
    
    // MARK: - Initialization
    
    init(passwordPublisher: Published<String>.Publisher){
        passwordPublisher
            .sink { [weak self] (password) in
                if password != ""{
                    self?.passwordData.password = password
                    self?.didUpdatePasswordField?(password)
                }
            }.store(in: &subscriptions)
    }

    // MARK: - Methods
    
    func generateAction() {
        didShowGeneratePassword?()
        Analitika.report("openPasswordGenerationAction")
    }
    
    func doneAction() {
        if passwordData.title == "" || passwordData.password == ""{
            Vibration.error.vibrate()
            JonyMessage.show(text: "Title and Password fields can not be emty")
        }else{
            try! realm.write {
                realm.add(passwordData)
            }
            didClose?(false)
            
            Analitika.report("passwordCreationDoneAction")
        }
    }
    
    func closeAction(animate: Bool) {
        didClose?(animate)
        Analitika.report("passwordCreationCloseAction")
    }
    
    func updateData(title: String, login: String, url: String, password: String) {
        passwordData.title = title
        passwordData.password = password
        passwordData.login = login
        passwordData.url = url
    }
}
