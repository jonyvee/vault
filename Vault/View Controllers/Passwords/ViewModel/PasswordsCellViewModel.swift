//
//  PasswordsCellViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 20.02.2022.
//

import UIKit


class PasswordsCellViewModel: PasswordCellPresentable{
    
    private let data: PasswordModel
    
    // MARK: - Initialization
    
    init(data: PasswordModel){
        self.data = data
    }
    
    var title: String{
        data.title
    }
    
    var login: String{
        data.login
    }
    
    var url: String{
        data.url
    }
    
    var password: String{
        data.password
    }
    
    var isPassHidden: Bool{
        data.isPassHidden
    }
    
    var isFavorite: Bool{
        data.isFavorite
    }
    
}
