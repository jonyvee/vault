//
//  PassDetailViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 23.02.2022.
//

import Foundation
import RealmSwift
import Combine

class PassDetailViewModel: PassDetailProtocol{
    
    
    
    // MARK: - Properties
    
    var didClose: (() -> ())?
    var didShowgeneratePassword: (() -> ())?
    var didUpdatePasswordField: ((String) -> ())?
    private var subscriptions: Set<AnyCancellable> = []
    
    // сделать эту переменную observable
    private let data: PasswordModel
    
    // MARK: - Initialization
    
    init(data: PasswordModel, passwordPublisher: Published<String>.Publisher){
        self.data = data
        
        passwordPublisher
            .sink { [weak self] (password) in
                if password != ""{
                    self?.data.password = password
                    self?.didUpdatePasswordField?(password)
                }
            }.store(in: &subscriptions)
    }
    
    deinit{
        print("DEINIT Pass Detail View Model")
    }
    
    // MARK: - Methods
    
    var title: String{
        data.title
    }
    
    var login: String{
        data.login
    }
    
    var url: String{
        data.url
    }
    
    var password: String{
        data.password
    }
    
    var isFavorite: Bool{
        data.isFavorite
    }    
    
    func backAction() {
        didClose?()
        Analitika.report("passwordDetailBackAction")
    }
    
    func favouriteAction() {
        data.isFavorite.toggle()
        Analitika.report("passwordDetailFavoriteAction")
    }
    
    func deleteAction() {
        let controller = UIAlertController(title: "Delete", message: "Do you want to delete password?", preferredStyle: .alert)
        let action = UIAlertAction(title: "Delete", style: .default) { _ in

            try? realm.write{
                realm.delete(realm.objects(PasswordModel.self).filter({$0.id == self.data.id}))
            }
            
            self.didClose?()
            Analitika.report("passwordDetailDeleteAction")
        }
        let action2 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(action)
        controller.addAction(action2)
        UIApplication.shared.topVC?.present(controller, animated: true, completion: nil)
    }
    
    func saveAction() {
        if data.title == "" || data.password == ""{
            JonyMessage.show(text: "Title and Password fields can't be empty")
        }else{
            try! realm.write {
                realm.add(data, update: .modified)
            }
            didClose?()
            Analitika.report("passwordDetailSaveAction")
        }
    }
    
    func generateAction() {
        didShowgeneratePassword?()
        Analitika.report("passwordDetailGenerateAction")
    }
    
    func updateData(title: String, login: String, url: String, password: String) {
        self.data.title = title
        self.data.login = login
        self.data.url = url
        self.data.password = password
    }

}

