//
//  PasswordsViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 19.02.2022.
//

import UIKit
import RealmSwift
import Combine
import SwiftUI

class PasswordsViewModel: PasswordsProtocol{
    
    // MARK: - Properties
    private var allData = realm.objects(PasswordModel.self)
    private var eventNotificationToken: NotificationToken? = nil
    
    // MARK: -
    
    var didShowPasswordAdd: (() -> ())?
    var didShowPasswordDetail: ((PasswordModel) -> ())?
    var didShowPurchase: (() -> ())?
    
    // MARK: -
    
    var reloadCollectionView: ((Int) -> ())?
    @Published var isFavourite: Bool = false
    @Published var searchText: String = ""
    private var subscriptions: Set<AnyCancellable> = []
    
    // MARK: - Initialization
    
    init() {
        observeEvents()
    }
    
    deinit{
        eventNotificationToken?.invalidate()
    }
    
    // MARK: - Methods
    
    var numberOfSections: Int{
        1
    }
    
    var numberOfRows: Int{
        return allData.count
    }
    
    func viewModel(at index: Int) -> PasswordCellPresentable{
        return PasswordsCellViewModel(data: allData[index])
    }
    
    func updatePasswordHidden(isHidden: Bool, at index: Int){
        try! realm.write {
            self.allData[index].isPassHidden = isHidden
        }
    }
    
    func showPasswordAdd(){
        if User.shared.isPurchased == true || allData.count <= 4{
            didShowPasswordAdd?()
            Analitika.report("passwordAddAction")
        }else{
            didShowPurchase?()
        }
    }
    
    func didSelectItem(at index: Int){
        guard let data = RealmHelper.DetachedCopy(of: allData[index]) else {return}
        didShowPasswordDetail?(data)
        Analitika.report("passwordOpenAction")
    }
    
    // MARK: - Observable
    
    private func observeEvents() {
        self.eventNotificationToken = self.allData.observe { (changes: RealmCollectionChange) in
            self.reloadCollectionView?(self.numberOfRows)
        }
        
        self.$isFavourite
            .sink { [weak self] (value) in
                self?.changeFilters(text: self?.searchText ?? "", isFavorite: value)
                self?.reloadCollectionView?(self?.numberOfRows ?? 0)
                
            }.store(in: &subscriptions)
        
        self.$searchText
            .sink { [weak self] (text) in
                self?.changeFilters(text: text, isFavorite: self?.isFavourite ?? false)
                self?.reloadCollectionView?(self?.numberOfRows ?? 0)
                
            }.store(in: &subscriptions)
    }
    
    // MARK: - Helper
    
    private func changeFilters(text: String, isFavorite: Bool){
        if text == ""{
            if isFavorite{
                self.allData = realm.objects(PasswordModel.self).where({$0.isFavorite == true})
            }else{
                self.allData = realm.objects(PasswordModel.self)
            }
        }else{
            if isFavorite{
                self.allData = realm.objects(PasswordModel.self).where({ password in
                    password.isFavorite == true && (password.login.contains(text) || password.url.contains(text) || password.title.contains(text) || password.password.contains(text))
                })
            }else{
                self.allData = realm.objects(PasswordModel.self).where({ password in
                    password.login.contains(text) || password.url.contains(text) || password.title.contains(text) || password.password.contains(text)
                })
            }
        }
    }
}
