//
//  CreateNoteViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 27.02.2022.
//

import Foundation
import UIKit
import CloudKit
import SwiftLoader
import RealmSwift
import Realm

class CreateNoteViewModel: CreateNoteProtocol, ImageSaverManager{
    
    // MARK: - Types
    
    enum CellType{
        case title(String)
        case text(String)
        case image(UIImage)
        case task(TaskModel)
    }
    
    // MARK: -
    
    private var data: NoteModel?
    private var imagePicker: ImagePicker?
    private var images: [UIImage?] = []
    private var dataCollectionPresentable: [[CellType]] = []
    
    // MARK: - Properties
    
    var didClose: (() -> ())?
    
    // MARK: -
    
    var reloadCollectionView: (() -> ())?
    
    
    // MARK: - Initialization
    
    init(data: NoteModel? = nil){
        if let data = data {
            self.data = data
        }else{
            self.data = NoteModel()
        }
        
        if let presentingController = UIApplication.shared.topVC{
            imagePicker =  ImagePicker(presentationController: presentingController, delegate: self)
        }
        
        updateDataCollectionPresentable()
    }
    
    // MARK: - Methods
    
    func removeImage(at indexPath: IndexPath) {
        if let id = self.data?.imageIds[indexPath.row]{
            self.removeImage(forKey: id)
            self.images.remove(at: indexPath.row)
            self.data?.imageIds.remove(at: indexPath.row)
            updateDataCollectionPresentable()
            reloadCollectionView?()
        }
    }
    
    func removeTask(at indexPath: IndexPath) {
        self.data?.tasks.remove(at: indexPath.row)
        self.updateDataCollectionPresentable()
        self.reloadCollectionView?()
    }
    
    var isFavorite: Bool{
        data?.isFavorite ?? false
    }
    
    func backAction() {
        if data?.title != "" || data?.text != "" || data?.tasks.isEmpty == false || images.isEmpty == false{
            SwiftLoader.show(animated: true)
            if let data = data {
                try! realm.write{
                    realm.add(data, update: .modified)
                }
            }
            SwiftLoader.hide()
        }
        
        didClose?()
        Analitika.report("noteCloseAction")
    }
    
    func deleteAction() {
        let controller = UIAlertController(title: "Delete", message: "Do you want to delete this note", preferredStyle: .alert)
        let action = UIAlertAction(title: "Delete", style: .default) { [self] _ in
            if let data = data {
                try? realm.write{
                    realm.delete(realm.objects(NoteModel.self).filter({$0.id == data.id}))
                }
                self.removeImage(forKey: data.id)
            }
            didClose?()
            Analitika.report("noteDeleteAction")
        }
        let action2 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(action)
        controller.addAction(action2)
        UIApplication.shared.topVC?.present(controller, animated: true, completion: nil)
    }
    
    func favoriteAction() {
        data?.isFavorite.toggle()
        Analitika.report("noteFavoriteAction")
    }
    
    func addTask() {
        data?.tasks.append(TaskModel())
        updateDataCollectionPresentable()
        reloadCollectionView?()
        Analitika.report("noteAddTaskAction")
    }
    
    func addImage() {
        imagePicker?.present(type: .photoLibrary)
        updateDataCollectionPresentable()
        reloadCollectionView?()
        Analitika.report("noteAddImageAction")
    }
    
    func update(title: String) {
        data?.title = title
    }
    
    func update(text: String) {
        data?.text = text
    }
    
    var numberOfSections: Int{
        return dataCollectionPresentable.count
    }
    
    func numberOfRows(at section: Int) -> Int{
        return dataCollectionPresentable[section].count
    }
    
    func sectionType(at section: Int) -> CellType{
        dataCollectionPresentable[section].first ?? .title("")
    }
    
    func viewModel(at indexPath: IndexPath) -> CreateNoteTitlePresentable {
        let data = dataCollectionPresentable[indexPath.section][indexPath.row]
        return CreateNoteTitleViewModel(data: data)
    }
    
    func viewModel(at indexPath: IndexPath) -> CreateNoteTextPresentable {
        let data = dataCollectionPresentable[indexPath.section][indexPath.row]
        return CreateNoteTextViewModel(data: data)
    }
    
    func viewModel(at indexPath: IndexPath) -> CreateNoteTaskPresentable {
        let data = dataCollectionPresentable[indexPath.section][indexPath.row]
        return CreateNoteTaskViewModel(data: data)
    }
    
    func viewModel(at indexPath: IndexPath) -> CreateNoteImagePresentable {
        let data = dataCollectionPresentable[indexPath.section][indexPath.row]
        return CreateNoteImageViewModel(data: data)
    }

    func height(at indexPath: IndexPath) -> CGFloat {
        let data = dataCollectionPresentable[indexPath.section][indexPath.row]
        switch data{
        case .title(let value):
            return value.height(withConstrainedWidth: UIDevice.width-48, font: Font.bold.size(24))
        case .text(let value):
            return value.height(withConstrainedWidth: UIDevice.width-48, font: Font.medium.size(16))
        default:
            return 0
        }
    }
    
    func updateTaskSelection(indexPath: IndexPath) {
        self.data?.tasks[indexPath.row].isSelected.toggle()
        updateDataCollectionPresentable()
    }
    
    func updateTask(indexPath: IndexPath, text: String) {
        self.data?.tasks[indexPath.row].title = text
    }
    
    
    // MARK: - Helper
    
    private func updateDataCollectionPresentable(){
        dataCollectionPresentable.removeAll()
        
        // #1 title
        dataCollectionPresentable.append([.title(data?.title ?? "")])
        
        // #2 tasks
        if let tasks = self.data?.tasks{
            var tasksPresentable: [CellType] = []
            tasks.forEach { task in
                tasksPresentable.append(.task(task))
            }
            dataCollectionPresentable.append(tasksPresentable)
        }
        
        // #3 images
        if self.images.isEmpty{
            // В первый раз извлекаем из памяти, в отсальные разы все норм
            if let imageIds = self.data?.imageIds{
                var images: [UIImage] = []
                var imagesPresentable: [CellType] = []
                
                imageIds.forEach { ID in
                    if let image = self.retrieveImage(forKey: ID){
                        images.append(image)
                        imagesPresentable.append(.image(image))
                        self.images.append(image)
                    }
                }
                dataCollectionPresentable.append(imagesPresentable)
            }
        }else{
            var imagesPresentable: [CellType] = []
            self.images.forEach { image in
                if let image = image{
                    imagesPresentable.append(.image(image))
                }
            }
            dataCollectionPresentable.append(imagesPresentable)
        }
        
        
        // #4 text
        dataCollectionPresentable.append([.text(data?.text ?? "")])
    }
    
}


extension CreateNoteViewModel: ImagePickerDelegate{
    func didSelect(image: UIImage?) {
        if let image = image {
            SwiftLoader.show(animated: true)
            let id = NSUUID().uuidString
            self.images.append(image)
            self.data?.imageIds.append(id)
            self.store(image: image, forKey: id)
            self.updateDataCollectionPresentable()
            reloadCollectionView?()
            SwiftLoader.hide()
        }
    }
}
