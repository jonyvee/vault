//
//  NotesCoordinator.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 19.02.2022.
//

import UIKit
import RealmSwift
import SwiftLoader

class NotesViewModel: NotesProtocol{
    
    // MARK: - Properties
    
    private var notes = realm.objects(NoteModel.self)
    private var eventNotificationToken: NotificationToken? = nil
    
    // MARK: -
    
    var reloadCollectionView: ((Int) -> ())?
    var didTapCreate: (() -> ())?
    var didOpenNote: ((NoteModel) -> ())?
    
    // MARK: -
    
    var selectedRows: [Int] = []
    var isEditMode: Bool = false
    
    // MARK: - Initialization
    
    init() {
        observeEvents()
    }
    
    deinit{
        eventNotificationToken?.invalidate()
    }
    
    
    // MARK: - Methods
    
    var numberOfRows: Int{
        return notes.count
    }
    
    var numberOfSections: Int{
        return 1
    }
    
    var isDeleteBtnHidden: Bool{
        selectedRows.count == 0
    }
    
    func viewModel(at index: Int) -> NoteCellPresentable {
        return NoteViewModel(data: notes[index], isSelected: selectedRows.contains(index))
    }
    
    func didSelectItem(at index: Int) {
        if isEditMode{
            if let indx = selectedRows.firstIndex(where: {$0 == index}){
                selectedRows.remove(at: indx)
            }else{
                selectedRows.append(index)
            }
        }else{
            if let copyNote = RealmHelper.DetachedCopy(of: notes[index]){
                didOpenNote?(copyNote)
            }
            Analitika.report("openNoteAction")
        }
    }
    
    func didSelectEdit() {
        isEditMode.toggle()
        if isEditMode == false{
            selectedRows.removeAll()
            reloadCollectionView?(numberOfRows)
        }
        Analitika.report("notesEditAction")
    }
    
    func createAction(){
        didTapCreate?()
        Analitika.report("notesCreateAction")
    }
    
    func didTapAllCategory() {
        notes = realm.objects(NoteModel.self)
        reloadCollectionView?(numberOfRows)
        Analitika.report("notesAllSection")
    }
    
    func didTapFavoriteCategory() {
        notes = realm.objects(NoteModel.self).where({$0.isFavorite == true})
        reloadCollectionView?(numberOfRows)
        Analitika.report("notesFavoriteSection")
    }
    
    func deleteAction() {
        let controller = UIAlertController(title: "Delete", message: "Do you want to delete \(selectedRows.count) notes", preferredStyle: .alert)
        let action = UIAlertAction(title: "Delete", style: .default) { _ in

            SwiftLoader.show(animated: true)

            var notes: [NoteModel] = []
            
            self.selectedRows.forEach { index in
                let album = self.notes[index]
                notes.append(album)
            }
            
            try! realm.write{
                realm.delete(notes)
            }
            
            SwiftLoader.hide()

            self.selectedRows = []
            self.isEditMode = false
            self.reloadCollectionView?(self.numberOfRows)
            
            Analitika.report("notesDeleteAction")
        }
        let action2 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(action)
        controller.addAction(action2)
        UIApplication.shared.topVC?.present(controller, animated: true, completion: nil)
    }
    
    // MARK: - Helper
    
    private func observeEvents() {
        self.eventNotificationToken = self.notes.observe { (changes: RealmCollectionChange) in
            self.reloadCollectionView?(self.numberOfRows)
        }
    }
}
