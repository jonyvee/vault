//
//  NoteViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 26.02.2022.
//

import UIKit


class NoteViewModel: NoteCellPresentable{
    
    
    
    // MARK: -
    
    private let data: NoteModel
    
    // MARK: - Initialization
    
    init(data: NoteModel, isSelected: Bool){
        self.data = data
        self.hasSelection = isSelected
    }
    
    // MARK: - Properties
    
    var text: String{
        data.title
    }
    
    var date: String{
        data.date.custom
    }

    var isFavorite: Bool{
        data.isFavorite
    }
    
    var hasSelection: Bool
}

// MARK: - Helper

fileprivate extension Date{
    var custom: String{
        self.toString(format: "MMMM dd, yyyy")
    }
}
