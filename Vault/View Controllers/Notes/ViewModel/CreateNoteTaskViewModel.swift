//
//  CreateNoteTaskViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 27.02.2022.
//

import UIKit


class CreateNoteTaskViewModel: CreateNoteTaskPresentable{
    
    private let data: CreateNoteViewModel.CellType
    
    // MARK: - Initialization
    
    init(data: CreateNoteViewModel.CellType){
        self.data = data
    }
    
    var isSelected: Bool{
        if case let .task(task) = data {
            return task.isSelected
        }else{
            return false
        }
    }
    
    var text: String{
        if case let .task(task) = data {
            return task.title
        }else{
            return ""
        }
    }
}
