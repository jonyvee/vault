//
//  CreateNoteTextViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 27.02.2022.
//

import UIKit


class CreateNoteTextViewModel: CreateNoteTextPresentable{
    
    
    
    private let data: CreateNoteViewModel.CellType
    
    // MARK: - Initialization
    
    init(data: CreateNoteViewModel.CellType){
        self.data = data
    }
    
    var text: String{
        if case let .text(text) = data {
            return text
        }else{
            return ""
        }
    }
    
}
