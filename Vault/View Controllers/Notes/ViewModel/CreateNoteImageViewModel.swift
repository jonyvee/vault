//
//  CreateNoteImageViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 27.02.2022.
//

import UIKit


class CreateNoteImageViewModel: CreateNoteImagePresentable{
    
    private let data: CreateNoteViewModel.CellType
    
    // MARK: - Initialization
    
    init(data: CreateNoteViewModel.CellType){
        self.data = data
    }
    
    var image: UIImage{
        if case let .image(image) = data {
            return image
        }else{
            return UIImage()
        }
    }
}
