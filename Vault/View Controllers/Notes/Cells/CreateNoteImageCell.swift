//
//  CreateNoteImageCell.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 27.02.2022.
//

import UIKit
import SnapKit

class CreateNoteImageCell: UICollectionViewCell{
    
    // MARK: - Static
    
    static let key = "CreateNoteImageCell"
    
    // MARK: - Properties
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerCurve = .continuous
        imageView.backgroundColor = .clear
        imageView.layer.cornerRadius = 12
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    private lazy var removeBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(removeAction), for: .touchUpInside)
        button.setImage("remove".img(), for: .normal)
        return button
    }()
    
    // MARK: -
    
    var didRemoveImage: (() -> ())?
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = .white
        setupViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configuration
    
    func configure(presentable: CreateNoteImagePresentable){
        self.imageView.image = presentable.image
    }
    
    // MARK: - Actions
    
    @objc private func removeAction(){
        Vibration.soft.vibrate()
        didRemoveImage?()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        contentView.addSubview(imageView)
        imageView.addSubview(removeBtn)
    }
    
    private func setConstraints(){
        imageView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.bottom.equalToSuperview().inset(20)
        }
        
        removeBtn.snp.makeConstraints { make in
            make.width.height.equalTo(32)
            make.trailing.top.equalToSuperview().inset(8)
        }
    }
    
}
