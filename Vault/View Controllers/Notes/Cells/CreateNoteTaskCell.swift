//
//  CreateNoteTaskCell.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 27.02.2022.
//

import UIKit
import SnapKit

class CreateNoteTaskCell: UICollectionViewCell{
    
    // MARK: - Static
    
    static let key = "CreateNoteTaskCell"
    
    // MARK: - Properties
    
    private let imageView = UIImageView()
    private lazy var textField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "What to do?"
        textField.textColor = UIColor(rgb: 0x1A202C)
        textField.backgroundColor = .clear
        textField.font = Font.medium.size(16)
        textField.autocorrectionType = .no
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return textField
    }()
    
    private lazy var removeBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(removeAction), for: .touchUpInside)
        button.setImage("remove".img(), for: .normal)
        return button
    }()
    
    // MARK: -
    
    var textFieldDidChange: ((String) ->())?
    var didRemoveTask: (() -> ())?
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = .white
        setupViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configuration
    
    func configure(presentable: CreateNoteTaskPresentable){
        self.imageView.image = presentable.isSelected ? "checked".img() : "unchecked".img()
        self.textField.textColor = presentable.isSelected ? UIColor(rgb: 0x718096) : UIColor(rgb: 0x1A202C)
        if presentable.isSelected{
            textField.attributedText = NSMutableAttributedString(string: presentable.text, attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.kern: 0.3])
        }else{
            self.textField.attributedText = nil
            self.textField.text = presentable.text
        }
    }
    
    // MARK: - Actions
    
    @objc private func textFieldDidChange(_ sender: UITextField){
        textFieldDidChange?(sender.text ?? "")
    }
    
    @objc private func removeAction(){
        Vibration.soft.vibrate()
        didRemoveTask?()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        contentView.addSubview(imageView)
        contentView.addSubview(textField)
        contentView.addSubview(removeBtn)
    }
    
    private func setConstraints(){
        imageView.snp.makeConstraints { make in
            make.leading.centerY.equalToSuperview()
            make.width.height.equalTo(24)
        }
        
        textField.snp.makeConstraints { make in
            make.leading.equalTo(imageView.snp.trailing).inset(-8)
            make.height.centerY.equalToSuperview()
            make.trailing.equalTo(removeBtn.snp.leading)
        }
        
        removeBtn.snp.makeConstraints { make in
            make.height.width.equalTo(contentView.snp.height).multipliedBy(0.6)
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview()
        }
    }
}
