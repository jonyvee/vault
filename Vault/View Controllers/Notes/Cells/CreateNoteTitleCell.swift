//
//  CreateNoteTitleCell.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 27.02.2022.
//

import UIKit
import SnapKit

class CreateNoteTitleCell: UICollectionViewCell, UITextViewDelegate{
    
    // MARK: - Static
    
    static let key = "CreateNoteTitleCell"
    
    // MARK: - Properties
    
    private let textViewPlaceholder = "Title"
    
    private lazy var textView: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.gray
        textView.font = Font.bold.size(24)
        textView.backgroundColor = .clear
        textView.isScrollEnabled = false
        textView.delegate = self
        textView.autocorrectionType = .no
        return textView
    }()
    
    // MARK: -
    
    var didChangeText: ((String) -> ())?
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = .white
        setupViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configuration
    
    func configure(presentable: CreateNoteTitlePresentable){
        self.textView.text = presentable.text
        self.didEndEditingTextViewCheckUI()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        contentView.addSubview(textView)
    }
    
    private func setConstraints(){
        textView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalToSuperview().inset(24)
        }
    }
    
    // MARK: - Overrides
    
    func textViewDidChange(_ textView: UITextView) {
        didChangeText?(textView.text)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"{
            return false
        }else{
            return true
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if self.textView.text == textViewPlaceholder{
            self.textView.text = ""
            textView.textColor = UIColor.gray
        }else{
            textView.textColor = UIColor(rgb: 0x1A202C)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        didEndEditingTextViewCheckUI()
    }
    
    private func didEndEditingTextViewCheckUI(){
        if self.textView.text == "" || self.textView.text == textViewPlaceholder{
            self.textView.text = textViewPlaceholder
            textView.textColor = UIColor.gray
        }else{
            textView.textColor = UIColor(rgb: 0x1A202C)
        }
    }
}
