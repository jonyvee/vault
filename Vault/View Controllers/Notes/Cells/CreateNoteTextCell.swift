//
//  CreateNoteTextCell.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 27.02.2022.
//

import UIKit
import SnapKit

class CreateNoteTextCell: UICollectionViewCell, UITextViewDelegate{
    
    // MARK: - Static
    
    static let key = "CreateNoteTextCell"
    
    // MARK: - Properties
    
    private let textViewPlaceholder = "Subtitle"
    
    private lazy var textView: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor(rgb: 0x718096)
        textView.font = Font.medium.size(16)
        textView.backgroundColor = .clear
        textView.isScrollEnabled = false
        textView.autocorrectionType = .no
        textView.delegate = self
        return textView
    }()
    
    // MARK: -
    
    var didChangeText: ((String) -> ())?
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = .white
        setupViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configuration
    
    func configure(presentable: CreateNoteTextPresentable){
        self.textView.text = presentable.text
        didEndEditingTextViewCheckUI()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        contentView.addSubview(textView)
    }
    
    private func setConstraints(){
        textView.snp.makeConstraints { make in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
    }
    
    // MARK: - Overrides
    
    func textViewDidChange(_ textView: UITextView) {
        didChangeText?(textView.text)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if self.textView.text == textViewPlaceholder{
            self.textView.text = ""
            textView.textColor = UIColor.gray
        }else{
            textView.textColor = UIColor(rgb: 0x718096)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        didEndEditingTextViewCheckUI()
    }
    
    private func didEndEditingTextViewCheckUI(){
        if self.textView.text == "" || self.textView.text == textViewPlaceholder{
            self.textView.text = textViewPlaceholder
            textView.textColor = UIColor.gray
        }else{
            textView.textColor = UIColor(rgb: 0x718096)
        }
    }
}
