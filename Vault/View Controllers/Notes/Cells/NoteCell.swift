//
//  NoteCell.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 26.02.2022.
//

import UIKit
import SnapKit

class NoteCell: UICollectionViewCell{
    
    // MARK: - Static
    
    static let key = "NoteCell"
    
    // MARK: - Properties
    
    private let backView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        view.backgroundColor = .white
        view.layer.cornerRadius = 20
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.1
        view.layer.shadowRadius = 6
        view.layer.shadowOffset = .zero
        view.layer.borderColor = UIColor(rgb: 0x334BFF).cgColor
        return view
    }()
    
    private let textLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x1D1D1F)
        label.font = Font.medium.size(14)
        return label
    }()
    
    private let dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x718096)
        label.font = Font.regular.size(12)
        return label
    }()

    private let heartImageView = UIImageView(image: "heartfill".img())
    
    // MARK: -
    
    private var hasSelection: Bool = false
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = .white
        setupViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backView.layer.shadowPath = UIBezierPath(roundedRect: backView.bounds, cornerRadius: 20).cgPath
    }
    
    // MARK: - Configuration
    
    func configure(with presentable: NoteCellPresentable){
        heartImageView.isHidden = !presentable.isFavorite
        self.hasSelection = presentable.hasSelection
        self.textLabel.text = presentable.text
        self.dateLabel.text = presentable.date
        setSelection()
        contentView.layoutIfNeeded()
    }
    
    // MARK: - UI
    
    private func setSelection(){
        if hasSelection{
            backView.layer.borderWidth = 4
        }else{
            backView.layer.borderWidth = 0
        }
    }
    
    private func setupViews(){
        contentView.addSubview(backView)
        backView.addSubview(textLabel)
        backView.addSubview(dateLabel)
        backView.addSubview(heartImageView)
    }
    
    private func setConstraints(){
        backView.snp.makeConstraints { make in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
        
        textLabel.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview().inset(16)
            make.bottom.equalTo(dateLabel.snp.top).inset(-8)
        }
        
        dateLabel.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview().inset(16)
            make.height.equalTo(13)
        }
        
        heartImageView.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.top.trailing.equalToSuperview().inset(8)
        }
    }

}
