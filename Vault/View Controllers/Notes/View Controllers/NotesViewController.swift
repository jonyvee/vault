//
//  NotesViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit
import SnapKit

protocol NotesProtocol{
    func createAction()
    func viewModel(at index: Int) -> NoteCellPresentable
    func didSelectItem(at index: Int)
    func didSelectEdit()
    func deleteAction()
    func didTapAllCategory()
    func didTapFavoriteCategory()
    
    var numberOfRows: Int {get}
    var numberOfSections: Int {get}
    var isEditMode: Bool {get}
    var isDeleteBtnHidden: Bool {get}
    var reloadCollectionView: ((Int) -> ())? {set get}
}

class NotesViewController: UIViewController{
    
    // MARK: - Properties
    
    private let navigationView: AddNavigationView = {
        let navigation = AddNavigationView(title: "Notes", isEditHidden: false)
        return navigation
    }()
    
    private lazy var plugImageView: UIImageView = {
        let imageView = UIImageView(image: "notesnone".img())
        imageView.contentMode = .scaleAspectFit
        imageView.isHidden = self.viewModel.numberOfRows > 0
        return imageView
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets.init(top: 24, left: 16, bottom: 20, right: 16)
        layout.itemSize = CGSize(width: (UIDevice.width-44)/2, height: 116)
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 11
        
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(NoteCell.self, forCellWithReuseIdentifier: NoteCell.key)
        
        return collectionView
    }()
    
    private var viewModel: NotesProtocol
    
    // MARK: - Initialization
    
    init(viewModel: NotesProtocol){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        setConstraints()
        
        navigationView.didTapAddBtn = { [weak self] in
            self?.viewModel.createAction()
        }
        
        navigationView.didTapAllBtn = { [weak self] in
            self?.viewModel.didTapAllCategory()
        }
        
        navigationView.didTapFavoriteBtn = { [weak self] in
            self?.viewModel.didTapFavoriteCategory()
        }
        
        navigationView.didTapEditBtn = { [weak self] in
            self?.viewModel.didSelectEdit()
            self?.updateEditUI()
        }
        
        navigationView.didTapDeleteBtn = { [weak self] in
            self?.viewModel.deleteAction()
            self?.updateEditUI()
        }
        
        viewModel.reloadCollectionView = { [weak self] (numberOfRows) in
            self?.plugImageView.isHidden = numberOfRows > 0
            self?.updateDeleteUI()
            self?.updateEditUI()
            self?.collectionView.reloadData()
        }
        
        if User.shared.isPurchased == false{
            AdManager.setBanner(rootVC: self)
        }
    }
    
    // MARK: - UI
    
    private func updateEditUI(){
        if viewModel.isEditMode{
            navigationView.updateEditText(text: "Cancel")
        }else{
            navigationView.updateEditText(text: "Edit")
        }
    }
    
    private func updateDeleteUI(){
        navigationView.updateDeleteBtn(isHidden: viewModel.isDeleteBtnHidden)
    }
    
    private func setupViews(){
        self.view.addSubview(navigationView)
        self.view.addSubview(plugImageView)
        self.view.addSubview(collectionView)
    }
    
    private func setConstraints(){
        navigationView.snp.makeConstraints { make in
            make.height.equalTo(AddNavigationView.height)
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(view.safeAreaLayoutGuide)
        }
        
        plugImageView.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalToSuperview()
            make.top.equalTo(navigationView.snp.bottom)
        }
        
        collectionView.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalToSuperview()
            make.top.equalTo(navigationView.snp.bottom)
        }
    }
    
}


extension NotesViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NoteCell.key, for: indexPath as IndexPath) as! NoteCell
        let presentable = viewModel.viewModel(at: indexPath.row)
        cell.configure(with: presentable)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Vibration.soft.vibrate()
        viewModel.didSelectItem(at: indexPath.row)
        self.updateDeleteUI()
        collectionView.reloadItems(at: [indexPath])
    }
}
