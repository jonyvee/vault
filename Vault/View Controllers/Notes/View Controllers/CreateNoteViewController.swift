//
//  CreateNoteViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 27.02.2022.
//

import UIKit
import SnapKit

protocol CreateNoteProtocol{
    var numberOfSections: Int {get}
    func numberOfRows(at section: Int) -> Int
    func sectionType(at section: Int) -> CreateNoteViewModel.CellType
    func viewModel(at indexPath: IndexPath) -> CreateNoteTitlePresentable
    func viewModel(at indexPath: IndexPath) -> CreateNoteTextPresentable
    func viewModel(at indexPath: IndexPath) -> CreateNoteTaskPresentable
    func viewModel(at indexPath: IndexPath) -> CreateNoteImagePresentable
    func height(at indexPath: IndexPath) -> CGFloat

    func removeTask(at indexPath: IndexPath)
    func removeImage(at indexPath: IndexPath)
    
    var isFavorite: Bool {get}
    var reloadCollectionView: (() -> ())? {get set}
    
    func update(title: String)
    func update(text: String)
    func updateTaskSelection(indexPath: IndexPath)
    func updateTask(indexPath: IndexPath, text: String)
    
    func addTask()
    func addImage()
    
    func backAction()
    func deleteAction()
    func favoriteAction()
}

class CreateNoteViewController: UIViewController{
    
    // MARK: - Properties
    
    private let backBtn: UIButton = {
        let button = UIButton()
        button.setImage("pass_back".img(), for: .normal)
        button.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        return button
    }()
    
    private let titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.textColor = UIColor(rgb: 0x1A202C)
        titleLabel.textAlignment = .center
        titleLabel.font = Font.bold.size(24)
        titleLabel.text = "Notes"
        return titleLabel
    }()
    
    private let deleteBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(deleteAction), for: .touchUpInside)
        button.setImage("trash".img(), for: .normal)
        return button
    }()
    
    private lazy var heartBtn: UIButton = {
        let button = UIButton()
        button.setImage(viewModel.isFavorite ? "heartfill".img() : "heart".img(), for: .normal)
        button.addTarget(self, action: #selector(heartAction), for: .touchUpInside)
        return button
    }()
    
    private let noteView: NoteView = {
        let view = NoteView()
        view.layer.zPosition = 2
        return view
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets.init(top: 0, left: 16, bottom: 0, right: 16)
        layout.minimumLineSpacing = 0

        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(CreateNoteTitleCell.self, forCellWithReuseIdentifier: CreateNoteTitleCell.key)
        collectionView.register(CreateNoteTextCell.self, forCellWithReuseIdentifier: CreateNoteTextCell.key)
        collectionView.register(CreateNoteTaskCell.self, forCellWithReuseIdentifier: CreateNoteTaskCell.key)
        collectionView.register(CreateNoteImageCell.self, forCellWithReuseIdentifier: CreateNoteImageCell.key)

        return collectionView
    }()

    // MARK: -
    
    private var viewModel: CreateNoteProtocol
    
    // MARK: - Initialization
    
    init(viewModel: CreateNoteProtocol){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad(){
        super.viewDidLoad()
        observeKeyboardNotifications()
        self.view.backgroundColor = .white
        setupViews()
        setConstraints()
        
        
        noteView.didAddList = { [weak self] in
            self?.viewModel.addTask()
        }
        
        noteView.didAddPhoto = { [weak self] in
            self?.viewModel.addImage()
        }
        
        noteView.didClose = { [weak self] in
            self?.view.endEditing(true)
        }
        
        viewModel.reloadCollectionView = {[weak self] in
            self?.collectionView.reloadData()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Actions
    
    @objc private func backAction(){
        viewModel.backAction()
    }
    
    @objc private func deleteAction(){
        viewModel.deleteAction()
    }
    
    @objc private func heartAction(){
        viewModel.favoriteAction()
        updateHeartBtnUI()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        view.addSubview(backBtn)
        view.addSubview(titleLabel)
        view.addSubview(deleteBtn)
        view.addSubview(heartBtn)
        view.addSubview(noteView)
        view.addSubview(collectionView)
    }
    
    private func setConstraints(){
        backBtn.snp.makeConstraints { make in
            make.width.height.equalTo(30)
            make.leading.equalToSuperview().inset(16)
            make.top.equalTo(view.safeAreaLayoutGuide).inset(24)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.height.equalTo(30)
            make.centerY.equalTo(backBtn)
            make.centerX.equalToSuperview()
        }
        
        deleteBtn.snp.makeConstraints { make in
            make.height.width.equalTo(24)
            make.trailing.equalTo(heartBtn.snp.leading).inset(-12)
            make.centerY.equalTo(backBtn)
        }
        
        heartBtn.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.trailing.equalToSuperview().inset(16)
            make.centerY.equalTo(backBtn)
        }
        
        noteView.snp.makeConstraints { make in
            make.height.equalTo(56)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }
        
        collectionView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(backBtn.snp.bottom).inset(-5)
            make.bottom.equalTo(noteView.snp.top)
        }
    }
    
    private func updateHeartBtnUI(){
        heartBtn.setImage(viewModel.isFavorite ? "heartfill".img() : "heart".img(), for: .normal)
    }
    
    
    // MARK: Keyboard Delegate
    
    private func observeKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(CreateNoteViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateNoteViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.4) { [self] in
                let offsetY: CGFloat = keyboardSize.height
                
                noteView.snp.updateConstraints { make in
                    make.bottom.equalTo(view.safeAreaLayoutGuide).inset(offsetY-UIDevice.bottomAreaHeight)
                }
                
                view.layoutIfNeeded()
                
            } completion: { (isSuccess) in}
        }
    }

    @objc func keyboardWillHide(notification: Notification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            UIView.animate(withDuration: 0.4) { [self] in
                
                noteView.snp.updateConstraints { make in
                    make.bottom.equalTo(view.safeAreaLayoutGuide)
                }
                
                view.layoutIfNeeded()
                
            } completion: { (isSuccess) in}
        }
    }
}

// MARK: - UICollectionViewDataSource

extension CreateNoteViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows(at: section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let sectionType = viewModel.sectionType(at: indexPath.section)
        
        switch sectionType {
        case .title(_):
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CreateNoteTitleCell.key, for: indexPath as IndexPath) as! CreateNoteTitleCell
            
            cell.didChangeText = { [weak self] (text) in
                self?.viewModel.update(title: text)
            }
            
            cell.configure(presentable: viewModel.viewModel(at: indexPath))
            return cell
            
        case .text(_):
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CreateNoteTextCell.key, for: indexPath as IndexPath) as! CreateNoteTextCell
            
            cell.didChangeText = { [weak self] (text) in
                self?.viewModel.update(text: text)
            }
            
            cell.configure(presentable: viewModel.viewModel(at: indexPath))
            return cell
        case .image(_):
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CreateNoteImageCell.key, for: indexPath as IndexPath) as! CreateNoteImageCell
            
            cell.didRemoveImage = {[weak self] in
                self?.viewModel.removeImage(at: indexPath)
            }
            
            cell.configure(presentable: viewModel.viewModel(at: indexPath))
            return cell
            
        case .task(_):
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CreateNoteTaskCell.key, for: indexPath as IndexPath) as! CreateNoteTaskCell
             
            cell.textFieldDidChange = {[weak self] (text) in
                self?.viewModel.updateTask(indexPath: indexPath, text: text)
            }
            
            cell.didRemoveTask = {[weak self] in
                self?.viewModel.removeTask(at: indexPath)
            }
            
            cell.configure(presentable: viewModel.viewModel(at: indexPath))
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let type = viewModel.sectionType(at: indexPath.section)
        
        if case .task(_) = type{
            viewModel.updateTaskSelection(indexPath: indexPath)
            self.collectionView.reloadItems(at: [indexPath])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let type = viewModel.sectionType(at: indexPath.section)
        let width = UIDevice.width - 32
        
        switch type{
        case .title(_):
            var height = viewModel.height(at: indexPath)
            height = height < 120 ? 120 : height
            return CGSize(width: width, height: height)
        case .image(_):
            return CGSize(width: width, height: width+40)
        case .task(_):
            return CGSize(width: width, height: 46)
        case .text(_):
            var height = viewModel.height(at: indexPath)
            height = height < 500 ? 500 : height
            return CGSize(width: width, height: height)
        }
    }
}
