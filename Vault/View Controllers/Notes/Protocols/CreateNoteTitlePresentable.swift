//
//  CreateNoteTitleCellPresentable.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 27.02.2022.
//

import Foundation


protocol CreateNoteTitlePresentable{
    
    // MARK: - Properties
    
    var text: String {get}
    
}
