//
//  NoteCellPresentable.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 26.02.2022.
//

import Foundation

protocol NoteCellPresentable{
    
    // MARK: - Properties
    
    var text: String {get}
    var date: String {get}
    var hasSelection: Bool {get}
    var isFavorite: Bool {get}
}

