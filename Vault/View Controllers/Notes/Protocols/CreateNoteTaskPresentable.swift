//
//  CreateNoteTaskPresentable.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 27.02.2022.
//

import Foundation


protocol CreateNoteTaskPresentable{
    
    // MARK: - Properties
    
    var isSelected: Bool {get}
    var text: String {get}
}
