//
//  CreateNoteImagePresentable.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 27.02.2022.
//

import UIKit


protocol CreateNoteImagePresentable{
    
    // MARK: - Properties
    
    var image: UIImage {get}
}
