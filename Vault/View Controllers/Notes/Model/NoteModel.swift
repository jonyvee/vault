//
//  NoteModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 26.02.2022.
//

import Foundation
import UIKit
import RealmSwift


class NoteModel: Object, Codable{
    @objc dynamic var id: String = NSUUID().uuidString
    @objc dynamic var title: String = ""
    @objc dynamic var text: String = ""
    @objc dynamic var isFavorite: Bool = false
    @objc dynamic var date: Date = Date()
    
    var imageIds = RealmSwift.List<String>()
    var tasks = RealmSwift.List<TaskModel>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

class TaskModel: Object, Codable{
    @objc dynamic var title: String = ""
    @objc dynamic var isSelected: Bool = false
}
