//
//  NoteView.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 27.02.2022.
//

import UIKit
import SnapKit

class NoteView: UIView{
    
    // MARK: - Properties
    
    private let horizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.axis = .horizontal
        stackView.spacing = 10
        stackView.isUserInteractionEnabled = true
        return stackView
    }()
    
    private lazy var listBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(listAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var photoBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(photoAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var closeBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        return button
    }()
    
    private let listImageView: UIImageView = {
        let imageView = UIImageView(image: "noteItem1".img())
        return imageView
    }()
    
    private let photoImageView: UIImageView = {
        let imageView = UIImageView(image: "noteItem2".img())
        return imageView
    }()
    
    private let closeImageView: UIImageView = {
        let imageView = UIImageView(image: "noteItem3".img())
        return imageView
    }()
    
    private let listLabel: UILabel = {
        let label = UILabel()
        label.text = "Add list"
        label.textColor = UIColor(rgb: 0x718096)
        label.textAlignment = .center
        label.font = Font.regular.size(10)
        return label
    }()
    
    private let photoLabel: UILabel = {
        let label = UILabel()
        label.text = "Add photo"
        label.textColor = UIColor(rgb: 0x718096)
        label.textAlignment = .center
        label.font = Font.regular.size(10)
        return label
    }()
    
    private let closeLabel: UILabel = {
        let label = UILabel()
        label.text = "Close"
        label.textColor = UIColor(rgb: 0x718096)
        label.textAlignment = .center
        label.font = Font.regular.size(10)
        return label
    }()
    
    // MARK: -
    
    var didAddList: (() -> ())?
    var didAddPhoto: (() -> ())?
    var didClose: (() -> ())?
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isUserInteractionEnabled = true
        self.backgroundColor = .white
        setupViews()
        setConstraints()
        setShadow()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Actions
    
    @objc private func listAction(){
        didAddList?()
    }
    
    @objc private func photoAction(){
        didAddPhoto?()
    }
    
    @objc private func closeAction(){
        didClose?()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        self.addSubview(horizontalStackView)
        horizontalStackView.addArrangedSubview(listBtn)
        horizontalStackView.addArrangedSubview(photoBtn)
        horizontalStackView.addArrangedSubview(closeBtn)
        listBtn.addSubview(listImageView)
        listBtn.addSubview(listLabel)
        photoBtn.addSubview(photoImageView)
        photoBtn.addSubview(photoLabel)
        closeBtn.addSubview(closeImageView)
        closeBtn.addSubview(closeLabel)
    }
    
    private func setConstraints(){
        horizontalStackView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(24)
            make.top.bottom.equalToSuperview().inset(8)
        }
        
        listImageView.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.top.centerX.equalToSuperview()
        }
        
        listLabel.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(14)
        }
        
        photoImageView.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.top.centerX.equalToSuperview()
        }
        
        photoLabel.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(14)
        }

        closeImageView.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.top.centerX.equalToSuperview()
        }

        closeLabel.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(14)
        }
    }
    
    private func setShadow(){
        self.layer.shadowColor = UIColor(red: 0.627, green: 0.682, blue: 0.753, alpha: 1).cgColor
        self.layer.shadowOpacity = 0.08
        self.layer.shadowRadius = 24
        self.layer.shadowOffset = CGSize(width: 0, height: -10)
    }
    
}
