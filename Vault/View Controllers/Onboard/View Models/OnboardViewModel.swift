//
//  OnboardViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit
import Combine

class OnboardViewModel {
    
    // MARK: - Properties
    
    private weak var coordinator: OnboardCoordinator?
    
    private let data = OnboardData()
    
    // MARK: -
    
    @Published  private(set) var selectedBoard: Int = 0
    
    // MARK: -
    
    var numberOfSections: Int{
        1
    }
    
    var numberOfRows: Int{
        data.items.count
    }
    
    // MARK: - Initialization
    
    init(coordinator: OnboardCoordinator) {
        self.coordinator = coordinator
    }
    
    // MARK: - Methods
    
    func viewModel(at index: Int) -> OnboardCellPresentable{
        data.items[index]
    }
    
    func didEndDecelerating(offesetX: CGFloat){
        selectedBoard = Int(offesetX)/Int(UIDevice.width)
    }
    
    func nextAction(){
        if selectedBoard < data.items.count-1{
            selectedBoard += 1
        }else{
            coordinator?.showFutureViewController()
        }
        
        Analitika.report("onboardNextAction\(selectedBoard)")
    }
    
}
