//
//  FutureViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit


class FutureViewModel{
    
    // MARK: - Properties
    
    private weak var coordinator: OnboardCoordinator?
    
    // MARK: - Initialization
    
    init(coordinator: OnboardCoordinator) {
        self.coordinator = coordinator
    }
    
    func nextAction(){
        coordinator?.showReminderViewController()
        Analitika.report("futureNextAction")
    }
    
}
