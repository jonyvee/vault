//
//  OnboardViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit
import SnapKit
import Combine

class OnboardViewController: UIViewController{
    
    // MARK: - Properties
    
    private var nextBtn: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(rgb: 0x334BFF)
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        button.layer.cornerRadius = 16
        button.setTitle("Next", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = Font.bold.size(16)
        return button
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = true
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(OnboardCell.self, forCellWithReuseIdentifier: OnboardCell.key)
        
        return collectionView
    }()
    
    private var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.spacing = 4
        return stackView
    }()
    
    private var stackViewItemsRef: [UIView] = []
    
    // MARK: -
    
    private var subscriptions: Set<AnyCancellable> = []
    
    private var viewModel: OnboardViewModel
    
    // MARK: - Initialization
    
    init(viewModel: OnboardViewModel){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ui
        self.view.backgroundColor = .white
        setupViews()
        updateStackViewItems(at: 0)
        setConstraints()
        
        // combine
        viewModel.$selectedBoard
            .sink { [weak self] (index) in
                self?.update(at: index)
            }.store(in: &subscriptions)
    }
    
    // MARK: - Actions
    
    @objc private func nextAction(){
        viewModel.nextAction()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        view.addSubview(nextBtn)
        view.addSubview(collectionView)
        view.addSubview(stackView)
        
        setupStackViewItems()
    }
    
    private func setupStackViewItems(){
        for _ in 0..<viewModel.numberOfRows{
            let view = UIView()
            view.layer.cornerRadius = 2
            stackView.addArrangedSubview(view)
            stackViewItemsRef.append(view)
        }
    }
    
    private func setConstraints(){
        // next btn
        nextBtn.snp.makeConstraints { make in
            make.height.equalTo(56)
            make.leading.trailing.equalToSuperview().inset(16)
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(UIDevice.isSmall ? 8 : 32)
        }
        
        collectionView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.bottom.equalTo(stackView.snp.top)
        }
        
        stackView.snp.makeConstraints { make in
            make.width.equalTo(176)
            make.height.equalTo(4)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(nextBtn.snp.top).inset(-48)
        }
    }
    
    // MARK: - Helper
    
    private func update(at index: Int){
        self.collectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .centeredHorizontally, animated: true)
        updateStackViewItems(at: index)
    }

    private func updateStackViewItems(at index: Int){
        stackViewItemsRef.enumerated().forEach { element in
            let isChoosed = index == element.offset
            element.element.backgroundColor = isChoosed ? UIColor(rgb: 0x334BFF) : UIColor(rgb: 0xEDF2F7)
        }
    }
    
}


extension OnboardViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        viewModel.didEndDecelerating(offesetX: scrollView.contentOffset.x)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OnboardCell.key, for: indexPath as IndexPath) as! OnboardCell
        let presentable = viewModel.viewModel(at: indexPath.row)
        cell.configure(with: presentable)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
}

