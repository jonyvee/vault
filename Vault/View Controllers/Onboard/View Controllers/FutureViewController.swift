//
//  FutureViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit
import SnapKit

class FutureViewController: UIViewController{
    
    // MARK: - Properties
    
    private let backImageView: UIImageView = {
        let imageView = UIImageView(image: "vector1".img())
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    private var nextBtn: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(rgb: 0x334BFF)
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        button.layer.cornerRadius = 16
        button.setTitle("Next", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = Font.bold.size(16)
        return button
    }()
    
    private var imageView: UIImageView = {
        let imageView = UIImageView(image: "futureText".img())
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "What will the\nupdates bring?"
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor(rgb: 0x1A202C)
        label.font = Font.bold.size(24)
        label.textAlignment = .center
        return label
    }()
    
    private var viewModel: FutureViewModel
    
    // MARK: - Initialization
    
    init(viewModel: FutureViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        setupViews()
        setConstraints()
    }
    
    // MARK: - Actions
    
    @objc private func nextAction(){
        viewModel.nextAction()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        view.addSubview(nextBtn)
        view.addSubview(imageView)
        view.addSubview(titleLabel)
        view.addSubview(backImageView)
    }
    
    private func setConstraints(){
        backImageView.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(140)
        }
        
        nextBtn.snp.makeConstraints { make in
            make.height.equalTo(56)
            make.leading.trailing.equalToSuperview().inset(16)
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(UIDevice.isSmall ? 8 : 32)
        }
        
        imageView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(view.snp.width).multipliedBy(1.312)
            make.bottom.equalTo(nextBtn.snp.top).inset( UIDevice.isSmall ? -8 : -32)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.bottom.equalTo(imageView.snp.top)
        }
    }
}
