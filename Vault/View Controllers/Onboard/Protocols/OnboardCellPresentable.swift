//
//  OnboardCellPresentable.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import Foundation
import UIKit

protocol OnboardCellPresentable{
    
    // MARK: - Properties
    
    var image: UIImage? {get}
    var title: String {get}
    var subtitle: String {get}
    
}
