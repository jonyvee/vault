//
//  OnboardCollectionViewCell.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit
import SnapKit

class OnboardCell: UICollectionViewCell{
    
    // MARK: - Properties
    
    static let key = "OnboardCell"
    
    // MARK: -
    
    private var backImageView: UIImageView = {
        let imageView = UIImageView(image: "vector1".img())
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    private var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x1A202C)
        label.textAlignment = .center
        label.font = Font.bold.size(24)
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    private var subtitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgb: 0x718096)
        label.textAlignment = .center
        label.font = Font.regular.size(16)
        label.numberOfLines = 3
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configuration
    
    func configure(with presentable: OnboardCellPresentable){
        self.imageView.image = presentable.image
        self.titleLabel.text = presentable.title
        self.subtitleLabel.text = presentable.subtitle
    }
    
    // MARK: - UI
    
    private func setupViews(){
        self.contentView.addSubview(backImageView)
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(subtitleLabel)
        self.contentView.addSubview(imageView)
    }
    
    private func setConstraints(){
        backImageView.snp.makeConstraints { make in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.height.equalTo(30)
            make.leading.trailing.equalToSuperview().inset(24)
            make.bottom.equalToSuperview().inset(120)
        }
        
        subtitleLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleLabel)
            make.top.equalTo(titleLabel.snp.bottom).inset(-16)
            make.bottom.lessThanOrEqualTo(contentView)
        }
        
        imageView.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview()
            make.bottom.equalTo(titleLabel.snp.top)
        }
    }
}
