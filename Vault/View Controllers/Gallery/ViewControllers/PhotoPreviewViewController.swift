//
//  PhotoPreviewViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 02.03.2022.
//

import UIKit
import SnapKit

protocol PhotoPreviewProtocol{
    var title: String {get}
    var image: UIImage? {get}
    var isFavorite: Bool{get}
    func deleteAction()
    func favoriteAction()
    func shareAction()
    func backAction()
}

class PhotoPreviewViewController: UIViewController{
    
    // MARK: - Properties
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = viewModel.image
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let topView: UIView = UIView()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = viewModel.title
        label.textColor = UIColor(rgb: 0x1D1D1F)
        label.textAlignment = .center
        label.font = Font.medium.size(18)
        return label
    }()
    
    private let safeBottomView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    private let bottomView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.layer.cornerRadius = 24
        return view
    }()
    
    private let backBtn: UIButton = {
        let button = UIButton()
        button.setImage("backBtn".img(), for: .normal)
        button.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        return button
    }()
    
    private let shareBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(shareAction), for: .touchUpInside)
        return button
    }()
    
    private let heartBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(favoriteAction), for: .touchUpInside)
        return button
    }()
    
    private let deleteBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(deleteAction), for: .touchUpInside)
        return button
    }()
    
    private let shareLabel: UILabel = {
        let label = UILabel()
        label.text = "Share"
        label.textAlignment = .center
        label.textColor = UIColor(rgb: 0x718096)
        label.font = Font.regular.size(10)
        return label
    }()
    
    private let heartLabel: UILabel = {
        let label = UILabel()
        label.text = "Favorite"
        label.textAlignment = .center
        label.textColor = UIColor(rgb: 0x718096)
        label.font = Font.regular.size(10)
        return label
    }()
    
    private let deleteLabel: UILabel = {
        let label = UILabel()
        label.text = "Delete"
        label.textAlignment = .center
        label.textColor = UIColor(rgb: 0x718096)
        label.font = Font.regular.size(10)
        return label
    }()
    
    private let shareImageView = UIImageView(image: "share".img())
    private let heartImageView = UIImageView(image: "heart".img())
    private let deleteImageView = UIImageView(image: "deleteBlue".img())
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    // MARK: -
    
    private let viewModel: PhotoPreviewProtocol
    
    // MARK: - Initialization
    
    init(viewModel: PhotoPreviewProtocol){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        setConstraints()
        setHeartUI()
    }
    

    // MARK: - Actions
    
    @objc private func backAction(){
        Vibration.soft.vibrate()
        viewModel.backAction()
    }
    
    @objc private func shareAction(){
        Vibration.soft.vibrate()
        viewModel.shareAction()
    }
    
    @objc private func favoriteAction(){
        Vibration.soft.vibrate()
        viewModel.favoriteAction()
        setHeartUI()
    }
    
    @objc private func deleteAction(){
        Vibration.soft.vibrate()
        viewModel.deleteAction()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        view.addSubview(safeBottomView)
        view.addSubview(imageView)
        view.addSubview(topView)
        view.addSubview(bottomView)
        topView.addSubview(backBtn)
        topView.addSubview(titleLabel)
        bottomView.addSubview(stackView)
        
        stackView.addArrangedSubview(shareBtn)
        stackView.addArrangedSubview(heartBtn)
        stackView.addArrangedSubview(deleteBtn)
        
        shareBtn.addSubview(shareImageView)
        shareBtn.addSubview(shareLabel)
        heartBtn.addSubview(heartImageView)
        heartBtn.addSubview(heartLabel)
        deleteBtn.addSubview(deleteImageView)
        deleteBtn.addSubview(deleteLabel)
    }
    
    private func setConstraints(){
        
        bottomView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(56)
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }
        
        safeBottomView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(bottomView.snp.bottom)
        }
        
        topView.snp.makeConstraints { make in
            make.height.equalTo(44)
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(view.safeAreaLayoutGuide)
        }
        
        imageView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(topView.snp.bottom)
            make.bottom.equalTo(safeBottomView.snp.top)
        }
        
        backBtn.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.leading.equalToSuperview().inset(16)
            make.centerY.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.height.equalToSuperview()
            make.centerX.equalToSuperview()
            make.leading.equalTo(backBtn.snp.trailing).inset(-5)
        }
        
        stackView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(24)
            make.height.equalTo(40)
            make.centerY.equalToSuperview()
        }
        
        heartImageView.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.centerX.top.equalToSuperview()
        }
        
        shareImageView.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.centerX.top.equalToSuperview()
        }
        
        deleteImageView.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.centerX.top.equalToSuperview()
        }
        
        shareLabel.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(14)
        }
        
        heartLabel.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(14)
        }
        
        deleteLabel.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(14)
        }
    }
    
    private func setHeartUI(){
        self.heartImageView.image = viewModel.isFavorite ? "heartfill".img() : "heart".img()
    }

}
