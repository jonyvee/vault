//
//  GallerAddViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 22.02.2022.
//

import UIKit
import SnapKit


class PhotoAddViewController: UIViewController{
    
    // MARK: - Properties
    
    private var backView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        view.layer.cornerCurve = .continuous
        view.layer.cornerRadius = 24
        return view
    }()
    
    private var topLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(rgb: 0xF6F6F6)
        view.layer.cornerRadius = 2.5
        return view
    }()
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Font.bold.size(24)
        label.textAlignment = .center
        label.textColor = UIColor(rgb: 0x23262F)
        label.text = "New object"
        return label
    }()
    
    private let photoImageView: UIImageView = {
        let imageView = UIImageView(image: "takephoto".img())
        return imageView
    }()
    
    private let uploadImageView: UIImageView = {
        let imageView = UIImageView(image: "uploadfile".img())
        return imageView
    }()
    
    private let photoBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(takePhotoAction), for: .touchUpInside)
        button.titleLabel?.font = Font.medium.size(18)
        button.setTitle("Take a photo", for: .normal)
        button.setTitleColor(UIColor(rgb: 0x1D1D1F), for: .normal)
        return button
    }()
    
    private let uploudBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(uploadFileAction), for: .touchUpInside)
        button.titleLabel?.font = Font.medium.size(18)
        button.setTitle("Upload file", for: .normal)
        button.setTitleColor(UIColor(rgb: 0x1D1D1F), for: .normal)
        return button
    }()
    
    private let bottomSafeAreaView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    // MARK: -
    
    var viewModel: GalleryAddProtocol
    
    // MARK: - Initialization
    
    init(viewModel: GalleryAddProtocol){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        setViews()
        setConstraints()
    }
    
    // MARK: - Actions
    
    @objc private func takePhotoAction(){
        Vibration.soft.vibrate()
        viewModel.takePhoto()
    }
    
    @objc private func uploadFileAction(){
        Vibration.soft.vibrate()
        viewModel.uploadFile()
    }
    
    @objc private func closeAction(){
        Vibration.soft.vibrate()
        viewModel.closeAction()
    }
    
    // MARK: - UI
    
    private func setViews(){
        self.view.addSubview(backView)
        self.view.addSubview(bottomSafeAreaView)
        backView.addSubview(topLineView)
        backView.addSubview(titleLabel)
        backView.addSubview(photoBtn)
        backView.addSubview(photoImageView)
        backView.addSubview(uploudBtn)
        backView.addSubview(uploadImageView)
    }
    
    private func setConstraints(){
        backView.snp.makeConstraints { make in
            make.height.equalTo(200)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }
        
        topLineView.snp.makeConstraints { make in
            make.height.equalTo(5)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(8)
            make.width.equalTo(47)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.height.equalTo(30)
            make.top.equalToSuperview().inset(28)
        }
        
        photoBtn.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(54)
            make.centerX.equalToSuperview()
            make.top.equalTo(titleLabel.snp.bottom).inset(-20)
        }
        
        photoImageView.snp.makeConstraints { make in
            make.height.width.equalTo(24)
            make.centerY.equalTo(photoBtn)
            make.centerX.equalTo(photoBtn).offset(-65)
        }
        
        uploudBtn.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(54)
            make.centerX.equalTo(photoBtn)
            make.top.equalTo(photoBtn.snp.bottom).inset(0)
        }
        
        uploadImageView.snp.makeConstraints { make in
            make.height.width.equalTo(24)
            make.centerY.equalTo(uploudBtn)
            make.centerX.equalTo(uploudBtn).offset(-60)
        }
        
        bottomSafeAreaView.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalToSuperview()
            make.top.equalTo(backView.snp.bottom)
        }
    }


    // MARK: - Touch
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first{
            let location = touch.location(in: self.view)
            if self.backView.frame.contains(location) == false{
                closeAction()
            }
        }
    }
}

