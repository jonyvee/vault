//
//  PhotoViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 28.02.2022.
//

import UIKit
import SnapKit

protocol PhotoProtocol{
    func viewModel(at index: Int) -> PhotoCellPresentable
    func didSelect(at index: Int)
    var numberOfSections: Int {get}
    var numberOfRows: Int{get}
    var title: String{get}
    var isEditMode: Bool {get}
    var isDeleteBtnHidden: Bool {get}
    func didSelectAdd()
    func didSelectEdit()
    func deleteAction()
    var reloadCollectionView: (() -> ())? {set get}
    func didSelectBack()
}

class PhotoViewController: UIViewController{
    
    // MARK: - Properties
    
    private lazy var navigationView: AddNavigationView = {
        let navigation = AddNavigationView(title: viewModel.title, isEditHidden: false, isBackHidden: false, isFavoriteHidden: true)
        return navigation
    }()
    
    private lazy var plugImageView: UIImageView = {
        let imageView = UIImageView(image: "photonone".img())
        imageView.contentMode = .scaleAspectFit
        imageView.isHidden = viewModel.numberOfRows > 0
        return imageView
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets.init(top: 24, left: 16, bottom: 20, right: 16)
        
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(PhotoCell.self, forCellWithReuseIdentifier: PhotoCell.key)
        
        return collectionView
    }()
    
    private var viewModel: PhotoProtocol
    
    // MARK: - Initialization
    
    init(viewModel: PhotoProtocol){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        
        print("init")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit{
        print("deinit")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        setConstraints()
        
        viewModel.reloadCollectionView = {[weak self] in
            self?.plugImageView.isHidden = self?.viewModel.numberOfRows ?? 0 > 0
            self?.collectionView.reloadData()
            self?.updateDeleteUI()
            self?.updateEditUI()
        }
        
        navigationView.didTapBackBtn = {[weak self] in
            self?.viewModel.didSelectBack()
        }
        
        navigationView.didTapAddBtn = { [weak self] in
            self?.viewModel.didSelectAdd()
        }
        
        navigationView.didTapEditBtn = { [weak self] in
            self?.viewModel.didSelectEdit()
            self?.updateEditUI()
        }
        
        navigationView.didTapDeleteBtn = { [weak self] in
            self?.viewModel.deleteAction()
            self?.updateEditUI()
        }
        
    }
    

    
    // MARK: - UI
    
    private func setupViews(){
        self.view.addSubview(navigationView)
        self.view.addSubview(plugImageView)
        self.view.addSubview(collectionView)
    }
    
    private func setConstraints(){
        navigationView.snp.makeConstraints { make in
            make.height.equalTo(AddNavigationView.height)
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(view.safeAreaLayoutGuide)
        }
        
        plugImageView.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalToSuperview()
            make.top.equalTo(navigationView.snp.bottom)
        }
        
        collectionView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(navigationView.snp.bottom)
        }
    }
    
    private func updateEditUI(){
        if viewModel.isEditMode{
            navigationView.updateEditText(text: "Cancel")
        }else{
            navigationView.updateEditText(text: "Edit")
        }
    }
    
    private func updateDeleteUI(){
        navigationView.updateDeleteBtn(isHidden: viewModel.isDeleteBtnHidden)
    }
}


extension PhotoViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCell.key, for: indexPath as IndexPath) as! PhotoCell
        cell.configure(presentable: viewModel.viewModel(at: indexPath.row))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Vibration.soft.vibrate()
        viewModel.didSelect(at: indexPath.row)
        self.updateDeleteUI()
        collectionView.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let item_width = (UIDevice.width-53)/3
        return CGSize(width: item_width, height: item_width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

