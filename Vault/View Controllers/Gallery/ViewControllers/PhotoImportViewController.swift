//
//  PhotoImportViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 06.03.2022.
//

import UIKit
import SnapKit


protocol PhotoImportProtocol{
    var isRemembered: Bool {get}
    func rememberAction()
    func leaveAction()
    func deleteAction()
}

class PhotoImportViewController: UIViewController{
    
    // MARK: - Properties
    
    private let backView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerCurve = .continuous
        view.layer.cornerRadius = 24
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        return view
    }()
    
    private let bottomSafeView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    private var topLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(rgb: 0xF6F6F6)
        view.layer.cornerRadius = 2.5
        return view
    }()
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Font.bold.size(24)
        label.textAlignment = .center
        label.textColor = UIColor(rgb: 0x23262F)
        label.text = "Files will be copy\nto your vault"
        label.numberOfLines = 2
        return label
    }()
    
    private let imageView: UIImageView = {
        let imageView = UIImageView(image: "photoImportImage".img())
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var rememberBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(rememberAction), for: .touchUpInside)
        button.layer.cornerRadius = 16
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(rgb: 0xE2E8F0).cgColor
        return button
    }()
    
    private let rememberLabel: UILabel = {
        let label = UILabel()
        label.text = "Remember my choise"
        label.textColor = UIColor(rgb: 0x718096)
        label.font = Font.medium.size(16)
        return label
    }()
    
    private let rememberImageView: UIImageView = UIImageView()
    
    private let leaveBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(leaveAction), for: .touchUpInside)
        button.setTitle("Keep in Photos", for: .normal)
        button.setTitleColor(UIColor(rgb: 0x334BFF), for: .normal)
        button.titleLabel?.font = Font.regular.size(16)
        return button
    }()
    
    private let deleteBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(deleteAction), for: .touchUpInside)
        button.setTitle("Delete from Photos", for: .normal)
        button.setTitleColor(UIColor(rgb: 0x334BFF), for: .normal)
        button.titleLabel?.font = Font.regular.size(16)
        return button
    }()
    
    // MARK: -
    
    private let viewModel: PhotoImportProtocol
    
    // MARK: - Initialization
    
    init(viewModel: PhotoImportProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        setupViews()
        setConstraitns()
        updateRememberImage()
    }
    
  
    // MARK: - Actions
    
    @objc private func rememberAction(){
        viewModel.rememberAction()
        updateRememberImage()
    }
    
    @objc private func leaveAction(){
        viewModel.leaveAction()
    }
    
    @objc private func deleteAction(){
        viewModel.deleteAction()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        view.addSubview(backView)
        view.addSubview(bottomSafeView)
        backView.addSubview(topLineView)
        backView.addSubview(titleLabel)
        backView.addSubview(imageView)
        backView.addSubview(rememberBtn)
        backView.addSubview(leaveBtn)
        backView.addSubview(deleteBtn)
        rememberBtn.addSubview(rememberLabel)
        rememberBtn.addSubview(rememberImageView)
    }
    
    private func setConstraitns(){
        backView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide)
            make.height.equalTo(395)
        }
        
        bottomSafeView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(backView.snp.bottom)
        }
        
        topLineView.snp.makeConstraints { make in
            make.height.equalTo(5)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(8)
            make.width.equalTo(47)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.height.equalTo(60)
            make.top.equalToSuperview().inset(28)
        }
        
        imageView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(159)
            make.top.equalTo(titleLabel.snp.bottom).inset(-24)
        }
        
        rememberBtn.snp.makeConstraints { make in
            make.height.equalTo(56)
            make.leading.trailing.equalToSuperview().inset(16)
            make.top.equalTo(imageView.snp.bottom).inset(-16)
        }
        
        rememberImageView.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().inset(16)
        }
        
        rememberLabel.snp.makeConstraints { make in
            make.height.centerY.equalToSuperview()
            make.leading.equalToSuperview().inset(16)
            make.trailing.equalTo(rememberImageView.snp.leading).inset(-8)
        }
        
        leaveBtn.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(24)
            make.top.equalTo(rememberBtn.snp.bottom)
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }
        
        deleteBtn.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(24)
            make.top.equalTo(rememberBtn.snp.bottom)
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }
    }
    
    private func updateRememberImage(){
        rememberImageView.image = viewModel.isRemembered ? "photoImportChecked".img() : "photoImportUnchecked".img()
    }
    

}
