//
//  GalleryCellPresentable.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 22.02.2022.
//

import Foundation
import UIKit


protocol GalleryCellPresentable{
    var image: UIImage? { get }
    var title: String { get }
    var subtitle: String { get }
    var isSelected: Bool {get}
}
