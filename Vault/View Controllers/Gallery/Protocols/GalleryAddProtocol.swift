//
//  GalleryAddProtocol.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 22.02.2022.
//

import UIKit


protocol GalleryAddProtocol{
    func takePhoto()
    func uploadFile()
    func closeAction()
}
