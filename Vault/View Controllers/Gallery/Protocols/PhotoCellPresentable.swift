//
//  PhotoCellPresentable.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 28.02.2022.
//

import Foundation
import UIKit


protocol PhotoCellPresentable{
    var image: UIImage? { get }
    var isSelected: Bool {get}
    var isFavorite: Bool {get}
}
