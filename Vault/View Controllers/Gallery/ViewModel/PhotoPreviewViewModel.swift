//
//  PhotoPreviewViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 02.03.2022.
//

import UIKit
import Realm

class PhotoPreviewViewModel: PhotoPreviewProtocol, ImageSaverManager{
        
    // MARK: - Properties
    
    var didClose: (() -> ())?
    
    // MARK: -
    
    var title: String{
        return "IMG_\(picture.id.prefix(4))"
    }
    
    var image: UIImage?{
        let id = picture.id
        return self.retrieveImage(forKey: id)
    }
    
    var isFavorite: Bool{
        self.picture.isFavorite
    }
    
    // MARK: -
    
    private let picture: PictureModel
    
    // MARK: - Initialization
    
    init(picture: PictureModel){
        self.picture = picture
    }
    
    // MARK: - Methods
    
    func deleteAction() {
        let alertController = UIAlertController(title: "Delete", message: "Do you want to delete this photo", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Delete", style: .default) { [self] _ in
            let id = picture.id
            try! realm.write{
                realm.delete(picture)
            }
            removeImage(forKey: id)
            didClose?()
            Analitika.report("deletePictureAction")
        }
        
        let action2 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(action1)
        alertController.addAction(action2)
        UIApplication.shared.topVC?.present(alertController, animated: true, completion: nil)
    }
    
    func favoriteAction() {
        try! realm.write{
            picture.isFavorite.toggle()
        }
        
        if picture.isFavorite{
            Analitika.report("addFavoritePictureAction")
        }else{
            Analitika.report("removeFavoritePictureAction")
        }
        
    }
    
    func shareAction() {
        if let image = image {
            ShareManager.share(items: [image])
        }
        
        Analitika.report("sharePictureAction")
    }
    
    func backAction() {
        didClose?()
        Analitika.report("picturePreviewBackAction")
    }
}
