//
//  GalleryCellViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 22.02.2022.
//

import UIKit



class GalleryCellViewModel: GalleryCellPresentable, ImageSaverManager{
    
    // MARK: - Properties
    
    private let album: AlbumModel
    
    internal var isSelected: Bool
    
    var image: UIImage?{
        if let picture = album.pictures.last{
            let id = picture.id
            if let image = retrievePreviewImage(forKey: id){
                return image
            }
        }
        return nil
    }
    
    var title: String
    
    var subtitle: String
    
    // MARK: - Initialization
    
    init(album: AlbumModel, isSelected: Bool){
        self.album = album
        self.title = album.title
        self.subtitle = "\(album.pictures.count) photos"
        self.isSelected = isSelected
    }
    
}
