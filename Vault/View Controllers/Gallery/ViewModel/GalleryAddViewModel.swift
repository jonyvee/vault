//
//  GalleryAddViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 22.02.2022.
//

import UIKit


class GalleryAddViewModel: GalleryAddProtocol{
    
    // MARK: - Properties
    
    var didClose: (() -> ())?
    var didOpenGallery: ((UIImagePickerController.SourceType) -> ())?
    
    // MARK: - Initialization
    
    init(){}
    
    
    // MARK: - Methods
    
    func takePhoto(){
        didOpenGallery?(.camera)
        Analitika.report("pictureAddWithCameraAction")
    }
    
    func uploadFile() {
        didOpenGallery?(.photoLibrary)
        Analitika.report("pictureAddWithLibraryAction")
    }
    
    func closeAction() {
        didClose?()
        Analitika.report("pictureAddCloseAction")
    }
    
}
