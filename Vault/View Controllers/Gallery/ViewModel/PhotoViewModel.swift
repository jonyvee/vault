//
//  PhotoViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 28.02.2022.
//

import UIKit
import Combine
import RealmSwift

class PhotoViewModel: PhotoProtocol, ImageSaverManager{
    
    // MARK: - Properties
    
    private let album: AlbumModel
    private var subscriptions: Set<AnyCancellable> = []
    private var eventNotificationToken: NotificationToken? = nil
    var selectedRows: [Int] = []
    
    // MARK: -
    
    private var imagePublisher: Published<[UIImage]>.Publisher?
    var didClose: (() -> ())?
    var didAdd: (() -> ())?
    var didOpenPhotoPreview: ((PictureModel) -> ())?
    var didOpenPurchase: (() -> ())?
    
    // MARK: -
    
    var reloadCollectionView: (() -> ())?
    
    // MARK: - Initialization
    
    init(album: AlbumModel, imagePublisher: Published<[UIImage]>.Publisher){
        self.album = album
        
        print("init view model")
        
        self.imagePublisher = imagePublisher
        
        self.imagePublisher?
            .sink { [weak self] (images) in

                print("images.count = ", images.count)
                
                images.forEach { [weak self] (image) in
                    let model = PictureModel()
                    self?.store(image: image, forKey: model.id)

                    try! realm.write{
                        self?.album.pictures.append(model)
                    }
                }

                self?.reloadCollectionView?()

            }.store(in: &subscriptions)
        
        observeEvents()
    }
    
    deinit{
        print("deinit view model")
        eventNotificationToken?.invalidate()
    }
    
    // MARK: - Methods
    var title: String{
        return album.title
    }
    
    func viewModel(at index: Int) -> PhotoCellPresentable {
        return PhotoCellViewModel(picture: album.pictures[index], isSelected: selectedRows.contains(index))
    }
    
    func didSelect(at index: Int) {
        if isEditMode{
            if let indx = selectedRows.firstIndex(where: {$0 == index}){
                selectedRows.remove(at: indx)
            }else{
                selectedRows.append(index)
            }
        }else{
            didOpenPhotoPreview?(album.pictures[index])
            Analitika.report("openPictureAction")
        }
    }
    
    var numberOfSections: Int{
        return 1
    }
    
    var numberOfRows: Int{
        return album.pictures.count
    }
    
    var isEditMode: Bool = false
    
    var isDeleteBtnHidden: Bool{
        selectedRows.count == 0
    }
    
    func deleteAction() {
        let controller = UIAlertController(title: "Delete", message: "Do you want to delete \(selectedRows.count) photos", preferredStyle: .alert)
        let action = UIAlertAction(title: "Delete", style: .default) { _ in
            
            var pictures: [PictureModel] = []
            
            self.selectedRows.forEach { index in
                let picture = self.album.pictures[index]
                pictures.append(picture)
                self.removeImage(forKey: picture.id)
            }
            
            try! realm.write{
                realm.delete(pictures)
            }
            
            self.selectedRows = []
            self.isEditMode = false
            self.reloadCollectionView?()
            
            Analitika.report("deletePicturesAction")
        }
        let action2 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(action)
        controller.addAction(action2)
        UIApplication.shared.topVC?.present(controller, animated: true, completion: nil)
    }
    
    func didSelectEdit() {
        isEditMode.toggle()
        if isEditMode == false{
            selectedRows.removeAll()
            reloadCollectionView?()
        }
    }
    
    func didSelectAdd() {
        if User.shared.isPurchased == true || album.pictures.count <= 9{
            didAdd?()
            Analitika.report("addPicturesAction")
        }else{
            didOpenPurchase?()
        }
    }
    
    func didSelectBack() {
        subscriptions.forEach { subscription in
            subscription.cancel()
        }
        didClose?()
        Analitika.report("picturesBackAction")
    }
    
    
    // MARK: - Observable
    
    private func observeEvents() {
        self.eventNotificationToken = self.album.pictures.observe { (changes: RealmCollectionChange) in
            self.reloadCollectionView?()
        }
    }
    
}
