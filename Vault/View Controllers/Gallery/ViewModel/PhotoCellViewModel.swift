//
//  PhotoCellViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 28.02.2022.
//

import UIKit


class PhotoCellViewModel: PhotoCellPresentable, ImageSaverManager{
        
    // MARK: - Properties
    
    private let picture: PictureModel
    
    var isSelected: Bool
    
    var isFavorite: Bool
    
    var image: UIImage?{
        let id = picture.id
        return self.retrievePreviewImage(forKey: id)
    }
    
    // MARK: - Initialization
    
    init(picture: PictureModel, isSelected: Bool){
        self.picture = picture
        self.isSelected = isSelected
        self.isFavorite = picture.isFavorite
    }

}
