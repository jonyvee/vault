//
//  PhotoImportViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 06.03.2022.
//

import Foundation


class PhotoImportViewModel: PhotoImportProtocol{
    
    
    // MARK: - Properties
    
    var didClose: (((ImportTypeViewModel.ImportType, Bool)) -> ())?
    
    // MARK: -
    
    private var isRemember: Bool = false
    
    // MARK: - Initialization
    
    init(){}
    
    // MARK: - Methods
    
    var isRemembered: Bool{
        isRemember
    }
    
    func rememberAction() {
        isRemember.toggle()
        
        if isRemember{
            Analitika.report("photoImportRememberAction")
        }else{
            Analitika.report("photoImportRemoveRememberAction")
        }
    }
    
    func leaveAction() {
        isWillResignActivePresentation = false
        didClose?((ImportTypeViewModel.ImportType.alwaysLeave, isRemember))
        Analitika.report("photoImportKeepAction")
    }
    
    func deleteAction() {
        isWillResignActivePresentation = false
        didClose?((ImportTypeViewModel.ImportType.alwaysDelete, isRemember))
        Analitika.report("photoImportDeleteAction")
    }
    
}
