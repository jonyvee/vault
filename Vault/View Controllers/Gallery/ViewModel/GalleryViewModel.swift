//
//  GalleryViewModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 19.02.2022.
//

import UIKit
import RealmSwift
import Combine
import SwiftLoader

class GalleryViewModel: ImageSaverManager{
    
    // MARK: - Properties
    
    private var albums = realm.objects(AlbumModel.self)
    private var favoriteImages = realm.objects(PictureModel.self).where({$0.isFavorite == true})
    
    private var eventNotificationToken: NotificationToken? = nil
    private var eventNotificationToken2: NotificationToken? = nil
    
    // MARK: -
    
    var selectedRows: [Int] = []
    var isEditMode: Bool = false
    
    // MARK: -
    
    var didOpenAlbum: ((AlbumModel) -> ())?
    var didOpenPhotoPreview: ((PictureModel) -> ())?
    var didOpenPurchase: (() -> ())?
    
    // MARK: -
    
    @Published var isFavourite: Bool = false
    private var subscriptions: Set<AnyCancellable> = []
    var reloadCollectionView: (() -> ())?
    
    // MARK: - Initialization
    
    init() {
        observeEvents()
    }
    
    deinit{
        eventNotificationToken?.invalidate()
        eventNotificationToken2?.invalidate()
    }
    
    // MARK: - Methods
    
    func addAction(){
        let alertController = UIAlertController(title: "Create new album", message: "Enter a name for your new album", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Album name"
        }
        let saveAction = UIAlertAction(title: "Create", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            
            if let text = firstTextField.text, text != ""{
                
                if User.shared.isPurchased == true || self.albums.count <= 2{
                    self.createAlbum(name: text)
                }else{
                    self.didOpenPurchase?()
                }
                
            }else{
                JonyMessage.show(text: "Album name can't be empty")
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        UIApplication.shared.topVC?.present(alertController, animated: true, completion: nil)
    }
    
    var isDeleteBtnHidden: Bool{
        selectedRows.count == 0
    }
    
    var numberOfSections: Int{
        return 1
    }
    
    var numberOfRows: Int{
        return isFavourite ? favoriteImages.count : albums.count
    }
    
    func viewModel(at index: Int) -> GalleryCellViewModel{
        return GalleryCellViewModel(album: self.albums[index], isSelected: selectedRows.contains(index))
    }
    
    func viewModel(at index: Int) -> PhotoCellPresentable{
        return PhotoCellViewModel(picture: favoriteImages[index], isSelected: selectedRows.contains(index))
    }
    
    func didSelect(at index: Int){
        if isEditMode{
            if let indx = selectedRows.firstIndex(where: {$0 == index}){
                selectedRows.remove(at: indx)
            }else{
                selectedRows.append(index)
            }
        }else{
            if isFavourite{
                didOpenPhotoPreview?(favoriteImages[index])
            }else{
                didOpenAlbum?(albums[index])
            }
        }
    }
    
    func didSelectFavorite(){
        self.selectedRows = []
        self.isEditMode = false
        reloadCollectionView?()
    }
    
    func didSelectAll(){
        self.selectedRows = []
        self.isEditMode = false
        reloadCollectionView?()
    }
    
    func deleteAction() {
        let controller = UIAlertController(title: "Delete", message: "Do you want to delete \(selectedRows.count) \(isFavourite ? "photos" : "albums")", preferredStyle: .alert)
        let action = UIAlertAction(title: "Delete", style: .default) { _ in

            SwiftLoader.show(animated: true)

            if self.isFavourite{
                var pictures: [PictureModel] = []
                self.selectedRows.forEach { index in
                    let picture = self.favoriteImages[index]
                    pictures.append(picture)
                    self.removeImage(forKey: picture.id)
                }
                
                try! realm.write{
                    realm.delete(pictures)
                }
                
                Analitika.report("deletePicturesAction")
                
            }else{
                var albums: [AlbumModel] = []
                self.selectedRows.forEach { index in
                    let album = self.albums[index]
                    albums.append(album)
                    
                    album.pictures.forEach { picture in
                        self.removeImage(forKey: picture.id)
                    }
                }
                
                try! realm.write{
                    realm.delete(albums)
                }
                
                Analitika.report("deleteAlbumsAction")
            }
            
            SwiftLoader.hide()

            self.selectedRows = []
            self.isEditMode = false
            self.reloadCollectionView?()
        }
        let action2 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(action)
        controller.addAction(action2)
        UIApplication.shared.topVC?.present(controller, animated: true, completion: nil)
    }
    
    func didSelectEdit() {
        isEditMode.toggle()
        if isEditMode == false{
            selectedRows.removeAll()
            reloadCollectionView?()
        }
    }
    
    // MARK: - Helper
    
    private func createAlbum(name: String){
        let newAlbum = AlbumModel()
        newAlbum.title = name
        
        try! realm.write {
            realm.add(newAlbum)
        }
        
        Analitika.report("createAlbumAction")
    }
    
    // MARK: - Observable
    
    private func observeEvents() {
        self.eventNotificationToken = self.albums.observe { (changes: RealmCollectionChange) in
            self.reloadCollectionView?()
        }
        
        self.eventNotificationToken2 = self.favoriteImages.observe { (changes: RealmCollectionChange) in
            self.reloadCollectionView?()
        }
        
        $isFavourite
            .sink { value in
                self.reloadCollectionView?()
            }.store(in: &subscriptions)
    }
    
}
