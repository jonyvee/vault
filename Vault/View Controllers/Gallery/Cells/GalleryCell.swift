//
//  GalleryCell.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 22.02.2022.
//

import UIKit
import SnapKit


class GalleryCell: UICollectionViewCell{
    
    // MARK: - Static
    
    static let key = "GalleryCell"
    
    // MARK: - Properties
    
    private let backView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerCurve = .continuous
        view.layer.cornerRadius = 20
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.1
        view.layer.shadowRadius = 6
        view.layer.shadowOffset = .zero
        view.layer.borderColor = UIColor(rgb: 0x334BFF).cgColor
        return view
    }()
    
    private lazy var albumImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerCurve = .continuous
        imageView.layer.cornerRadius = 20
        return imageView
    }()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = Font.bold.size(14)
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor(rgb: 0x1A202C)
        label.textAlignment = .center
        return label
    }()
    
    private let countLabel: UILabel = {
        let label = UILabel()
        label.font = Font.regular.size(12)
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor(rgb: 0x718096)
        label.textAlignment = .center
        return label
    }()
    
    // MARK: -
    
    private var hasSelection: Bool = false
    
    // MARK: - Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backView.layer.shadowPath = UIBezierPath(roundedRect: backView.bounds, cornerRadius: 20).cgPath
    }
    
    // MARK: - Configuration
    
    func configure(presentable: GalleryCellPresentable){
        self.hasSelection = presentable.isSelected
        
        if let image = presentable.image{
            albumImageView.image = image
            albumImageView.backgroundColor = .clear
        }else{
            albumImageView.image = "folder_empty".img()
            albumImageView.backgroundColor = UIColor(rgb: 0xC6CCD5)
        }
        
        setSelection()
        nameLabel.text = presentable.title
        countLabel.text = presentable.subtitle
        self.contentView.layoutIfNeeded()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        contentView.addSubview(backView)
        backView.addSubview(albumImageView)
        backView.addSubview(nameLabel)
        backView.addSubview(countLabel)
    }
    
    private func setConstraints(){
        backView.snp.makeConstraints { make in
            make.leading.trailing.bottom.top.equalToSuperview()
        }
        
        albumImageView.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.614)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(8)
            make.height.equalTo(20)
            make.top.equalTo(albumImageView.snp.bottom).inset(-10)
        }
        
        countLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(8)
            make.height.equalTo(16)
            make.top.equalTo(nameLabel.snp.bottom).inset(-5)
        }
    }
    
    private func setSelection(){
        if hasSelection{
            backView.layer.borderWidth = 4
        }else{
            backView.layer.borderWidth = 0
        }
    }
}
