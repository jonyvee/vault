//
//  PhotoCell.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 28.02.2022.
//

import UIKit
import SnapKit


class PhotoCell: UICollectionViewCell{
    
    // MARK: - Static
    
    static let key = "PhotoCell"
    
    // MARK: - Properties

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerCurve = .continuous
        imageView.layer.cornerRadius = 16
        imageView.layer.borderColor = UIColor(rgb: 0x334BFF).cgColor
        return imageView
    }()
    
    private let heartImageView = UIImageView(image: "heartfill".img())
    
    // MARK: -
    
    private var hasSelection: Bool = false
    
    // MARK: - Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Configuration
    
    func configure(presentable: PhotoCellPresentable){
        self.hasSelection = presentable.isSelected
        if let image = presentable.image{
            imageView.image = image
        }
        
        heartImageView.isHidden = !presentable.isFavorite
        
        setSelection()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        contentView.addSubview(imageView)
        imageView.addSubview(heartImageView)
    }
    
    private func setConstraints(){
        imageView.snp.makeConstraints { make in
            make.leading.top.trailing.bottom.equalToSuperview()
        }
        
        heartImageView.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.top.trailing.equalToSuperview().inset(8)
        }
    }
    
    private func setSelection(){
        if hasSelection{
            imageView.layer.borderWidth = 4
        }else{
            imageView.layer.borderWidth = 0
        }
    }
}
