//
//  AlbumModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 28.02.2022.
//

import UIKit
import RealmSwift

class AlbumModel: Object, Codable{
    @objc dynamic var id: String = NSUUID().uuidString
    @objc dynamic var title: String = ""
    @objc dynamic var isFavorite: Bool = false
    var pictures = RealmSwift.List<PictureModel>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
