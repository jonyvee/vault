//
//  PictureModel.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 28.02.2022.
//


import UIKit
import Realm
import RealmSwift

class PictureModel: Object, Codable{
    @objc dynamic var id: String = NSUUID().uuidString
    @objc dynamic var isFavorite: Bool = false
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
