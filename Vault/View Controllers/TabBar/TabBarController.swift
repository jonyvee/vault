//
//  TabBarController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit


class TabBarController: UITabBarController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        // здесь можем поменять иконки для таббаров ...
        self.tabBar.backgroundColor = .white
        self.tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.tabBar.layer.cornerRadius = 24
        
        self.tabBar.layer.shadowOpacity = 0.2
        self.tabBar.layer.shadowColor = UIColor(rgb: 0xA0AEC0).cgColor
        self.tabBar.layer.shadowRadius = 24
        self.tabBar.layer.shadowOffset = .init(width: 0, height: -10)
    }

}
