//
//  AppCoordinator.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit


class TabCoordinator: Coordinator{
    
    // MARK: - Properties
    
    private let tabBarController = TabBarController()
    
    var rootViewController: UIViewController {
        return tabBarController
    }
    
    var hasEnterCoordinator: Bool = true
    
    // MARK: - Initialization

    override init() {
        super.init()
        
        let galleryCoordinator = GalleryCoordinator()
        let passwordsCoordinator = PasswordsCoordinator()
        let notesCoordinator = NotesCoordinator()
        let settignsCoordinator = SettingsCoordinator()

        // Update View Controllers
        tabBarController.viewControllers = [
            galleryCoordinator.rootViewController,
            passwordsCoordinator.rootViewController,
            notesCoordinator.rootViewController,
            settignsCoordinator.rootViewController
        ]
        

        // Append to Child Coordinators
        childCoordinators.append(galleryCoordinator)
        childCoordinators.append(passwordsCoordinator)
        childCoordinators.append(notesCoordinator)
        childCoordinators.append(settignsCoordinator)
    }

    // MARK: - Overrides
    
    override func start() {
        childCoordinators.forEach { (childCoordinator) in
            // Start Child Coordinator
            childCoordinator.start()
        }
        
        if hasEnterCoordinator{
            let enterType: EnterViewModel.EnterType = User.shared.password == "" ? .registration : .login
            let enterCoordinator = EnterCoordinator(presentingViewController: tabBarController, enterType: enterType)
            enterCoordinator.start()
        }
    }
    
}
