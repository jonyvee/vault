//
//  OnboardCoordinator.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit
import SafariServices

class OnboardCoordinator: Coordinator{
    
    // MARK: - Properties
    
    private var navigationController: UINavigationController = {
        let navigation = UINavigationController()
        navigation.isNavigationBarHidden = true
        return navigation
    }()
    
    var rootViewController: UIViewController{
        navigationController
    }
    
    // MARK: - Initialization
    
    override init(){
        super.init()
    }

    // MARK: - Overrides
    
    override func start() {
        let viewModel = OnboardViewModel(coordinator: self)
        let controller = OnboardViewController(viewModel: viewModel)
        navigationController.pushViewController(controller, animated: false)
    }
    
    func showFutureViewController(){
        let viewModel = FutureViewModel(coordinator: self)
        let controller = FutureViewController(viewModel: viewModel)
        self.navigationController.pushViewController(controller, animated: true)
    }
    
    func showReminderViewController(){
        let viewModel = ReminderViewModel()
        
        viewModel.didClose = {
            User.shared.isPassOnboard = true
            UIApplication.shared.keyWindow?.rootViewController = SceneDelegate.appCoordinator.rootViewController
            UIApplication.shared.keyWindow?.makeKeyAndVisible()
            SceneDelegate.appCoordinator.start()
        }
        
        viewModel.didShowTerms = { [weak self] in
            self?.openDocument(link: AppData.termsOfUse)
        }
        
        viewModel.didShowPrivacy = { [weak self] in
            self?.openDocument(link: AppData.privacyPolicy)
        }
        
        let controller = ReminderViewController(viewModel: viewModel)
        self.navigationController.pushViewController(controller, animated: true)
    }
    
    
    // MARK: - Helper
    
    private func openDocument(link: String){
        if let url = URL(string: link) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            self.navigationController.present(vc, animated: true, completion: nil)
        }
    }
}
