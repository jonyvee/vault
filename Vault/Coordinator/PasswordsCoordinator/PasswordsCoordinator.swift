//
//  PasswordsCoordinator.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 19.02.2022.
//

import UIKit
import Combine
import SafariServices
import MessageUI

class PasswordsCoordinator: Coordinator{
    
    
    // MARK: - Properties

    private var navigationController: UINavigationController = {
        let navigation = UINavigationController()
        navigation.isNavigationBarHidden = true
        navigation.tabBarItem.title = "Passwords"
        navigation.tabBarItem.image = "unselected2".img()
        navigation.tabBarItem.selectedImage = "selected2".img()
        return navigation
    }()

    var rootViewController: UIViewController {
        return navigationController
    }
    
    // MARK: -
    
    var passwordAddNavigationController: UINavigationController?
    
    // MARK: -
    
    @Published var generatedPassword: String = ""

    // MARK: - Overrides

    override func start() {
        let viewModel = PasswordsViewModel()
        
        viewModel.didShowPasswordDetail = {[weak self] (passwordData) in
            self?.showPassDetail(data: passwordData)
        }
        
        viewModel.didShowPasswordAdd = {[weak self] in
            self?.showPasswordAdd()
        }
        
        viewModel.didShowPurchase = {[weak self] in
            self?.openPurchase(type: 2)
        }
        
        let controller = PasswordsViewController(viewModel: viewModel)
        self.navigationController.pushViewController(controller, animated: false)
    }
    
    
    private func showPasswordAdd(){
        self.passwordAddNavigationController = UINavigationController()
        passwordAddNavigationController?.isNavigationBarHidden = true
        let viewModel = PasswordAddViewModel(passwordPublisher: $generatedPassword)
        
        viewModel.didShowGeneratePassword = { [weak self] in
            if User.shared.isPurchased!{
                self?.showGeneratePassword()
            }else{
                self?.openPurchase(type: 1)
            }
        }
        
        viewModel.didClose = { [weak self] (animate) in
            self?.dissmissPasswordAddNavigation(animate: animate)
        }
        
        let controller = PasswordAddViewController(viewModel: viewModel)
        passwordAddNavigationController?.modalPresentationStyle = .overFullScreen
        self.navigationController.present(passwordAddNavigationController!, animated: false, completion: nil)
        passwordAddNavigationController?.pushViewController(controller, animated: true)
    }
    
    private func showGeneratePassword(){
        let viewModel = GeneratePassViewModel()
        
        viewModel.didClose = {[weak self] (generatedPassword) in
            self?.generatedPassword = generatedPassword
            self?.passwordAddNavigationController?.popViewController(animated: true)
        }
                
        let controller = GeneratePassViewController(viewModel: viewModel)
        passwordAddNavigationController?.pushViewController(controller, animated: true)
    }
    
    private func showPassDetail(data: PasswordModel){
        let viewModel = PassDetailViewModel(data: data, passwordPublisher: $generatedPassword)
        
        viewModel.didClose = {[weak self] in
            self?.navigationController.popViewController(animated: true)
            self?.generatedPassword = ""
        }
        
        viewModel.didShowgeneratePassword = { [weak self] in
            if User.shared.isPurchased!{
                self?.showGeneratePasswordFromDetail()
            }else{
                self?.openPurchase(type: 2)
            }
        }
        
        let controller = PassDetailViewController(viewModel: viewModel)
        self.navigationController.pushViewController(controller, animated: true)
    }
    
    private func showGeneratePasswordFromDetail(){
        let viewModel = GeneratePassViewModel()
        
        viewModel.didClose = {[weak self] (generatedPassword) in
            self?.generatedPassword = generatedPassword
            self?.navigationController.popViewController(animated: true)
        }
        
        let controller = GeneratePassViewController(viewModel: viewModel)
        controller.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func dissmissPasswordAddNavigation(animate: Bool = false){
        self.passwordAddNavigationController = nil
        self.navigationController.dismiss(animated: animate, completion: nil)
    }
    
    private func openPurchase(type: Int){
        if PayManager.shared.isWhiteOnboard{
            let viewModel = PurchaseViewModel()
            viewModel.didClose = { [weak self] in
                if type == 1{
                    self?.passwordAddNavigationController?.popViewController(animated: true)
                }else{
                    self?.navigationController.popViewController(animated: true)
                }
            }
            viewModel.didShowTerms = { [weak self] in
                self?.openTerms(type: .terms, type2: type)
            }
            viewModel.didShowPrivacy = { [weak self] in
                self?.openTerms(type: .privacy, type2: type)
            }
            let controller = PurchaseViewController(viewModel: viewModel)
            controller.hidesBottomBarWhenPushed = true
            self.navigationController.pushViewController(controller, animated: true)
        }else{
            let viewModel = PurchaseBlueViewModel()
            viewModel.didClose = { [weak self] in
                if type == 1{
                    self?.passwordAddNavigationController?.popViewController(animated: true)
                }else{
                    self?.navigationController.popViewController(animated: true)
                }
            }
            viewModel.didShowTerms = { [weak self] in
                self?.openTerms(type: .terms, type2: type)
            }
            viewModel.didShowPrivacy = { [weak self] in
                self?.openTerms(type: .privacy, type2: type)
            }
            let controller = PurchaseBlueViewController(viewModel: viewModel)
            controller.hidesBottomBarWhenPushed = true
            
            if type == 1{
                self.passwordAddNavigationController?.pushViewController(controller, animated: true)
            }else{
                self.navigationController.pushViewController(controller, animated: true)
            }
        }
    }
    
    private func openTerms(type: SettingsData.SettingType, type2: Int){
        var linkString = ""
        
        if type == .terms{
            linkString = AppData.termsOfUse
        }else if type == .privacy{
            linkString = AppData.privacyPolicy
        }else if type == .support{
            linkString = AppData.support
        }
        
        if let url = URL(string: linkString) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            
            if type2 == 1{
                self.passwordAddNavigationController?.present(vc, animated: true, completion: nil)
            }else{
                self.navigationController.present(vc, animated: true, completion: nil)
            }
        }
    }
}

extension PasswordsCoordinator: MFMailComposeViewControllerDelegate{
    
}
