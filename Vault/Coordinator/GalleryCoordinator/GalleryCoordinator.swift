//
//  GalleryCoordinator.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 19.02.2022.
//

import UIKit
import Combine
import BSImagePicker
import SwiftLoader
import Photos
import SafariServices
import MessageUI

class GalleryCoordinator: Coordinator{
    
    
    // MARK: - Properties
    
    private var navigationController: UINavigationController = {
        let navigation = UINavigationController()
        navigation.isNavigationBarHidden = true
        navigation.tabBarItem.title = "Gallery"
        navigation.tabBarItem.image = "unselected1".img()
        navigation.tabBarItem.selectedImage = "selected1".img()
        return navigation
    }()
    
    var rootViewController: UIViewController {
        return navigationController
    }
    
    private var imagePicker: ImagePicker?
    @Published var newGalleryImage: [UIImage] = []
    
    // MARK: - Initialization
    
    override init(){
        super.init()
        imagePicker = ImagePicker(presentationController: self.navigationController, delegate: self)
    }
    
    // MARK: - Overrides
    
    override func start() {
        let viewModel = GalleryViewModel()
        
        viewModel.didOpenAlbum = {[weak self] (album) in
            self?.showPhoto(album: album)
        }
        
        viewModel.didOpenPhotoPreview = {[weak self] (picture) in
            self?.showPhotoPreview(picture: picture)
        }
        
        viewModel.didOpenPurchase = {[weak self] in
            self?.openPurchase()
        }
    
        let controller = GalleryViewController(viewModel: viewModel)
        self.navigationController.pushViewController(controller, animated: false)
    }
    
    private func showPhoto(album: AlbumModel){
        let viewModel = PhotoViewModel(album: album, imagePublisher: $newGalleryImage)
        
        viewModel.didClose = {[weak self] in
            self?.navigationController.popViewController(animated: true)
            self?.newGalleryImage.removeAll()
        }
        
        viewModel.didAdd = {[weak self] in
            self?.showGalleryAdd()
        }
        
        viewModel.didOpenPhotoPreview = { [weak self] (picture) in
            self?.showPhotoPreview(picture: picture)
        }
        
        viewModel.didOpenPurchase = { [weak self] in
            self?.openPurchase()
        }
        
        let controller = PhotoViewController(viewModel: viewModel)
        self.navigationController.pushViewController(controller, animated: true)
    }
    
    private func showGalleryAdd(){
        let viewModel = GalleryAddViewModel()
        
        viewModel.didClose = {[weak self] in
            self?.navigationController.dismiss(animated: false, completion: nil)
        }
        
        viewModel.didOpenGallery = {[weak self] (type) in
            self?.navigationController.dismiss(animated: false, completion: nil)
                        
            if type == .camera{
                self?.imagePicker?.present(type: type)
            }else{
                                
                if User.shared.importType == .askToDeleteOrLeave{
                    
                    self?.showPhotoImport(didClose: { [weak self] (arg0) in
                        self?.navigationController.dismiss(animated: false, completion: nil)
                        let (type, isRemember) = arg0
                        
                        if isRemember{
                            User.shared.importType = type
                        }
                        
                        self?.presentImagePicker(importType: type)
                    })
                    
                }else{
                    self?.presentImagePicker(importType: User.shared.importType!)
                }
            }
        }
        
        let controller = PhotoAddViewController(viewModel: viewModel)
        controller.modalPresentationStyle = .overFullScreen
        self.navigationController.present(controller, animated: false, completion: nil)
    }

    private func showPhotoPreview(picture: PictureModel){
        let viewModel = PhotoPreviewViewModel(picture: picture)
        
        viewModel.didClose = { [weak self] in
            self?.navigationController.popViewController(animated: true)
        }
        
        let controller = PhotoPreviewViewController(viewModel: viewModel)
        controller.hidesBottomBarWhenPushed = true
        self.navigationController.pushViewController(controller, animated: true)
    }
    
    private func showPhotoImport(didClose: (((ImportTypeViewModel.ImportType, Bool)) -> ())?){
        let viewModel = PhotoImportViewModel()
        viewModel.didClose = didClose
        let controller = PhotoImportViewController(viewModel: viewModel)
        controller.modalPresentationStyle = .overFullScreen
        self.navigationController.present(controller, animated: false, completion: nil)
    }
    
    
    // MARK: - Helper
    
    private func presentImagePicker(importType: ImportTypeViewModel.ImportType){
        navigationController.presentImagePicker(ImagePickerController(),
                                                       select: { (asset) in},
                                                       deselect: { asset in},
                                                       cancel: { asset in},
                                                       finish: { asset in
            
            SwiftLoader.show(animated: true)
            self.loadImage(assets: asset)
            
            if importType == .alwaysDelete{
                isWillResignActivePresentation = false
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.deleteAssets(asset as NSFastEnumeration)
                })
            }
        })
    }
    
    private func loadImage(assets: [PHAsset]) {
                
        if assets.isEmpty {
            DispatchQueue.main.async {
                SwiftLoader.hide()
                self.newGalleryImage = []
            }
            return
        }
        
        var assets = assets
        let asset = assets.removeFirst()
        
        let options = PHImageRequestOptions()
        options.isSynchronous = true
        PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: options) { (image, info) in
            guard let info = info, let image = image, let result = info["PHImageResultIsDegradedKey"] as? Int, result == 0 else {
                return
            }
        
            DispatchQueue.main.async {
                self.newGalleryImage = [image]
            }
            
            self.loadImage(assets: assets)
        }
    }
    
    
    private func openPurchase(){
        if PayManager.shared.isWhiteOnboard{
            let viewModel = PurchaseViewModel()
            viewModel.didClose = { [weak self] in
                self?.navigationController.popViewController(animated: true)
            }
            viewModel.didShowTerms = { [weak self] in
                self?.openTerms(type: .terms)
            }
            viewModel.didShowPrivacy = { [weak self] in
                self?.openTerms(type: .privacy)
            }
            let controller = PurchaseViewController(viewModel: viewModel)
            controller.hidesBottomBarWhenPushed = true
            self.navigationController.pushViewController(controller, animated: true)
        }else{
            let viewModel = PurchaseBlueViewModel()
            viewModel.didClose = { [weak self] in
                self?.navigationController.popViewController(animated: true)
            }
            viewModel.didShowTerms = { [weak self] in
                self?.openTerms(type: .terms)
            }
            viewModel.didShowPrivacy = { [weak self] in
                self?.openTerms(type: .privacy)
            }
            let controller = PurchaseBlueViewController(viewModel: viewModel)
            controller.hidesBottomBarWhenPushed = true
            self.navigationController.pushViewController(controller, animated: true)
        }
    }
    
    private func openTerms(type: SettingsData.SettingType){
        var linkString = ""
        
        if type == .terms{
            linkString = AppData.termsOfUse
        }else if type == .privacy{
            linkString = AppData.privacyPolicy
        }else if type == .support{
            linkString = AppData.support
        }
        
        if let url = URL(string: linkString) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            self.navigationController.present(vc, animated: true, completion: nil)
        }
    }
    
}

// MARK: - ImagePickerDelegate
extension GalleryCoordinator: ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        if let image = image{
            newGalleryImage = [image]
        }else{
            print("image is nil")
        }
    }
}


extension GalleryCoordinator: MFMailComposeViewControllerDelegate{
    
}
