//
//  AppCoordinator.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit

class AppCoordinator: Coordinator{
    
    // MARK: - Properties
    
    private var tabBarCoordinator = TabCoordinator()
    
    private let onboardCoordinator = OnboardCoordinator()
    
    var hasEnterCoordinator: Bool = true
    
    var rootViewController: UIViewController {
        if User.shared.isPassOnboard == false{
            return onboardCoordinator.rootViewController
        }else{
            return tabBarCoordinator.rootViewController
        }
    }
    
    override init(){
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(screenshotTaken), name: UIApplication.userDidTakeScreenshotNotification, object: nil)
    }
    
    // MARK: - Overrides
    
    override func start() {
        if User.shared.isPassOnboard == false{
            onboardCoordinator.start()
        }else{
            tabBarCoordinator.hasEnterCoordinator = self.hasEnterCoordinator
            tabBarCoordinator.start()
        }
    }
    
    func presentEnterViewController(){
        if User.shared.isPassOnboard == true && User.shared.password != ""{
            let enterCoordinator = EnterCoordinator(presentingViewController: tabBarCoordinator.rootViewController, enterType: .login)
            enterCoordinator.start()
        }
    }
    
    // MARK: - Helper
    
    @objc private func screenshotTaken(){
        if User.shared.hasScreenRestriction == true{
            DispatchQueue.main.async {
                self.presentEnterViewController()
            }
        }
    }
}
