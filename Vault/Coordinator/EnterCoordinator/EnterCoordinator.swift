//
//  PasswordCoordinator.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 20.02.2022.
//

import UIKit


class EnterCoordinator: Coordinator{
    
    // MARK: - Properties

    private var navigationController: UINavigationController = {
        let navigation = UINavigationController()
        navigation.isNavigationBarHidden = true
        navigation.modalPresentationStyle = .overFullScreen
        return navigation
    }()

    private let presentingViewController: UIViewController
    
    private let enterType: EnterViewModel.EnterType

    // MARK: - Initialization
    
    init(presentingViewController: UIViewController, enterType: EnterViewModel.EnterType) {
        self.presentingViewController = presentingViewController
        self.enterType = enterType
    }
    
    deinit{
        print("deinit Enter Coordinator")
    }

    // MARK: - Overrides

    override func start() {
        let viewModel = EnterViewModel(enterType: enterType)
        
        viewModel.didClose = {
            self.navigationController.dismiss(animated: true, completion: nil)
        }
        
        let controller = EnterViewController(viewModel: viewModel)
        self.navigationController.pushViewController(controller, animated: false)
        self.presentingViewController.present(navigationController, animated: false, completion: nil)
    }
}
