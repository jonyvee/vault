//
//  NotesCoordinator.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 19.02.2022.
//

import UIKit


class NotesCoordinator: Coordinator{
    
    // MARK: - Properties

    private var navigationController: UINavigationController = {
        let navigation = UINavigationController()
        navigation.isNavigationBarHidden = true
        navigation.tabBarItem.title = "Notes"
        navigation.tabBarItem.image = "unselected3".img()
        navigation.tabBarItem.selectedImage = "selected3".img()
        return navigation
    }()

    var rootViewController: UIViewController {
        return navigationController
    }

    // MARK: - Overrides

    override func start() {
        let viewModel = NotesViewModel()
        
        viewModel.didTapCreate = {[weak self] in
            self?.openCreateNote(note: nil)
        }
        
        viewModel.didOpenNote = {[weak self] (note) in
            self?.openCreateNote(note: note)
        }
        
        let controller = NotesViewController(viewModel: viewModel)
        self.navigationController.pushViewController(controller, animated: false)
    }
    
    
    private func openCreateNote(note: NoteModel?){
        let viewModel = CreateNoteViewModel(data: note)
        
        viewModel.didClose = { [weak self] in
            self?.navigationController.popViewController(animated: true)
        }
        
        let controller = CreateNoteViewController(viewModel: viewModel)
        controller.hidesBottomBarWhenPushed = true
        self.navigationController.pushViewController(controller, animated: true)
    }
    
}
