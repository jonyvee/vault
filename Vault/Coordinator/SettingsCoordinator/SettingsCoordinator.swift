//
//  SettingsCoordinator.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 19.02.2022.
//

import UIKit
import SafariServices
import MessageUI

class SettingsCoordinator: Coordinator{
    
    // MARK: - Properties

    private var navigationController: UINavigationController = {
        let navigation = UINavigationController()
        navigation.isNavigationBarHidden = true
        navigation.tabBarItem.title = "Settings"
        navigation.tabBarItem.image = "unselected4".img()
        navigation.tabBarItem.selectedImage = "selected4".img()
        return navigation
    }()

    var rootViewController: UIViewController {
        return navigationController
    }

    // MARK: - Overrides

    override func start() {
        let viewModel = SettingsViewModel()
        
        viewModel.didOpenPurchase = { [weak self] in
            self?.openPurchase()
        }
        
        viewModel.didOpen = { [weak self] (type) in
            switch type{
            case .support:
                self?.openSupport()
            case .privacy, .terms:
                self?.openTerms(type: type)
            case .rate:
                self?.openRate()
            case .vault:
                self?.openImportType()
            case .fakePasscode:
                if User.shared.isPurchased!{
                    self?.openFakePass()
                }else{
                    self?.openPurchase()
                }
            case .changePasscode:
                self?.openCreatePasscode()
            default:
                break
            }
        }
        
        let controller = SettingsViewController(viewModel: viewModel)
        self.navigationController.pushViewController(controller, animated: false)
    }
    
    
    private func openPurchase(){
        if PayManager.shared.isWhiteOnboard{
            let viewModel = PurchaseViewModel()
            viewModel.didClose = { [weak self] in
                self?.navigationController.popViewController(animated: true)
            }
            viewModel.didShowTerms = { [weak self] in
                self?.openTerms(type: .terms)
            }
            viewModel.didShowPrivacy = { [weak self] in
                self?.openTerms(type: .privacy)
            }
            let controller = PurchaseViewController(viewModel: viewModel)
            controller.hidesBottomBarWhenPushed = true
            self.navigationController.pushViewController(controller, animated: true)
        }else{
            let viewModel = PurchaseBlueViewModel()
            viewModel.didClose = { [weak self] in
                self?.navigationController.popViewController(animated: true)
            }
            viewModel.didShowTerms = { [weak self] in
                self?.openTerms(type: .terms)
            }
            viewModel.didShowPrivacy = { [weak self] in
                self?.openTerms(type: .privacy)
            }
            let controller = PurchaseBlueViewController(viewModel: viewModel)
            controller.hidesBottomBarWhenPushed = true
            self.navigationController.pushViewController(controller, animated: true)
        }
    }
    
    private func openRate(){
        let viewModel = RateViewModel()
        
        viewModel.didClose = { [weak self] in
            self?.navigationController.dismiss(animated: false, completion: nil)
        }
        
        let controller = RateViewController(viewModel: viewModel)
        controller.modalPresentationStyle = .overFullScreen
        self.navigationController.present(controller, animated: false, completion: nil)
    }
    
    private func openImportType(){
        let viewModel = ImportTypeViewModel()
        
        viewModel.didClose = { [weak self] in
            self?.navigationController.dismiss(animated: false, completion: nil)
        }
        
        let controller = ImportTypeViewController(viewModel: viewModel)
        controller.modalPresentationStyle = .overFullScreen
        self.navigationController.present(controller, animated: false, completion: nil)
    }
    
    private func openFakePass(){
        let viewModel = FakePassViewModel()
        
        viewModel.didClose = {[weak self] in
            self?.navigationController.dismiss(animated: false, completion: nil)
        }
        
        viewModel.didTapCreatePasscode = {[weak self] in
            guard let view = self else {return}
            let enterCoordinator = EnterCoordinator(presentingViewController: view.navigationController, enterType: .fakePasscode)
            enterCoordinator.start()
        }
        
        let controller = FakePassViewController(viewModel: viewModel)
        controller.modalPresentationStyle = .overFullScreen
        self.navigationController.present(controller, animated: false, completion: nil)
    }
    
    private func openCreatePasscode(){
        let enterCoordinator = EnterCoordinator(presentingViewController: self.navigationController, enterType: .registrationWithClose)
        enterCoordinator.start()
    }
    
    // MARK: - Helper
    
    private func openSupport(){
        if MFMailComposeViewController.canSendMail() {
            let recipientEmail = "lockit.vault@gmail.com"
            let subject = "Hello dear Support"
            let body = ""
            
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([recipientEmail])
            mail.setSubject(subject)
            mail.setMessageBody(body, isHTML: false)
            self.navigationController.present(mail, animated: true, completion: nil)
        }
    }

    private func openTerms(type: SettingsData.SettingType){
        var linkString = ""
        
        if type == .terms{
            linkString = AppData.termsOfUse
        }else if type == .privacy{
            linkString = AppData.privacyPolicy
        }else if type == .support{
            linkString = AppData.support
        }
        
        if let url = URL(string: linkString) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            self.navigationController.present(vc, animated: true, completion: nil)
        }
    }
    
}


extension SettingsCoordinator: MFMailComposeViewControllerDelegate{
    
}
