//
//  Label.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 20.02.2022.
//

import UIKit


extension UILabel{
    
    func secureTextEntry(){
        guard let text = self.text else {return}
        var newText = ""
        text.forEach({_ in newText.append("•")})
        self.text = newText
    }
}


