//
//  String.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit


extension String{
    
    func img() -> UIImage?{
        return UIImage(named: self)
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont?) -> CGFloat {
        guard let fontValue = font else {
            return 0
        }
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: [NSAttributedString.Key.font: fontValue],
                                            context: nil)
        return ceil(boundingBox.height)
    }
}
