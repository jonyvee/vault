//
//  UIDevice.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit


extension UIDevice {
    
    static var hasNotch: Bool {
        UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0 > 20
    }
    
    static var isSmall: Bool{
        width == 375 && height == 667 // iphone 8
    }
    
    static var topAreaHeight: CGFloat {
        UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
    }
    
    static var bottomAreaHeight: CGFloat{
        UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
    }
    
    static var width: CGFloat{
        UIScreen.main.bounds.width
    }
    
    static var height: CGFloat{
        UIScreen.main.bounds.height
    }
    
    static func getId() -> String{
        UIDevice.current.identifierForVendor!.uuidString.replacingOccurrences(of: "-", with: "")
    }
    
}


