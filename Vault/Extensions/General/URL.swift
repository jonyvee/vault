//
//  URL.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 05.03.2022.
//

import UIKit


extension URL{
    
    static var documentDirectory: URL?{
        FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
    }
    
}
