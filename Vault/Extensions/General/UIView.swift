//
//  UIView.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 21.03.2022.
//

import UIKit


extension UIView {
    func applyGradient(colours: [UIColor], locations: [NSNumber]? = [0,1], startPoint: CGPoint = CGPoint(x: 0, y: 0), endPoint: CGPoint = CGPoint(x: 1, y: 1), cornerRadius: CGFloat = 0) {
        
        var isInitialized = false
        
        if let layer = self.layer.sublayers?.first(where: {$0.name == "gradient"}){
            CATransaction.begin()
            CATransaction.setDisableActions(true)
            layer.frame = self.bounds
            isInitialized = true
            CATransaction.commit()
        }
        
        if isInitialized == false{
            CATransaction.begin()
            CATransaction.setDisableActions(true)
            let gradient: CAGradientLayer = CAGradientLayer()
            gradient.frame = self.bounds
            gradient.colors = colours.map { $0.cgColor }
            gradient.locations = locations
            gradient.startPoint = startPoint
            gradient.endPoint = endPoint
            gradient.name = "gradient"
            gradient.cornerRadius = cornerRadius
            gradient.zPosition = -10
            self.layer.insertSublayer(gradient, at: 0)
            CATransaction.commit()
        }
    }
    
    
    func removeGradient(){
        self.layer.sublayers?.removeAll(where: {$0.name == "gradient"})
    }
}
