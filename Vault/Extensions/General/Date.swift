//
//  Date.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 26.02.2022.
//

import UIKit


extension Date {
    // примеры формата: "dd/MM HH:mm"   "dd MMMM yyyy"  "MMMM yyyy"
    func toString(format: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = .current
        return dateFormatter.string(from: self)
    }
    
    func get(_ components: Calendar.Component..., calendar: Calendar = Calendar.current) -> DateComponents {
        return calendar.dateComponents(Set(components), from: self)
    }
    
    func get(_ component: Calendar.Component, calendar: Calendar = Calendar.current) -> Int {
        return calendar.component(component, from: self)
    }

    func date(byAdding component: Calendar.Component, value: Int) -> Date{
        if let date = Calendar.current.date(byAdding: component, value: value, to: self) {
            return date
        }
        fatalError("jonyvee Method error date(byAdding)")
    }
}

