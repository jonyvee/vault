//
//  ShareManager.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 03.03.2022.
//

import UIKit
import SwiftLoader


final class ShareManager{
    
    static func share(items: [Any]){
        SwiftLoader.show(animated: true)
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)

        // ios 13 bug: https://forums.developer.apple.com/thread/119482
        // remove when fixed
        let fakeViewController = UIViewController()
        fakeViewController.modalPresentationStyle = .overFullScreen


        ac.completionWithItemsHandler = { [weak fakeViewController] _, _, _, _ in
            if let presentingViewController = fakeViewController?.presentingViewController {
                presentingViewController.dismiss(animated: false, completion: nil)
            } else {
                fakeViewController?.dismiss(animated: false, completion: nil)
            }
        }
        
        UIApplication.shared.topVC?.present(fakeViewController, animated: true) { [weak fakeViewController] in
            fakeViewController?.present(ac, animated: true, completion: {
                SwiftLoader.hide()
            })
        }
    }
    
}
