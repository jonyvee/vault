//
//  SubscriptionData.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 15.03.2022.
//

import UIKit
import Adapty


enum Identifiers: String, CaseIterable{
    case weak = "reminder.week"
    case month = "lockit.month"
    case year = "lockit.year"
}

enum PaywallType: String, CaseIterable{
    case reminder
    case blue
    case white
}


class PayManager{
    
    // MARK: - Static
    
    static let shared = PayManager()
    
    // MARK: - Properties
    
    private var paywalls: [PaywallModel] = []
    private(set) var isWhiteOnboard: Bool = true
    
    // MARK: - Methods
    
    func initialiaze(){
        Adapty.logLevel = .all
        Adapty.activate("public_live_lqvurJsW.OqjZWPp2Kjll1LfU3rJl", customerUserId: UIDevice.getId())
    }
    
    func loadPaywalls(){
        Adapty.getPaywalls(forceUpdate: false) { (paywalls, products, error) in
            if error == nil, let paywalls = paywalls {
                self.paywalls = paywalls
                self.checkIsWhiteOnboard()
            }
        }
    }
    
    func getProducts(paywall type: PaywallType, completion: @escaping (([ProductModel]?) -> ())){
        if paywalls.isEmpty{
            Adapty.getPaywalls(forceUpdate: false) { (paywalls, products, error) in
                if error == nil, let paywalls = paywalls {
                    self.paywalls = paywalls
                    self.checkIsWhiteOnboard()
                    DispatchQueue.main.async {
                        completion(self.getProducts(type))
                    }
                }else{
                    DispatchQueue.main.async {
                        completion(nil)
                    }
                }
            }
        }else{
            DispatchQueue.main.async {
                completion(self.getProducts(type))
            }
        }
    }
    
    func getPrice(for product: ProductModel, devide: Float = 0) -> String? {
        guard let skProduct = product.skProduct else {return nil}
        let price = Float(truncating: skProduct.price) / devide
        let price2 = skProduct.price
    
        let numberFormatter = NumberFormatter()
        let locale = skProduct.priceLocale
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = locale
        
        if devide != 0{
            return numberFormatter.string(from: NSNumber(value: price))
        }else{
            return numberFormatter.string(from: price2)
        }
    }
    
    func getPurchaserInfo(completion: @escaping ((_ isSuccess: Bool) -> ())){
        Adapty.getPurchaserInfo { purchaser, error in
            if error == nil {
                completion(false)
                return
            }
            
            if let access = purchaser?.accessLevels["premium"]{
                User.shared.isPurchased = access.isLifetime ? true : access.isActive
            }
            completion(true)
        }
    }
    
    // MARK: - Helper
    
    private func getProducts(_ type: PaywallType) -> [ProductModel]{
        var products: [ProductModel] = []
        if let paywall = self.paywalls.first(where: {$0.developerId == type.rawValue}){
            products = paywall.products
        }
        return products
    }
    
    private func checkIsWhiteOnboard(){
        guard let paywall = self.paywalls.first(where: {$0.developerId == PaywallType.white.rawValue}) else {return}
        guard let data = paywall.customPayload?.first?.value as? String else {return}
        isWhiteOnboard = data == "white"
    }
}
