//
//  ImageSaver.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 28.02.2022.
//

import UIKit


protocol ImageSaverManager{
    func store(image: UIImage, forKey key: String)
    func retrievePreviewImage(forKey key: String) -> UIImage?
    func retrieveImage(forKey key: String) -> UIImage?
    func removeImage(forKey key: String)
}


extension ImageSaverManager{
    
    func store(image: UIImage, forKey key: String) {
        if let jpegRepresentation = image.jpegData(compressionQuality: 1) {
            if let filePath = filePath(forKey: key) {
                do  {
                    try jpegRepresentation.write(to: filePath, options: .atomic)
                } catch let err {
                    print("Saving file resulted in error: ", err)
                }
            }
        }
        
        let newImage = image.aspectFitted(UIDevice.width/3)
        if let previewRepresentation = newImage.jpegData(compressionQuality: 0.25) {
            if let filePath = filePath(forKey: "\(key)_preview") {
                do  {
                    try previewRepresentation.write(to: filePath, options: .atomic)
                } catch let err {
                    print("Saving file resulted in error: ", err)
                }
            }
        }
    }
    
    func retrievePreviewImage(forKey key: String) -> UIImage? {
        if let filePath = self.filePath(forKey: "\(key)_preview"),
            let fileData = FileManager.default.contents(atPath: filePath.path),
            let image = UIImage(data: fileData) {
            return image
        }
        
        return nil
    }
    
    func retrieveImage(forKey key: String) -> UIImage? {
        if let filePath = self.filePath(forKey: key),
            let fileData = FileManager.default.contents(atPath: filePath.path),
            let image = UIImage(data: fileData) {
            return image
        }
        
        return nil
    }
    
    func removeImage(forKey key: String){
        if let filePath = self.filePath(forKey: key), let filePath2 = self.filePath(forKey: "\(key)_preview"){
            try? FileManager.default.removeItem(at: filePath)
            try? FileManager.default.removeItem(at: filePath2)
        }
    }
    
    // MARK: - Helper
    
    private func filePath(forKey key: String) -> URL? {
        let fileManager = FileManager.default
        guard let documentURL = fileManager.urls(for: .documentDirectory,
                                                in: FileManager.SearchPathDomainMask.userDomainMask).first else { return nil }
        
        return documentURL.appendingPathComponent(key + ".jpeg")
    }
}
