//
//  AnalitycsManager.swift
//  Raskraska2
//
//  Created by Nikola Cvetkovic on 05.10.2021.
//  Copyright © 2021 jonyvee. All rights reserved.
//

import UIKit
import YandexMobileMetrica
import Firebase
import FirebaseAnalytics
import Branch

class Analitika{
    
    static func initialize(){
        let configuration = YMMYandexMetricaConfiguration.init(apiKey: "f5c2f945-2a4d-4138-a5e3-7b2c2f15b2a7")
        configuration?.userProfileID = UIDevice.getId()
        configuration?.locationTracking = true
        configuration?.crashReporting = true
        YMMYandexMetrica.activate(with: configuration!)
        
        FirebaseApp.configure()
        Analytics.setUserID(UIDevice.getId())
        
        Branch.getInstance().setIdentity(UIDevice.getId())
    }
    
    static func report(_ name: String, _ params: [String: Any] = [:]){
        YMMYandexMetrica.reportEvent(name, parameters: params) { error in
            print(error.localizedDescription)
        }
        
        Analytics.logEvent(name, parameters: params)
    }
    
    static func updateUser(_ data: [String: String]){
        let profile = YMMMutableUserProfile()
        var profileArray: [YMMUserProfileUpdate] = []
        
        // other attributes
        data.forEach { (key: String, value: String) in
            Analytics.setUserProperty(value, forName: key)
            profileArray.append(YMMProfileAttribute.customString(key).withValue(value))
        }
        
        // logins count
        profileArray.append(YMMProfileAttribute.customCounter("logins_count").withDelta(1))
        
        // has premium
        Analytics.setUserProperty(User.shared.isPurchased!.description, forName: "has_premium")
        profileArray.append(YMMProfileAttribute.customBool("has_premium").withValue(User.shared.isPurchased!))
        
        // profile apply
        profile.apply(from: profileArray)
        YMMYandexMetrica.report(profile, onFailure: { (error) in
            print("REPORT ERROR: %@", error.localizedDescription)
        })
    }

}
