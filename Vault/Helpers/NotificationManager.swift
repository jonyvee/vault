//
//  LocalNotificationManager.swift
//  Raskraska2
//
//  Created by Nikola Cvetkovic on 17.11.2021.
//  Copyright © 2021 jonyvee. All rights reserved.
//

import UIKit
import UserNotifications


class NotificationManager: NSObject, UNUserNotificationCenterDelegate{
    
    static func setDelegate(delegate: UNUserNotificationCenterDelegate){
        UNUserNotificationCenter.current().delegate = delegate
    }
    
    static func registerForPushNotifications(completion: @escaping (_ isEnabled: Bool) -> ()) {
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: { (isEnabled, error) in
            completion(isEnabled)
        })
        UIApplication.shared.registerForRemoteNotifications()
    }
        
    static func turnOnTrialReminderNotification(){
        let content = UNMutableNotificationContent()
        content.title = "LockIt"
        content.body = "Your trial subscription ends tomorrow"
        
        let date = Date().date(byAdding: .day, value: 2)
        var comp = date.get(.year, .month, .day, .hour, .minute, .second)
        comp.calendar = Calendar.current
        comp.timeZone = NSTimeZone.system
        
        // Create the trigger as a repeating event.
        let trigger = UNCalendarNotificationTrigger(dateMatching: comp, repeats: false)
        let uuidString = "notification_reminder_notification"
        let request = UNNotificationRequest(identifier: uuidString,
                    content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { error in
            if let error = error{print("jonyvee error = ", error.localizedDescription)}
        }
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate{}
