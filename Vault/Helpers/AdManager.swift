//
//  AdManager.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 25.03.2022.
//

import UIKit
import Appodeal

class AdManager{
    
    static var timer: Timer?
    
    static func initialize(){
        Appodeal.setAutocache(false, types: [.banner, .interstitial])
        Appodeal.initialize(withApiKey: "b001a7d592ccc2613def88c909d9beda5bfad4e848bca9bd", types: [.banner, .interstitial])
        Appodeal.setTestingEnabled(true)
        
        setupTimer()
    }
    
    private static func setupTimer(){
        if User.shared.isPurchased == false{
            timer = Timer.scheduledTimer(timeInterval: 180, target: self, selector: #selector(showInterstitial), userInfo: nil, repeats: true)
        }
    }
    
    @objc static func showInterstitial(){
        if Appodeal.isReadyForShow(with: .interstitial), User.shared.isPurchased == false {
            guard let rootVC = UIApplication.shared.topVC else {return}
            Appodeal.showAd(AppodealShowStyle.interstitial, rootViewController: rootVC)
        }
    }
    
    static func setBanner(rootVC: UIViewController){
        Appodeal.showAd(.bannerBottom, rootViewController: rootVC)
    }
        
}
