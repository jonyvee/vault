//
//  JoonyMessage.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 03.03.2022.
//

import UIKit
import SnapKit

final class JonyMessage{
        
    static func show(text: String, backgroundColor: UIColor = UIColor(rgb: 0xFF453A)){
        let messageView = UIView()
        messageView.frame = CGRect(x: 26, y: -56, width: UIDevice.width-52, height: 56)
        messageView.backgroundColor = backgroundColor
        messageView.layer.cornerRadius = 20
        
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.text = text
        label.font = Font.semibold.size(16)
        label.adjustsFontSizeToFitWidth = true
        messageView.addSubview(label)
        
        label.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(-16)
            make.top.bottom.equalToSuperview()
        }
        
        UIApplication.shared.topVC?.view.addSubview(messageView)
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut) {
            messageView.frame.origin.y = UIDevice.topAreaHeight + 12
        } completion: { _ in
            UIView.animate(withDuration: 0.3, delay: 1.5, options: .curveEaseOut) {
                messageView.frame.origin.y = -56
            } completion: { _ in
                messageView.removeFromSuperview()
            }
        }
    }
    
}
