//
//  NavigationView.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 19.02.2022.
//

import UIKit
import SnapKit
import Combine

class AddNavigationView: UIView{
    
    // MARK: - Static
    
    static let height: CGFloat = 108
    
    // MARK: - Properties
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = Font.bold.size(24)
        label.textColor = UIColor(rgb: 0x1A202C)
        return label
    }()
    
    private lazy var addBtn: UIButton = {
        let button = UIButton()
        button.setImage("add".img(), for: .normal)
        button.addTarget(self, action: #selector(addAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var backBtn: UIButton = {
        let button = UIButton()
        button.setImage("pass_back".img(), for: .normal)
        button.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var allBtn: UIButton = {
        let button = UIButton()
        button.setTitle("All", for: .normal)
        button.setTitleColor(UIColor(rgb: 0x334BFF), for: .normal)
        button.titleLabel?.font = Font.bold.size(16)
        button.addTarget(self, action: #selector(allAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var favoriteBtn: UIButton = {
        let button = UIButton()
        button.setTitle("Favorite", for: .normal)
        button.setTitleColor(UIColor(rgb: 0x334BFF), for: .normal)
        button.titleLabel?.font = Font.bold.size(16)
        button.addTarget(self, action: #selector(favoriteAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var editBtn: UIButton = {
        let button = UIButton()
        button.setTitle("Edit", for: .normal)
        button.layer.cornerRadius = 12
        button.backgroundColor = UIColor(rgb: 0x334BFF)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = Font.regular.size(14)
        button.addTarget(self, action: #selector(editAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var deleteBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(deleteAction), for: .touchUpInside)
        button.setImage("deleteBlue".img(), for: .normal)
        button.isHidden = true
        return button
    }()
    
    private let bottomLine: UIView = {
        let line = UIView()
        line.backgroundColor = UIColor(rgb: 0xE6EAF2)
        return line
    }()
    
    private lazy var selectionLine: UIView = {
        let view = UIView(frame: CGRect(x: 12, y: -1, width: 32, height: 3))
        view.backgroundColor = UIColor(rgb: 0x334BFF)
        return view
    }()
    
    // MARK: -
    
    private var isSelectedAll: Bool = true
    private var isBackBtnHidden = true
    // MARK: -
    
    var didTapAddBtn: (() -> ())?
    var didTapAllBtn: (() -> ())?
    var didTapFavoriteBtn: (() -> ())?
    var didTapEditBtn: (() -> ())?
    var didTapBackBtn: (() -> ())?
    var didTapDeleteBtn: (() -> ())?
    
    // MARK: - Initialization
    
    init(title: String, isEditHidden: Bool = true, isBackHidden: Bool = true, isFavoriteHidden: Bool = false) {
        super.init(frame: .zero)
        self.isBackBtnHidden = isBackHidden
        self.favoriteBtn.isHidden = isFavoriteHidden
        self.titleLabel.text = title
        self.editBtn.isHidden = isEditHidden
        self.backBtn.isHidden = isBackHidden
        setupViews()
        setConstraints()
        updateSelectionConstants()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Public Methods
    
    public func updateEditText(text: String){
        editBtn.setTitle(text, for: .normal)
    }
    
    public func updateDeleteBtn(isHidden: Bool){
        deleteBtn.isHidden = isHidden
    }
    
    
    // MARK: - Actions
    
    @objc private func backAction(){
        Vibration.soft.vibrate()
        didTapBackBtn?()
    }
    
    @objc private func addAction(){
        Vibration.soft.vibrate()
        didTapAddBtn?()
    }
                                                 
    @objc private func allAction(){
        Vibration.soft.vibrate()
        isSelectedAll = true
        updateSelection()
        didTapAllBtn?()
    }
    
    @objc private func favoriteAction(){
        Vibration.soft.vibrate()
        isSelectedAll = false
        updateSelection()
        didTapFavoriteBtn?()
    }
    
    @objc private func editAction(){
        Vibration.soft.vibrate()
        didTapEditBtn?()
    }
    
    @objc private func deleteAction(){
        Vibration.soft.vibrate()
        didTapDeleteBtn?()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        addSubview(titleLabel)
        addSubview(addBtn)
        addSubview(bottomLine)
        addSubview(allBtn)
        addSubview(favoriteBtn)
        addSubview(editBtn)
        addSubview(backBtn)
        addSubview(deleteBtn)
        bottomLine.addSubview(selectionLine)
    }
    
    private func setConstraints(){
        titleLabel.snp.makeConstraints { make in
            make.height.equalTo(30)
            if isBackBtnHidden{
                make.leading.equalToSuperview().inset(16)
            }else{
                make.centerX.equalToSuperview()
                make.trailing.equalTo(addBtn.snp.leading).inset(-16)
            }
            make.centerY.equalTo(addBtn)
        }
        
        addBtn.snp.makeConstraints { make in
            make.height.width.equalTo(40)
            make.trailing.equalToSuperview().inset(16)
            make.top.equalToSuperview().inset(24)
        }
        
        bottomLine.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
        
        editBtn.snp.makeConstraints { make in
            make.height.equalTo(24)
            make.width.equalTo(52)
            make.trailing.equalToSuperview().inset(16)
            make.bottom.equalToSuperview().inset(5)
        }
        
        deleteBtn.snp.makeConstraints { make in
            make.width.height.equalTo(25)
            make.trailing.equalTo(editBtn.snp.leading).inset(-8)
            make.centerY.equalTo(editBtn)
        }
        
        allBtn.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(12)
            make.height.equalTo(32)
            make.width.equalTo(32)
            make.bottom.equalToSuperview()
        }
        
        favoriteBtn.snp.makeConstraints { make in
            make.height.equalTo(32)
            make.width.equalTo(61)
            make.leading.equalToSuperview().inset(61)
            make.centerY.equalTo(allBtn)
        }
        
        backBtn.snp.makeConstraints { make in
            make.height.width.equalTo(30)
            make.centerY.equalTo(addBtn)
            make.leading.equalToSuperview().inset(16)
        }
    }
    
    // MARK: - Methods
    
    public func setHiddenAddBtn(isHidden: Bool){
        addBtn.isHidden = isHidden
    }
    
    public func setHiddenEditBtn(isHidden: Bool){
        editBtn.isHidden = isHidden
    }
    
    // MARK: - Private Methods
    
    private func updateSelection(){
        updateSelectionConstants()
        
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut, animations: {
            if self.isSelectedAll{
                self.selectionLine.frame = CGRect(x: self.allBtn.frame.origin.x, y: -1, width: self.allBtn.frame.width, height: 3)
            }else{
                self.selectionLine.frame = CGRect(x: self.favoriteBtn.frame.origin.x, y: -1, width: self.favoriteBtn.frame.width, height: 3)
            }
        }, completion: nil)
    }
    
    private func updateSelectionConstants(){
        allBtn.setTitleColor(isSelectedAll ? UIColor(rgb: 0x334BFF) : UIColor(rgb: 0x718096), for: .normal)
        favoriteBtn.setTitleColor(isSelectedAll ? UIColor(rgb: 0x718096) : UIColor(rgb: 0x334BFF), for: .normal)
        allBtn.titleLabel?.font = isSelectedAll ? Font.bold.size(16) : Font.regular.size(16)
        favoriteBtn.titleLabel?.font = isSelectedAll ? Font.regular.size(16) : Font.bold.size(16)
    }
}
