//
//  PurchaseBlueData.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 21.03.2022.
//

import UIKit


struct PurchaseBlueData{
    
    let items: [Item] = [
        .init(image: "purchicon1".img(), title: "Fake Passwords", subtitle: """
You can create multiple passwords. If a user
enters the wrong password, they are
redirected to another account.
"""),
        .init(image: "purchicon2".img(), title: "Unlimited Files", subtitle: """
Create as many files and
folders on your device as you want
"""),
        .init(image: "purchicon3".img(), title: "No ads", subtitle: "Turn off all ads in the app"),
        .init(image: "purchicon4".img(), title: "Screenshot Preventing", subtitle: """
Stop taking screenshots on your device,
keep all data protected!
"""),
        .init(image: "purchicon5".img(), title: "Password Generator", subtitle: """
Create strong passwords
for any type of data
"""),
        .init(image: "purchicon6".img(), title: "Passwords Section", subtitle: """
Create as many files and
folders on your device as you want
"""),
        .init(image: "purchicon7".img(), title: "New Features and Updates", subtitle: """
Get access to new features
after updating the app.
"""),
    ]
    
    struct Item: PurchaseBlueCellPresentable{
        let image: UIImage?
        let title: String
        let subtitle: String
    }
}

