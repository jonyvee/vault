//
//  SettingsData.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 22.02.2022.
//

import UIKit


struct SettingsData{
    
    // MARK: - Types
    
    enum SettingType: Int{
        case faceId = 0
        case screenshots
        case changePasscode
        case fakePasscode
        case vault
        case privacy
        case terms
        case support        
        case rate
    }
        
    let items: [Item] = [
        .init(isSwitcher: true, isOn: User.shared.hasFaceId, icon: "settingsIcon1".img(), title: "Use Face ID", subtitle: "To enter the application", type: .faceId),
        .init(isSwitcher: true, isOn: User.shared.hasScreenRestriction, icon: "settingsIcon2".img(), title: "Screenshots restrictions", subtitle: "To enter the application", type: .screenshots),
        .init(isSwitcher: false, isOn: nil, icon: "settingsIcon3".img(), title: "Change passcode", subtitle: "To enter the application", type: .changePasscode),
        .init(isSwitcher: false, isOn: nil, icon: "settingsIcon4".img(), title: "Fake passcode", subtitle: "To enter the application", type: .fakePasscode),
        .init(isSwitcher: false, isOn: nil, icon: "settingsIcon5".img(), title: "Adding to Vault", subtitle: "After adding to vault", type: .vault),
        .init(isSwitcher: false, isOn: nil, icon: "settingsIcon6".img(), title: "Privacy Policy", subtitle: "Have some question? ", type: .privacy),
        .init(isSwitcher: false, isOn: nil, icon: "settingsIcon6".img(), title: "Terms of use", subtitle: "Have some question? ", type: .terms),
        .init(isSwitcher: false, isOn: nil, icon: "settingsIcon6".img(), title: "Support", subtitle: "Have some question? ", type: .support),
        .init(isSwitcher: false, isOn: nil, icon: "settingsIcon7".img(), title: "Rate the app", subtitle: "Evaluate us to make us better!", type: .rate),
    ]
    
    let fakeItems: [Item] = [
        .init(isSwitcher: true, isOn: User.shared.hasFaceId, icon: "settingsIcon1".img(), title: "Use Face ID", subtitle: "To enter the application", type: .faceId),
        .init(isSwitcher: true, isOn: User.shared.hasScreenRestriction, icon: "settingsIcon2".img(), title: "Screenshots restrictions", subtitle: "To enter the application", type: .screenshots),
        .init(isSwitcher: false, isOn: nil, icon: "settingsIcon5".img(), title: "Adding to Vault", subtitle: "After adding to vault", type: .vault),
        .init(isSwitcher: false, isOn: nil, icon: "settingsIcon6".img(), title: "Privacy Policy", subtitle: "Have some question? ", type: .privacy),
        .init(isSwitcher: false, isOn: nil, icon: "settingsIcon6".img(), title: "Terms of use", subtitle: "Have some question? ", type: .terms),
        .init(isSwitcher: false, isOn: nil, icon: "settingsIcon6".img(), title: "Support", subtitle: "Have some question? ", type: .support),
        .init(isSwitcher: false, isOn: nil, icon: "settingsIcon7".img(), title: "Rate the app", subtitle: "Evaluate us to make us better!", type: .rate),
    ]
    
    struct Item: SettingsCellPresentable{
        var isSwitcher: Bool
        var isOn: Bool?
        var icon: UIImage?
        var title: String
        var subtitle: String
        var type: SettingType
    }
}
