//
//  OnboardData.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit


struct OnboardData{
    
    let items: [Item] = [
        .init(image: "board1".img(), title: "Complete confidentiality", subtitle: "Do not allow unauthorized persons and\ncompanies to collect your personal data"),
        .init(image: "board2".img(), title: "Data Security solution", subtitle: "Hide your personal: photos, passwords, notes\nand other  in a secure storage by LockIt"),
        .init(image: "board3".img(), title: "Secure Access Only", subtitle: "All your data is securely password protected.\nYou can also set access only by Face ID"),
        .init(image: "board4".img(), title: "Fake password", subtitle: "You can create multiple passwords. If a user\nenters the wrong password, they are redirected\nto another account"),
        .init(image: "board5".img(), title: "Screenshot Preventing", subtitle: "Stop taking screenshots on your device, leave\nall data protected! ")
    ]
    
    struct Item: OnboardCellPresentable{
        let image: UIImage?
        let title: String
        let subtitle: String
    }
}
