//
//  Font.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit


enum Font: String{
    case regular = "SFProDisplay-Regular"
    case medium = "SFProDisplay-Medium"
    case bold = "SFProDisplay-Bold"
    case semibold = "SFProDisplay-Semibold"
    
    func size(_ size: CGFloat) -> UIFont{
        return UIFont(name: self.rawValue, size: size)!
    }
}
