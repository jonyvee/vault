//
//  UserData.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 20.02.2022.
//

import UIKit

class User{
    
    static let shared = User()
    
    private let defaults = UserDefaults()
    
    enum DefaultsType: String{
        case isPassOnboard
        case password
        case isPurchased
        case hasFaceId
        case hasScreenRestriction
        case importType
        case isFakePasscoded
        case fakePasscode
        case isPushEnabled
    }
    
    var isFakeLogin = false
    
    var isPassOnboard: Bool?{
        set{defaults.set(newValue ?? false, forKey: DefaultsType.isPassOnboard.rawValue)}
        get{return defaults.value(forKey: DefaultsType.isPassOnboard.rawValue) as? Bool ?? false}
    }
    
    var password: String?{
        set{defaults.set(newValue ?? "", forKey: DefaultsType.password.rawValue)}
        get{return defaults.value(forKey: DefaultsType.password.rawValue) as? String ?? ""}
    }
    
    var isPurchased: Bool?{
        set{defaults.set(newValue ?? false, forKey: DefaultsType.isPurchased.rawValue)}
        get{return defaults.value(forKey: DefaultsType.isPurchased.rawValue) as? Bool ?? false}
    }
    
    var hasFaceId: Bool?{
        set{defaults.set(newValue ?? false, forKey: DefaultsType.hasFaceId.rawValue)}
        get{return defaults.value(forKey: DefaultsType.hasFaceId.rawValue) as? Bool ?? false}
    }
    
    var hasScreenRestriction: Bool?{
        set{defaults.set(newValue ?? false, forKey: DefaultsType.hasScreenRestriction.rawValue)}
        get{return defaults.value(forKey: DefaultsType.hasScreenRestriction.rawValue) as? Bool ?? false}
    }
    
    var importType: ImportTypeViewModel.ImportType?{
        set{defaults.set(newValue?.rawValue ?? 0, forKey: DefaultsType.importType.rawValue)}
        get{return ImportTypeViewModel.ImportType(rawValue: defaults.value(forKey: DefaultsType.importType.rawValue) as? Int ?? 0)}
    }
    
    var isFakePasscoded: Bool?{
        set{defaults.set(newValue ?? false, forKey: DefaultsType.isFakePasscoded.rawValue)}
        get{return defaults.value(forKey: DefaultsType.isFakePasscoded.rawValue) as? Bool ?? false}
    }
    
    var fakePasscode: String?{
        set{defaults.set(newValue ?? "", forKey: DefaultsType.fakePasscode.rawValue)}
        get{return defaults.value(forKey: DefaultsType.fakePasscode.rawValue) as? String ?? ""}
    }
    
    var isPushEnabled: Bool?{
        set{defaults.set(newValue ?? false, forKey: DefaultsType.isPushEnabled.rawValue)}
        get{return defaults.value(forKey: DefaultsType.isPushEnabled.rawValue) as? Bool ?? false}
    }
}

